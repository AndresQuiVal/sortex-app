﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AndroidApp = Android.App.Application;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android;
using Sortex.Droid.Services;

namespace Sortex.Droid
{
    [BroadcastReceiver(Enabled =true, Name ="com.companyname.Sortex.RebootReceiver")]
    [IntentFilter(new[] { Intent.ActionBootCompleted })]
    public class RebootReceiver : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent)
        {
            if (!Intent.ActionBootCompleted.Equals(intent.Action))
                return;

            Intent serviceIntent = new Intent(context, typeof(RebootService));
            context.StartService(serviceIntent);
        }
    }
}