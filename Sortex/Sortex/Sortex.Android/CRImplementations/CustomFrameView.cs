﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Sortex.CustomRenderers;
using Sortex.Droid.CRImplementations;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TagFrame), typeof(CustomFrameView))]
namespace Sortex.Droid.CRImplementations
{
    public class CustomFrameView : ViewRenderer<TagFrame, ViewGroup>
    {
        public CustomFrameView(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<TagFrame> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                ViewGroup vg = new CardView(Context)
                {
                    Radius = 100,
                    Elevation = 11,
                };
                SetNativeControl(vg);
            }
        }
    }
}