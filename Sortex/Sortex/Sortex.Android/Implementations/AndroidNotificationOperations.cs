﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Broadcast;
using Sortex.Droid.Implementations;
using Sortex.Interfaces;
using Xamarin.Forms;

[assembly:Dependency(typeof(AndroidNotificationOperations))]
namespace Sortex.Droid.Implementations
{
    public class AndroidNotificationOperations : INotificationOperations
    {
        public void RemoveNotificationTag()
        {
            var notificationManager = NotificationManagerCompat.From(Android.App.Application.Context);
            notificationManager.CancelAll();
        }
    }
}