﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Support.V4.App;
using Sortex.Droid.Implementations;
using Sortex.Helpers;
using Sortex.Interfaces;
using System;
using Xamarin.Forms;
using AndroidApp = Android.App.Application;
using Sortex.Droid.Broadcast;
using Sortex.Enums;

[assembly: Dependency(typeof(AndroidNotificationManager))]
namespace Sortex.Droid.Implementations
{
    public class AndroidNotificationManager : INotificationManager
    {
        const string channelId = "default";
        const string channelName = "Default";
        const string channelDescription = "The default channel for notifications.";
        const int pendingIntentId = 0;

        public const string TitleKey = "title";
        public const string MessageKey = "message";
        public const string ID = "id";
        public const string Messages = "messages";

        bool channelInitialized = false;
        
        int messageId = -1;
        
        NotificationManager manager;
        public static PendingIntent pendingIntent;
        public static AlarmManager alarmManager;

        public event EventHandler NotificationReceived;

        public void Initialize()
        {
            CreateNotificationChannel();
        }

        public void SetReminderNotification(
            string title,
            string message,
            long startSeconds,
            int id,
            bool isRepeated = false
            /*long[] intervalSeconds = null*/)
        {
            Intent reminderIntent = 
                this.CreateNotificationContext<NotificationReciever>(title, message, id);
            SetPendingIntent(reminderIntent, id);
            //App.Current.Properties.Add( CODE NOT NECCESARY!
            //    "Times",
            //    NotificationReciever.testTimes); //persistance data

            //App.Current.Properties.Add("PendingIntent", pendingIntent); //persistance data

            //if (!intervalIfChron) DISCOMMENT
            //{
            //EstablishNotification(startSeconds);
            //    return;
            //}

            //alarmManager.Set( DISCOMMENT
            //    AlarmType.ElapsedRealtimeWakeup,
            //    SystemClock.ElapsedRealtime() + (startSeconds * 1000),
            //    pendingIntent);]

            //alarmManager.SetRepeating( THIS REALLY NEEDS TO BE DISCOMMENTED
            //    AlarmType.ElapsedRealtimeWakeup,
            //    SystemClock.ElapsedRealtime() + (startSeconds * 1000),
            //    24 * 3600 * 1000, 
            //    pendingIntent);'
            EstablishNotification(startSeconds);
        }

        public void SetRepeatedNotification(
            string title, 
            string message, 
            long startSeconds,
            int id)
        {
            //Intent repeatedIntent = this.CreateNotificationContext<NotificationReciever>(
            //        title, message, id);
            //SetPendingIntent(repeatedIntent, id);

            Intent repeatedIntent = CreateNotificationContext<NotificationReciever>(
                    title, message, id);

            SetPendingIntent(repeatedIntent, id); /* SetPendingIntentActivity()
            FOR GETTING AT THE FIRST TIME THE PLANS PAGE*/

            alarmManager.SetRepeating(
                AlarmType.ElapsedRealtimeWakeup,
                SystemClock.ElapsedRealtime() + (startSeconds * 1000),
                24 * 3600 * 1000, // every 24 hrs
                pendingIntent);
        }

        public void SetNotification(
            string title, string message, long startSeconds, string[] messages, int id)
        {
            Intent taskIntent = this.CreateNotificationContext<TaskNotificationReceiver>(
                    title, message, id);

            string mess = "";
            Array.ForEach(messages, (m) => 
            {
                mess += $"{m},";
            });

            taskIntent.PutExtra(Messages, mess);

            SetPendingIntent(taskIntent, id);

            alarmManager.SetRepeating(
                AlarmType.ElapsedRealtimeWakeup,
                SystemClock.ElapsedRealtime() + (startSeconds * 1000),
                (4 * 3600 * 1000),
                pendingIntent);
        }

        public Intent CreateNotificationContext<T>(
            string title,
            string message,
            int id
            /*long[] intervalSeconds*/)
        {
            if (!channelInitialized)
                CreateNotificationChannel();

            //if (sound) DISCOMMENT
            //    NotificationReciever.Sound = NotificationDefaults.Sound;
            //if(vibrate)
            //    NotificationReciever.Vibrate = NotificationDefaults.Vibrate;

            NotificationReciever.title = title;
            NotificationReciever.message = message;

            //NotificationReciever.isChronometer = intervalIfChron;

            alarmManager = 
                (AlarmManager)AndroidApp.Context.GetSystemService(Context.AlarmService);

            Intent intent = new Intent(
                AndroidApp.Context, typeof(T)/*typeof(NotificationReciever)*/);
            intent.PutExtra(TitleKey, title);
            intent.PutExtra(MessageKey, message);
            intent.PutExtra(ID, id);
            intent.AddFlags(ActivityFlags.ClearTop);

            return intent;

            //pendingIntent = PendingIntent.GetBroadcast(
            //    AndroidApp.Context,
            //    id, intent,
            //    PendingIntentFlags.OneShot); //PendingIntentFlags.UpdateCurrent

            //NotificationReciever.intervalSeconds = intervalSeconds;
        }

        public void SetPendingIntent(Intent intentParam, int id)
        {
            pendingIntent = PendingIntent.GetBroadcast(
               AndroidApp.Context,
               id, intentParam,
               PendingIntentFlags.OneShot);
        }

        public void SetPendingIntentActivity(Intent intentParam, int id)
        {
            pendingIntent = PendingIntent.GetActivity(
                AndroidApp.Context,
                id, intentParam,
                PendingIntentFlags.OneShot);
        }

        public static void EstablishNotification(long startSeconds)
            => alarmManager.Set(
                AlarmType.ElapsedRealtimeWakeup,
                SystemClock.ElapsedRealtime() + (startSeconds * 1000),
                /*2000, */pendingIntent);

        public void ReceiveNotification(string title, string message)
        {
            var args = new NotificationsEventArgs()
            {
                Title = title,
                Message = message,
            };

            AndroidNotificationOperations anOp = new AndroidNotificationOperations();
            anOp.RemoveNotificationTag();

            NotificationReceived?.Invoke(null, args);
        }

        void CreateNotificationChannel()
        {
            manager = (NotificationManager)AndroidApp.Context.GetSystemService(Context.NotificationService);

            if (Build.VERSION.SdkInt >= BuildVersionCodes.O)
            {
                var channelNameJava = new Java.Lang.String(channelName);
                var channel = new NotificationChannel(channelId, channelNameJava, NotificationImportance.Default)
                {
                    Description = channelDescription
                };
                manager.CreateNotificationChannel(channel);
            }

            channelInitialized = true;
        }

        public void CancelNotification(int id, BroadCastType broadcastReceiver)
        {
            Type broadcast = typeof(NotificationReciever);
            if (broadcastReceiver == BroadCastType.Task)
                broadcast = typeof(TaskNotificationReceiver);

            var cancelIntent = new Intent(
                AndroidApp.Context, broadcast/*typeof(NotificationReceiver)*/);
            cancelIntent.PutExtra(TitleKey, "");
            cancelIntent.PutExtra(MessageKey, "");
            cancelIntent.AddFlags(ActivityFlags.ClearTop);

            pendingIntent = PendingIntent.GetBroadcast(
                AndroidApp.Context,
                id, cancelIntent,
                PendingIntentFlags.UpdateCurrent);

            var alarmManager = 
                (AlarmManager)AndroidApp.Context.GetSystemService(Context.AlarmService);
            alarmManager.Cancel(pendingIntent);

            var notificationManager = 
                NotificationManagerCompat.From(Android.App.Application.Context);

            notificationManager.CancelAll();
            notificationManager.Cancel(id);
        }
    }
}