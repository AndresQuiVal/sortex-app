﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Implementations;
using Sortex.Interfaces;
using SQLite;//using SQLite.Net;
using Xamarin.Forms;

[assembly: Dependency(typeof(AndroidSQLiteService))]
namespace Sortex.Droid.Implementations
{
    public class AndroidSQLiteService : ISQLiteService
    {
        public SQLiteConnection CreateConnection()
        {
            var path = Path.Combine(
                System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal), 
                "sortexdb.sqlite");
            return new SQLiteConnection(path);
        }
    }
}