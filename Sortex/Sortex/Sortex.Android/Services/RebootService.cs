﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AndroidApp = Android.App.Application;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Classes;
using Sortex.Droid.Helpers;
using Sortex.Droid.Implementations;

namespace Sortex.Droid.Services
{
    [Service(Name = "com.companyname.Sortex.RebootService")]
    public class RebootService : Service
    {
        public override void OnCreate()
        {
            base.OnCreate();
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Toast.MakeText(this, "Sortex alarms rescheduled", ToastLength.Long).Show();

            ReestablishNotifications();

            return StartCommandResult.Sticky;
        }

        public void ReestablishNotifications()
        {
            SQLHelper sqlConnection = new SQLHelper();
            AndroidNotificationManager androidNotification =
                new AndroidNotificationManager();

            IEnumerable<ActivityItemViewModel> activities = 
                from item in SQLHelper.Connection.Table<ActivityModel>()
                select ConverterHelper.ConvertToActivityItemViewModel(item);

            var tableUser = SQLHelper.Connection.Table<UserModel>();

            //SQLHelper.Connection.Close();
            
            if (tableUser == null)
            {
                SQLHelper.Connection.Close(); // 
                return;
            }

            UserModel userModel = tableUser.ElementAt(0);

            foreach (var item in activities)
            {
                long time = 0, day = (int)DateTime.Now.DayOfWeek, counterDays = 0;
                while (true)
                {
                    if (item.DayHourDict.ContainsKey(((DayOfWeek)day).ToString()))
                    {
                        long restTime = TimeManagerHelper.GetRest(
                            item.DayHourDict[((DayOfWeek)day).ToString()])
                            + 24 * 3600 * counterDays;

                        if (restTime >= 0)
                        {
                            if (counterDays == 0 && item.IsDone) // beacuse a day hasn't passed
                                restTime += 24 * 3600;
                            time = restTime;
                            break;
                        }
                    }
                    day++;
                    counterDays++;
                    if (day >= 7)
                        day = 0;
                }

                if (time <= 0)
                    continue;

                //Set notification
                androidNotification.SetRepeatedNotification(
                    item.Name,
                    $"{userModel.ActivitiesNotificationText} {item.Name}", time,
                    Convert.ToInt32(item.Id)); // Convert.ToInt64 for long values
            }

            if (!string.IsNullOrEmpty(userModel.SleepHour))
            {
                double min30less = TimeManagerHelper.GetRest(
                        userModel.SleepHour.Substring(0, userModel.SleepHour.LastIndexOf(":")));

                if (min30less < 0)
                    min30less = (24 * 3600) + min30less;

                if ((min30less - (.5 * 3600)) > 0)
                    min30less -= (.5 * 3600);

                long newTime = (long)min30less;

                androidNotification.SetRepeatedNotification(
                    userModel.PlansNotificationsText.Substring(0, userModel.PlansNotificationsText.IndexOf(",")),
                    userModel.PlansNotificationsText.Substring(userModel.PlansNotificationsText.IndexOf(",") + 1),
                    newTime,
                    0);
            }

            SQLHelper.Connection.Close(); //
        }


        public override void OnDestroy()
        {
            base.OnDestroy();

            //Toast.MakeText(this, "Sortex has stopped for several reasons...", ToastLength.Long).Show();
        }
    }
}