﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Implementations;
using Sortex.Interfaces;
using Xamarin.Forms;
using AndroidApp = Android.App.Application;

namespace Sortex.Droid
{
    [Activity(
        Label = "Sortex",
        Theme ="@style/SplashTheme",
        MainLauncher =true,
        NoHistory =true,
        ConfigurationChanges =ConfigChanges.ScreenSize,
        LaunchMode =LaunchMode.SingleTop)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            MainActivity.NotificationIntent = Intent;
            StartActivity(typeof(MainActivity));
            //CreateNotificationFromIntent(Intent);
            base.OnCreate(savedInstanceState);

            // Create your application here
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            CreateNotificationFromIntent(intent);
        }

        public static void CreateNotificationFromIntent(Intent intent)
        {
            if (intent?.Extras != null)
            {
                try
                {
                    string title = intent.Extras.GetString(
                        AndroidNotificationManager.TitleKey);
                    string message = intent.Extras.GetString(
                        AndroidNotificationManager.MessageKey);
                    DependencyService.Get<INotificationManager>().ReceiveNotification(
                        title, message);
                }
                catch (Exception ex)
                {
                    new AlertDialog.Builder(AndroidApp.Context)
                       .SetTitle("Error")
                       .SetMessage(ex.Message)
                       .Show();
                }
            }
        }
    }
}