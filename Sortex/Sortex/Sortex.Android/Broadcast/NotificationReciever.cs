﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Implementations;
using AndroidApp = Android.App.Application;
using Sortex;
using Sortex.Helpers;
using Sortex.Models;
using Xamarin.Forms;
using Sortex.Interfaces;
//using SQLite.Net.Attributes;
//using SQLite.Net;
using SQLite;
using Sortex.ViewModels;
//using SQLite.Net.Attributes;

namespace Sortex.Droid.Broadcast
{
    [BroadcastReceiver(Enabled = true)]
    public class NotificationReciever : BroadcastReceiver
    {
        const string channelId = "default";
        public static string title = "";
        public static string message = "";
        //public int id = 0; // TODO: POSSIBLE ERROR! 
        const int pendingIntentId = 0;

        public const string TitleKey = "title";
        public const string MessageKey = "message";
        public const string ID = "id";

        public static long[] intervalSeconds;
        public Dictionary<string, string> intervalSecondsDict;

        public static NotificationDefaults Sound;
        public static NotificationDefaults Vibrate;
        public static bool isChronometer = false;

        public static long[] testTimes = { 120, 120, 60, 240, 60 };//

        private static int counter = ((int)DateTime.Now.DayOfWeek) - 1;

        int messageId = -1;

        private NotificationManager manager;
        private AlarmManager alarmManager;

        public override void OnReceive(Context context, Intent intent)
        {
            if (intent?.Extras != null)
            {
                title = intent.Extras.GetString(AndroidNotificationManager.TitleKey);
                message = intent.Extras.GetString(AndroidNotificationManager.MessageKey);
                //id = intent.Extras.GetInt(AndroidNotificationManager.ID);
            }

            var id = intent.Extras.GetInt(AndroidNotificationManager.ID);

            Intent _intent = new Intent(AndroidApp.Context, typeof(SplashActivity));
            _intent.PutExtra(TitleKey, title);
            _intent.PutExtra(MessageKey, message);
            _intent.PutExtra(ID, id); // TODO: <-- POSSIBLE ERROR

            PendingIntent pendingIntent = PendingIntent.GetActivity(
                AndroidApp.Context,
                id, _intent,
                /*PendingIntentFlags.UpdateCurrent | */
                PendingIntentFlags.OneShot);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(AndroidApp.Context, channelId)
               .SetContentIntent(pendingIntent)
               .SetContentTitle(title)
               .SetContentText(message)
               .SetLargeIcon(BitmapFactory.DecodeResource(AndroidApp.Context.Resources, Resource.Drawable.SortexAppIcon))
               .SetSmallIcon(Resource.Drawable.SortexAppIcon)
               .SetDefaults((int)NotificationDefaults.Sound | (int)NotificationDefaults.Vibrate);

            manager = 
                (NotificationManager)context.GetSystemService(
                    AndroidApp.NotificationService);

            var notification = builder.Build();
            manager.Notify(messageId, notification);

            if ((string.Equals(title, "Your plans") || 
                string.Equals(title, "Tus planes") || 
                string.Equals(title, "Vos plans")) && id == 0)
                return;

            alarmManager = (AlarmManager)AndroidApp.Context.GetSystemService(
                Context.AlarmService);

            intervalSeconds = this.GetIntervalSeconds(title);

            if (counter >= intervalSeconds.Length)
                counter = ((int)DateTime.Now.DayOfWeek) - 1;

            intervalSecondsDict = this.GetIntervalSeconds2(title);

            long time;
            int dayInt = ((int)DateTime.Now.DayOfWeek) + 1;

            try
            {
                time = GetRest(intervalSecondsDict[((DayOfWeek)dayInt).ToString()]);
            }
            catch (Exception ex)
            {
                int counterDays = 1;
                while (true)
                {
                    if (intervalSecondsDict.ContainsKey(((DayOfWeek)dayInt).ToString()))
                    {
                        time = GetRest(
                            intervalSecondsDict[((DayOfWeek)dayInt).ToString()])
                                * counterDays;
                        break;
                    }

                    dayInt++;
                    counterDays++;

                    if (dayInt >= 6) dayInt = 0;
                }
            }

            //time = Math.Abs((24 * 3600 * 1000) - (time * 1000));

            if (GetHourInDouble(intervalSecondsDict[DateTime.Now.DayOfWeek.ToString()]) >= 43200 &&
                GetHourInDouble(intervalSecondsDict[DateTime.Now.DayOfWeek.ToString()]) <= 46764) // check if condition is correct
                time += (12 * 3600 * 1000);

            //time = Math.Abs((hoursToMult * 3600 * 1000) - (time * 1000));

            alarmManager.Set(
                AlarmType.ElapsedRealtimeWakeup,
                SystemClock.ElapsedRealtime() + (time * 1000),
                GetMainIntent(intent, id));
        }

        public PendingIntent GetMainIntent(Intent ORIntent, int id)
        {
            var pendingIntent = PendingIntent.GetBroadcast(
                   AndroidApp.Context,
                   id, ORIntent,
                   PendingIntentFlags.UpdateCurrent);

            return pendingIntent;
        }

        public static long GetRest(string _hour)
        {
            var currentHourSTR = DateTime.Now.ToString("mm/dd/yyyy HH:mm:ss");
            currentHourSTR = currentHourSTR.Substring(
                currentHourSTR.IndexOf(" ") + 1, 5);

            var currentHour = GetHourInDouble(currentHourSTR);

            var mil = GetHourInDouble(_hour) + 24 * 3600/* 3600*/;

            var milHourRest = (long)mil;
            var longCurrentHourRest = (long)(currentHour);

            //if (dayInHour != 0)
              //  return (milHourRest - longCurrentHourRest) + ((24 * 3600) * dayInHour);

            return milHourRest - longCurrentHourRest;
        }

        public long[] GetIntervalSeconds(string titleIntent) // comment please
        {
            try
            {
                var config = new Implementations.Config();
                //var connection = new SQLiteConnection(
                //    config.Platform,
                //    System.IO.Path.Combine(config.DirectoryDB, "Sortex.db3"));

                var connection = new AndroidSQLiteService().CreateConnection();

                //var sqlConnection = new SQLHelper();
                var actM = connection.Table<ActivityModel>().Where(
                                    a => titleIntent.Contains(a.Name)).ToArray()[0];
                var table = ConvertToActivityItemViewModel(actM);
                return table.NotificationIntervalsArr;
            }
            catch (Exception ex)
            {
                return new long[0];
            }
        }

        public Dictionary<string, string/*long*/> GetIntervalSeconds2(string titleIntent)
        {
            var config = new Implementations.Config();
            //var connection = new SQLiteConnection(
            //    config.Platform,
            //    System.IO.Path.Combine(config.DirectoryDB, "Sortex.db3"));

            var connection = new AndroidSQLiteService().CreateConnection();

            //var sqlConnection = new SQLHelper();
            var actM = connection.Table<ActivityModel>().Where(
                                a => titleIntent.Contains(a.Name)).ToArray()[0];
            var table = ConvertToActivityItemViewModel(actM);
            return table.DayHourDict;
        }

        public static ActivityItemViewModel ConvertToActivityItemViewModel(object model)
        {
            ActivityModel amInstace = (ActivityModel)model;
            return new ActivityItemViewModel()
            {
                Id = amInstace.Id,
                Name = amInstace.Name,
                Description = amInstace.Description,
                ActivityIcon = amInstace.ActivityIcon,
                ActivityDays = amInstace.ActivityDays,
                TimingEnabled = amInstace.TimingEnabled,
                PersonalizedIcon = amInstace.PersonalizedIcon,
                IsDone = amInstace.IsDone,
                Times = amInstace.Times,
                DoneTimes = amInstace.DoneTimes,
                NotificationIntervals = amInstace.NotificationIntervals,
            };
        }

        public class ActivityModel
        {
            [PrimaryKey, AutoIncrement]
            public int? Id { get; set; }
            [NotNull, Unique]
            public string Name { get; set; }
            public string Description { get; set; }
            public string ActivityIcon { get; set; }
            [NotNull]
            public string ActivityDays { get; set; }
            public bool TimingEnabled { get; set; }
            public byte[] PersonalizedIcon { get; set; } = null;
            public bool IsDone { get; set; }
            public string Times { get; set; } // ListTimes prop gets the times in a correct format
            public int DoneTimes { get; set; }
            public string NotificationIntervals { get; set; }
            public string WeekChronTimes { get; set; }
        }

        public class ActivityItemViewModel : ActivityModel
        {
            [Ignore]
            public long[] NotificationIntervalsArr
            {
                get // how to parse all elems without a for or a foreach or while??
                {
                    string[] splitted = this.NotificationIntervals.Split(char.Parse(","));
                    long[] newItems = TimeToLongParser(
                        splitted.Skip(0).Take(splitted.Length - 1).ToArray());

                    //for (int i = 0; i < splitted.Length; i++)
                    //    newItems[i] = long.Parse(splitted[i]);
                    return newItems;
                }
            }

            //[Ignore]
            //public Dictionary<string, long> DayHourDict
            //{
            //    get
            //    {
            //        Dictionary<string, long> dict = new Dictionary<string, long>();
            //        var separated = this.ActivityDays.Split(char.Parse(","));
            //        foreach (var item in separated)
            //        {
            //            if (item != "")
            //            {
            //                var key = LanguagesDayParser(
            //                    item.Substring(0, item.IndexOf(":")));
            //                var value = GetHourInDouble(item.Substring(item.IndexOf(":") + 1));
            //                if (value != 0)
            //                    dict.Add(key, Convert.ToInt64(value));
            //            }
            //        }
            //        return dict;
            //    }
            //}

            [Ignore]
            public Dictionary<string, string> DayHourDict
            {
                get
                {
                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    var separated = this.ActivityDays.Split(char.Parse(","));
                    foreach (var item in separated)
                    {
                        if (item != "")
                        {
                            var key = LanguagesDayParser(
                                item.Substring(0, item.IndexOf(":")));
                            var value = item.Substring(item.IndexOf(":") + 1);
                            dict.Add(key, value);
                        }
                    }
                    return dict;
                }
            }
        }

        public static string LanguagesDayParser(string day) //SUPER BAD IMPLEMENTATION AND METHOD!
        {
            if (string.Equals(day, "Lunes") || string.Equals(day, "Lundi"))
                return "Monday";
            else if (string.Equals(day, "Martes") || string.Equals(day, "Mardi"))
                return "Tuesday";
            else if (string.Equals(day, "Miércoles") || string.Equals(day, "Mercredi"))
                return "Wednesday";
            else if (string.Equals(day, "Jueves") || string.Equals(day, "Jeudi"))
                return "Thursday";
            else if (string.Equals(day, "Viernes") || string.Equals(day, "Vendredi"))
                return "Friday";
            else if (string.Equals(day, "Sábado") || string.Equals(day, "Samedi"))
                return "Saturday";
            else if (string.Equals(day, "Domingo") || string.Equals(day, "Dimanche"))
                return "Sunday";
            return day;
        }

        public static long[] TimeToLongParser(string[] times)
        {
            long[] timesInLong = new long[times.Length];

            for (int i = 0; i < times.Length; i++)
            {
                string hour = times[i].Substring(0, times[i].IndexOf(":"));
                string min = times[i].Substring(times[i].IndexOf(":") + 1);

                //rule of three
                int minToDouble = (100 * int.Parse(min)) / 60;

                long timeLong =
                    (long)((double.Parse($"{hour}.{minToDouble}")) *
                        3600);
                if (timeLong <= 0)
                    timeLong = 3600;

                timesInLong[i] = timeLong;
            }
            return timesInLong;
        }

        public static long GetHourInDouble(string _hour) // can be commented
        {
            int hour = int.Parse(_hour.Substring(0, _hour.IndexOf(":")));
            int minHour =
                (100 * int.Parse(_hour.Substring(_hour.IndexOf(":") + 1))) / 60;
            //double min = (100 * minHour) / 60;
            var result = (long)(double.Parse($"{hour}.{minHour}") * 3600);
            //if (result <= 0)
            //    return 3600;
            return result;
        }
    }
}