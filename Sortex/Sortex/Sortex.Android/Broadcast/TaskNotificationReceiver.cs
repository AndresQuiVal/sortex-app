﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Implementations;
using AndroidApp = Android.App.Application;

namespace Sortex.Droid.Broadcast
{
    public class TaskNotificationReceiver : BroadcastReceiver
    {
        const string channelId = "default";
        public static string title = "";
        public static string message = "";
        public static string messages = "";
        //public int id = 0;
        const int pendingIntentId = 0;

        public const string TitleKey = "title";
        public const string MessageKey = "message";

        public static long[] intervalSeconds;
        public Dictionary<string, string> intervalSecondsDict;

        public static NotificationDefaults Sound;
        public static NotificationDefaults Vibrate;
        public static bool isChronometer = false;

        public static long[] testTimes = { 120, 120, 60, 240, 60 };

        int messageId = -1;
        private int counter = DateTime.Now.Hour;

        private NotificationManager manager;

        public override void OnReceive(Context context, Intent intent)
        {
            if (intent?.Extras != null)
            {
                title = intent.Extras.GetString(AndroidNotificationManager.TitleKey);
                message = intent.Extras.GetString(AndroidNotificationManager.MessageKey);
                messages = intent.Extras.GetString(AndroidNotificationManager.Messages);
            }

            int id = intent.Extras.GetInt(AndroidNotificationManager.ID);

            //

            counter += 4;

            var randomMessages = messages.Split(char.Parse(","));

            Random rnd = new Random();
            int index = rnd.Next(randomMessages.Length);

            if (counter >= 24)
            {
                this.CancelTaskNotification(id); // POSSIBLE ERROR;
                counter = 0;
                return;
            }
            message = randomMessages[index];

            //

            Intent _intent = new Intent(AndroidApp.Context, typeof(MainActivity));
            _intent.PutExtra(TitleKey, title);
            _intent.PutExtra(MessageKey, message);

            PendingIntent pendingIntent = PendingIntent.GetActivity(
                AndroidApp.Context,
                -1, _intent,
                PendingIntentFlags.OneShot);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(AndroidApp.Context, channelId)
               .SetContentIntent(pendingIntent)
               .SetContentTitle(title)
               .SetContentText(message)
               .SetLargeIcon(BitmapFactory.DecodeResource(AndroidApp.Context.Resources, Resource.Drawable.SortexAppIcon))
               .SetSmallIcon(Resource.Drawable.SortexAppIcon)
               .SetDefaults((int)NotificationDefaults.Sound | (int)NotificationDefaults.Vibrate);

            manager =
                (NotificationManager)context.GetSystemService(
                    AndroidApp.NotificationService);

            var notification = builder.Build();
            manager.Notify(messageId, notification);
        }

        public void CancelTaskNotification(int id)
        {
            var intent = new Intent(
                AndroidApp.Context, typeof(TaskNotificationReceiver)/*typeof(NotificationReceiver)*/);
            intent.PutExtra(TitleKey, "");
            intent.PutExtra(MessageKey, "");
            intent.AddFlags(ActivityFlags.ClearTop);

            var pendingIntent = PendingIntent.GetBroadcast(
                AndroidApp.Context,
                id, intent,
                PendingIntentFlags.UpdateCurrent);

            var alarmManager =
                (AlarmManager)AndroidApp.Context.GetSystemService(Context.AlarmService);

            alarmManager.Cancel(pendingIntent);

            var notificationManager = 
                NotificationManagerCompat.From(Android.App.Application.Context);

            notificationManager.CancelAll();
            notificationManager.Cancel(id);
        }
    }
}