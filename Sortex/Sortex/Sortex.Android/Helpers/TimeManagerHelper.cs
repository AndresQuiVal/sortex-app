﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Sortex.Droid.Helpers
{
    public static class TimeManagerHelper
    {
        public static long GetRest(string _hour)
        {
            var currentHourSTR = DateTime.Now.ToString("mm/dd/yyyy HH:mm:ss");
            currentHourSTR = currentHourSTR.Substring(
                currentHourSTR.IndexOf(" ") + 1, 5);

            var currentHour = GetHourInDouble(currentHourSTR);

            var mil = GetHourInDouble(_hour);

            var milHourRest = (long)mil;
            var longCurrentHourRest = (long)(currentHour);

            return milHourRest - longCurrentHourRest;
        }

        public static long GetRest(string _hour, int dayInHour = 0)
        {
            var currentHourSTR = DateTime.Now.ToString("mm/dd/yyyy HH:mm:ss");
            currentHourSTR = currentHourSTR.Substring(
                currentHourSTR.IndexOf(" ") + 1, 5);

            var currentHour = GetHourInDouble(currentHourSTR);

            var mil = GetHourInDouble(_hour) * 3600;

            var milHourRest = (long)mil;
            var longCurrentHourRest = (long)(currentHour * 3600);

            if (dayInHour != 0)
                return (milHourRest - longCurrentHourRest) + ((24 * 3600) * dayInHour);

            return milHourRest - longCurrentHourRest;
        }

        public static long GetHourInDouble(string _hour)
        {
            int hour = int.Parse(_hour.Substring(0, _hour.IndexOf(":")));
            int minHour =
                (100 * int.Parse(_hour.Substring(_hour.IndexOf(":") + 1))) / 60;

            return (long)(double.Parse($"{hour}.{minHour}") * 3600);
        }
    }
}