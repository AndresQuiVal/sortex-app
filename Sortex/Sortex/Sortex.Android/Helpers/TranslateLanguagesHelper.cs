﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Sortex.Droid.Helpers
{
    public static class TranslateLanguagesHelper
    {
        public static string LanguagesDayParser(string day) //SUPER BAD IMPLEMENTATION AND METHOD!
        {
            if (string.Equals(day, "Lunes") || string.Equals(day, "Lundi"))
                return "Monday";
            else if (string.Equals(day, "Martes") || string.Equals(day, "Mardi"))
                return "Tuesday";
            else if (string.Equals(day, "Miércoles") || string.Equals(day, "Mercredi"))
                return "Wednesday";
            else if (string.Equals(day, "Jueves") || string.Equals(day, "Jeudi"))
                return "Thursday";
            else if (string.Equals(day, "Viernes") || string.Equals(day, "Vendredi"))
                return "Friday";
            else if (string.Equals(day, "Sábado") || string.Equals(day, "Samedi"))
                return "Saturday";
            else if (string.Equals(day, "Domingo") || string.Equals(day, "Dimanche"))
                return "Sunday";
            return day;
        }
    }
}