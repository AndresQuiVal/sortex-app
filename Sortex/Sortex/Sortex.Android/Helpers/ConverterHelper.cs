﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Classes;

namespace Sortex.Droid.Helpers
{
    public class ConverterHelper
    {
        public static ActivityItemViewModel ConvertToActivityItemViewModel(object model)
        {
            ActivityModel amInstace = (ActivityModel)model;
            return new ActivityItemViewModel()
            {
                Id = amInstace.Id,
                Name = amInstace.Name,
                Description = amInstace.Description,
                ActivityIcon = amInstace.ActivityIcon,
                ActivityDays = amInstace.ActivityDays,
                TimingEnabled = amInstace.TimingEnabled,
                PersonalizedIcon = amInstace.PersonalizedIcon,
                IsDone = amInstace.IsDone,
                Times = amInstace.Times,
                DoneTimes = amInstace.DoneTimes,
                NotificationIntervals = amInstace.NotificationIntervals
            };
        }
    }
}