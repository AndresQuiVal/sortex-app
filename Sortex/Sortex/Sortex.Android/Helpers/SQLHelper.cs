﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Implementations;
using SQLite;
//using SQLite.Net;

namespace Sortex.Droid.Helpers
{
    public class SQLHelper
    {
        public static SQLiteConnection Connection { get; set; }
        public SQLHelper()
        {
            var config = new Implementations.Config();
            //Connection = new SQLiteConnection(
            //    config.Platform,
            //    System.IO.Path.Combine(config.DirectoryDB, "Sortex.db3"));
            Connection = new AndroidSQLiteService().CreateConnection();
        }
    }
}