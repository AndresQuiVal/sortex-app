﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Sortex.Droid.Helpers;
using SQLite;
//using SQLite.Net.Attributes;

namespace Sortex.Droid.Classes
{
    public class ActivityItemViewModel : ActivityModel
    {
        [Ignore]
        public Dictionary<string, string> DayHourDict
        {
            get
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                var separated = this.ActivityDays.Split(char.Parse(","));
                foreach (var item in separated.Take(separated.Length - 1))
                {
                    if (item != "")
                    {
                        var key = TranslateLanguagesHelper.LanguagesDayParser(
                            item.Substring(0, item.IndexOf(":")));
                        var value = item.Substring(item.IndexOf(":") + 1);
                        dict.Add(key, value);
                    }
                }
                return dict;
            }
        }
    }
}