﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
//using SQLite.Net.Attributes;

namespace Sortex.Droid.Classes
{
    public class UserModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] ProfileImage { get; set; }
        public string CurrentDayTask { get; set; }
        public bool ToDoListAllowed { get; set; }
        public string SleepHour { get; set; }
        public int TotalTasks { get; set; }
        public int TasksDone { get; set; }
        public string ActivitiesNotificationText { get; set; }
        public string PlansNotificationsText { get; set; } //both title and context are separated by comma 
        public bool HasEvaluatedStats { get; set; }
        [Ignore]
        public TimeSpan SleepHourTSFormat
        {
            get
            {
                if (this.sleepHourTSFormat == TimeSpan.Zero)
                {
                    if (string.IsNullOrEmpty(this.SleepHour))
                        this.sleepHourTSFormat = new TimeSpan(0, 0, 0);
                    else
                        this.sleepHourTSFormat = TimeSpan.Parse(this.SleepHour);
                }
                return this.sleepHourTSFormat;
            }
            set { this.sleepHourTSFormat = value; }
        }

        private TimeSpan sleepHourTSFormat = TimeSpan.Zero;
    }
}