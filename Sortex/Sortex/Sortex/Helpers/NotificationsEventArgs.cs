﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sortex.Helpers
{
    public class NotificationsEventArgs : EventArgs
    {
        public string Title { get; set; }
        public string Message { get; set; }
    }
}
