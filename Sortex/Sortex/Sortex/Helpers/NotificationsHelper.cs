﻿using Plugin.LocalNotifications;
using Sortex.Enums;
using Sortex.Interfaces;
using Sortex.Models;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace Sortex.Helpers
{
    public static class NotificationsHelper // change to non static class
    {
        public static void SetNotificationPlugin(int hours)
        {
            var counter = 0;
            Random randomNum = new Random();
            string[] randomNotificationMessages = new string[5]
            {
                    Languages.RandomNotification1,
                    Languages.RandomNotification2,
                    Languages.RandomNotification3,
                    Languages.RandomNotification4,
                    Languages.RandomNotification5
            };
            int hoursRemaining = 24 - DateTime.Now.Hour;
            for (int i = hours; i <= hoursRemaining; i += hours, counter++)
            {
                int messageIndex = randomNum.Next(randomNotificationMessages.Length);
                CrossLocalNotifications.Current.Show(
                    Languages.PendingTasksTitle,
                    randomNotificationMessages[messageIndex],
                    counter,
                    DateTime.Now.AddHours(i));
            }
        }

        public static void SetNotificationForTasks(/*int hours*/)
        {
            //Random randomNum = new Random();
            INotificationManager notificationManager = 
                DependencyService.Get<INotificationManager>();

            string[] randomNotificationMessages = new string[5]
            {
                    Languages.RandomNotification1,
                    Languages.RandomNotification2,
                    Languages.RandomNotification3,
                    Languages.RandomNotification4,
                    Languages.RandomNotification5
            };

            //string randomNotificationMsgString = "";

            //Array.ForEach(randomNotificationMessages, (arg) => 
            //{ randomNotificationMsgString += $"{arg},"; });

            //int messageIndex = randomNum.Next(randomNotificationMessages.Length);

            notificationManager.SetNotification(
                Languages.PendingTasksTitle,
                randomNotificationMessages[0],
                4 * 3600 * 1000,
                randomNotificationMessages,
                -1);
        }

        public static void CancelNotificationPlugin(int limit)
        {
            for (int i = 0; i < limit; i++)
            {
                try
                {
                    CrossLocalNotifications.Current.Cancel(i);
                }
                catch (Exception)
                {
                    break;
                }
            }
        }

        public static async void SetRepeatedNotificationDependency( // can use notificationManager of this constructor
                INotificationManager notificationManager,
                string title,
                string context,
                string hour,
                string[] notificationIntervals,
                int id)
        {
            var rest = TimeManagerHelper.GetRest(hour);

            if (rest >= 0)
            {
                notificationManager.SetReminderNotification(
                    title, context, rest, id, true);
                return;
            }

            SQLHelper sqlConnection = new SQLHelper();
            ActivityItemViewModel activity = ConverterHelper.ConvertToActivityItemViewModel(
                SQLHelper.Connection.Table<ActivityModel>().Where(
                a => a.Name == title).ToArray()[0]);

            string currentDay = ((DayOfWeek)(((int)DateTime.Now.DayOfWeek) + 1)).ToString();

            KeyValuePair<int, string> nextDayHour =
                await TimeManagerHelper.GetHourOfAvailableDay(activity.DayHourDict);

            rest = TimeManagerHelper.GetRest(nextDayHour.Value, nextDayHour.Key);

            notificationManager.SetReminderNotification(
                    title, context, rest, id, true);
            //... TODO: CONTINUE
        }

        public static void CancelNotification(
            INotificationManager notificationManager, int id, 
            BroadCastType broadcastReceiver) =>
            notificationManager.CancelNotification(id, broadcastReceiver);
    }
}
