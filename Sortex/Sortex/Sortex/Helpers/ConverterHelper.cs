﻿using Sortex.Models;
using Sortex.ViewModels;
using SQLite;
using System;
using System.Collections.Generic;

namespace Sortex.Helpers
{
    public static class ConverterHelper //TODO: class violating the open/close principle
    {
        public static IEnumerable<TaskItemViewModel> ConvertToTaskItemViewModel
            (TableQuery<TaskModel> model) // SQLite.Net.TableQuery<TaskModel> model
        {
            var enumerable = new List<TaskItemViewModel>();
            foreach (var item in model)
            {
                enumerable.Add(new TaskItemViewModel()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    PriorityLevel = item.PriorityLevel,
                    Subtasks = item.Subtasks,
                    IsDoneTask = item.IsDoneTask,
                    IsNotRepeatedTask = item.IsNotRepeatedTask,
                    IsPlan = item.IsPlan,
                });
            }
            return enumerable;
        }

        public static TaskModel ConvertToTaskModel(object task)
        {
            TaskItemViewModel taskItem = (TaskItemViewModel)task;
            return new TaskModel()
            {
                Id = taskItem.Id,
                Name = taskItem.Name,
                Description = taskItem.Description,
                PriorityLevel = taskItem.PriorityLevel,
                Subtasks = taskItem.Subtasks,
                IsDoneTask = taskItem.IsDoneTask,
                IsNotRepeatedTask = taskItem.IsNotRepeatedTask,
                IsPlan = taskItem.IsPlan,
            };
        }

        public static ResponseModel ConvertToUserModel
            (List<UserModel> model)
        {
            var enumerable = new List<UserModel>();
            foreach (var item in model)
            {
                enumerable.Add(new UserModel()
                {
                    Id = item.Id,
                    UserName = item.UserName,
                    ProfileImage = item.ProfileImage,
                    CurrentDayTask = item.CurrentDayTask,
                    ToDoListAllowed = item.ToDoListAllowed,
                    SleepHour = item.SleepHour,
                    TotalTasks = item.TotalTasks,
                    TasksDone = item.TasksDone,
                    HasEvaluatedStats = item.HasEvaluatedStats,
                    ActivitiesNotificationText = item.ActivitiesNotificationText,
                    PlansNotificationsText = item.PlansNotificationsText
                }
                );
            }
            try
            {
                return new ResponseModel()
                {
                    IsSuccess = true,
                    Model = enumerable[0],
                };
            }
            catch
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                };
            }
        }

        public static ActivityModel ConvertToActivityModel(object model)
        {
            var aivmInstace = (ActivityItemViewModel) model;
            return new ActivityModel()
            {
                Id = aivmInstace.Id,
                Name = aivmInstace.Name,
                Description = aivmInstace.Description,
                ActivityIcon = aivmInstace.ActivityIcon,
                ActivityDays = aivmInstace.ActivityDays,
                //TimingEnabled = aivmInstace.TimingEnabled,
                PersonalizedIcon = aivmInstace.PersonalizedIcon,
                IsDone = aivmInstace.IsDone,
                Times = aivmInstace.Times,
                DoneTimes = aivmInstace.DoneTimes,
                NotificationIntervals = aivmInstace.NotificationIntervals,
                WeekChronTimes = aivmInstace.WeekChronTimes,
            };
        }

        public static ActivityItemViewModel ConvertToActivityItemViewModel(object model)
        {
            ActivityModel amInstace = (ActivityModel)model;
            return new ActivityItemViewModel()
            {
                Id = amInstace.Id,
                Name = amInstace.Name,
                Description = amInstace.Description,
                ActivityIcon = amInstace.ActivityIcon,
                ActivityDays = amInstace.ActivityDays,
                //TimingEnabled = amInstace.TimingEnabled,
                PersonalizedIcon = amInstace.PersonalizedIcon,
                IsDone = amInstace.IsDone,
                Times = amInstace.Times,
                DoneTimes = amInstace.DoneTimes,
                NotificationIntervals = amInstace.NotificationIntervals,
                WeekChronTimes = amInstace.WeekChronTimes,
            };
        }

        public static List<ActivityItemViewModel> ConverToListActivityItemViewModel
            (List<ActivityModel> modelList)
        {
            var enumerable = new List<ActivityItemViewModel>();
            foreach (var item in modelList)
            {
                enumerable.Add(new ActivityItemViewModel()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    ActivityIcon = item.ActivityIcon,
                    ActivityDays = item.ActivityDays,
                    //TimingEnabled = item.TimingEnabled,
                    PersonalizedIcon = item.PersonalizedIcon,
                    IsDone = item.IsDone,
                    Times = item.Times,
                    DoneTimes = item.DoneTimes,
                    NotificationIntervals = item.NotificationIntervals,
                    WeekChronTimes = item.WeekChronTimes,
                });
            }
            return enumerable;
        }
    }
}
