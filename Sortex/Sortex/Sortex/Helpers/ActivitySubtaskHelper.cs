﻿using Sortex.Models;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Sortex.Helpers
{
    public static class ActivitySubtaskHelper
    {
        public static void AddSubtask(
            ref ObservableCollection<SubtaskModel> subtaskItems,
            ref int listViewHeight)
        {
            subtaskItems.Add(new SubtaskModel());

            //Activate getter of ListViewHeight // otherwise use setter
            SetListViewLength(ref subtaskItems, ref listViewHeight);
        }

        public static void SetListViewLength(
            ref ObservableCollection<SubtaskModel> subtaskItems, 
            ref int listViewHeight) =>
            listViewHeight = subtaskItems.Count *
                MainViewModel.GetInstance().ListViewSubtaskHeight;

        public static void LoadSubtaskItem(
            ref ObservableCollection<SubtaskModel> subtaskItems,
            ref int listViewHeight)
        {
            subtaskItems = new ObservableCollection<SubtaskModel>();
            subtaskItems.Add(new SubtaskModel());
            listViewHeight = MainViewModel.GetInstance().ListViewSubtaskHeight; //60
        }
    }
}
