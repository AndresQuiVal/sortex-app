﻿using Sortex.Models;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sortex.Helpers
{
    public static class TimeManagerHelper
    {
        public static long[] TimeToLongParser(string[] times)
        {
            long[] timesInLong = new long[7];
            for (int i = 0; i < times.Length; i++)
            {
                string hour = times[i].Substring(0, times[i].IndexOf(":"));
                string min = times[i].Substring(times[i].IndexOf(":") + 1);

                //rule of three
                int minToDouble = (100 * int.Parse(min)) / 60;

                long timeLong =
                    (long)((double.Parse($"{hour}.{minToDouble}")) *
                        3600);
                if (timeLong <= 0)
                    timeLong = 3600;

                timesInLong[i] = timeLong;
            }
            return timesInLong;
        }

        public static double GetHourInDouble(string _hour)
        {
            int minHour;
            int hour = int.Parse(_hour.Substring(0, _hour.IndexOf(":")));
            if(_hour.IndexOf(":") == _hour.LastIndexOf(":"))
                minHour =
                    (100 * int.Parse(_hour.Substring(_hour.IndexOf(":") + 1))) / 60;
            else
                minHour =
                    (100 * int.Parse(_hour.Substring(_hour.IndexOf(":") + 1, 2))) / 60;
            //double min = (100 * minHour) / 60;
            var result = double.Parse($"{hour}.{minHour}");
            if (result <= 0)
                return 1;
            return double.Parse($"{hour}.{minHour}");
        }

        public static string GetDoubleInHour(double doubleHour)
        {
            //    string[] splitted = doubleHour.ToString().Split(char.Parse("."));
            //    return $"{splitted[0]}:{(60 * int.Parse(splitted[1])) / 100}";
            doubleHour = doubleHour / 3600;
            List<string> hourMinutes;

            hourMinutes = doubleHour.ToString().Split(char.Parse(".")).ToList();
            if (hourMinutes.Count == 1)
                hourMinutes.Add("0");

            string doubleHourMinutes = hourMinutes[1];
            int doubleToMin = (60 * int.Parse(doubleHourMinutes)) / 100;

            string doubleToMinString = doubleToMin.ToString();

            if (doubleToMinString.Length == 1) doubleToMinString += "0";

            return $"{hourMinutes[0]}:{doubleToMinString}";
        }

        public static int GetConstValueDay(string day)
        {
            bool canParseDay = Enum.TryParse(day, out DayOfWeek myDay);
            if (!canParseDay)
                return -1;
            return (int)myDay;
        }

        public static string AddHours(string hour, int timeToAdd)
        {
            string[] hourMin = hour.Split(char.Parse(":"));
            //int theNewHour = 
            //    Math.Abs(int.Parse(hourMin[0]) - Convert.ToInt32(DateTime.Now.Hour));
            int newHour = int.Parse(hourMin[0]) + timeToAdd;
            int min = int.Parse(hourMin[1]);
            return $"{newHour}:{min}";
        }

        public static long GetRest(string _hour, int dayInHour = 0)
        {
            var currentHourSTR = DateTime.Now.ToString("mm/dd/yyyy HH:mm:ss");
            currentHourSTR = currentHourSTR.Substring(
                currentHourSTR.IndexOf(" ") + 1, 5);

            var currentHour = GetHourInDouble(currentHourSTR);

            var mil = GetHourInDouble(_hour) * 3600;

            var milHourRest = (long)mil;
            var longCurrentHourRest = (long)(currentHour * 3600);

            if(dayInHour != 0)
                return (milHourRest - longCurrentHourRest) + ((24 * 3600) * dayInHour);

            return milHourRest - longCurrentHourRest;
        }

        public static string GetSubstractionHours(string hour1, string hour2)
        {
            string hourSubstraction = "";
            string[] hour1Splitted = hour1.Split(char.Parse(":"));
            string[] hour2Splitted = hour2.Split(char.Parse(":"));

            for (int i = 0; i < hour1Splitted.Length; i++)
                hourSubstraction += $"{Math.Abs(int.Parse(hour2Splitted[i]) - int.Parse(hour1Splitted[i]))}:";
            hourSubstraction.Remove(hourSubstraction.Length - 1);

            return hourSubstraction;
        }

        public static string GetSubstractionHourLongString(string hour1, string hour2)
        {
            return (TimeSpan.Parse(hour1) - TimeSpan.Parse(hour2)).ToString();
        }

        public static string GetRestToHourFormat(string hour) // TODO: implement GetRestToHourFormat()
        {
            //3:15 - 6:18 = 3:03
            return string.Empty;
        }

        public static async Task<KeyValuePair<int, string>> GetHourOfAvailableDay(Dictionary<string, string> dictTimes)
        {
            string time = "";
            int counterDays = 1, dayInt = ((int)DateTime.Now.DayOfWeek) + 1;

            await Task.Run(() =>
            {
                while (true)
                {
                    if (dictTimes.ContainsKey(
                        MainViewModel.GetInstance().GetDayEnglishToLanguages(
                            ((DayOfWeek)dayInt).ToString())))
                    {
                        time = dictTimes[MainViewModel.GetInstance().GetDayEnglishToLanguages(
                            ((DayOfWeek)dayInt).ToString())];
                        break;
                    }

                    dayInt++;
                    counterDays++;

                    if (dayInt >= 6) dayInt = 0;
                }
            });

            //daysCounter = counterDays;
            return new KeyValuePair<int, string>(counterDays, time);
        }
    }
}
