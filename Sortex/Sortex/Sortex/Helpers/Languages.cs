﻿using Sortex.Interfaces;
using Sortex.Resources;
using System.Globalization;
using Xamarin.Forms;

namespace Sortex.Helpers
{
    public static class Languages
    {
        static Languages()
        {
            CultureInfo ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            Resource.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }

        #region Properties - words to translate
        public static string ToDoTitleText { get { return Resource.ToDoTitleText; } }
        public static string NewTaskText { get { return Resource.NewTaskText; } }
        public static string HightPriorText { get { return Resource.HightPriorText; } }
        public static string MediumPriorText { get { return Resource.MediumPriorText; } }
        public static string LessPriorityText { get { return Resource.LessPriorityText; } }
        public static string DoneText { get { return Resource.DoneText; } }
        public static string NotDoneText { get { return Resource.NotDoneText; } }
        public static string TaskNameText { get { return Resource.TaskNameText; } }
        public static string TaskPriorityLevelText { get { return Resource.TaskPriorityLevelText; } }
        public static string DefaultTaskNameText { get { return Resource.DefaultTaskNameText; } }
        public static string TaskOptionsDetailsText { get { return Resource.TaskOptionsDetailsText; } }//
        public static string TaskOptionsEditText { get { return Resource.TaskOptionsEditText; } }
        public static string TaskOptionsDeleteText { get { return Resource.TaskOptionsDeleteText; } }
        public static string CancelText { get { return Resource.CancelText; } }
        public static string Task { get { return Resource.Task; } }
        public static string CorrectText { get { return Resource.CorrectText; } }
        public static string AddedCorrectrlySubstring { get { return Resource.AddedCorrectrlySubstring; } }
        public static string TaskAdded { get { return Resource.TaskAdded; } }
        public static string MondayDay { get { return Resource.MondayDay; } }
        public static string TuesdayDay { get { return Resource.TuesdayDay; } }
        public static string WednesdayDay { get { return Resource.WednesdayDay; } }
        public static string ThursdayDay { get { return Resource.ThursdayDay; } }
        public static string FridayDay { get { return Resource.FridayDay; } }
        public static string SaturdayDay { get { return Resource.SaturdayDay; } }
        public static string SundayDay { get { return Resource.SundayDay; } }
        public static string PendingTasksTitle { get { return Resource.PendingTasksTitle; } }
        public static string RandomNotification1 { get { return Resource.RandomNotification1; } }
        public static string RandomNotification2 { get { return Resource.RandomNotification2; } }
        public static string RandomNotification3 { get { return Resource.RandomNotification3; } }
        public static string RandomNotification4 { get { return Resource.RandomNotification4; } }
        public static string RandomNotification5 { get { return Resource.RandomNotification5; } }
        public static string TaskOptionsText { get { return Resource.TaskOptionsText; } }
        public static string TaskNotDoneTitle { get { return Resource.TaskNotDoneTitle; } }
        public static string TitleImageConfigSelection { get { return Resource.TitleImageConfigSelection; } }
        public static string TakeImageTitle { get { return Resource.TakeImageTitle; } }
        public static string GalleryImageTitle { get { return Resource.GalleryImageTitle; } }
        public static string ResetImageTitle { get { return Resource.ResetImageTitle; } }
        public static string UserNamePromtQuestion { get { return Resource.UserNamePromtQuestion; } }
        public static string UserNamePromtTitle { get { return Resource.UserNamePromtTitle; } }
        public static string FilterTasksTitle { get { return Resource.FilterTasksTitle; } }
        public static string FilterViewAllText { get { return Resource.FilterViewAllText; } }
        public static string RemoveButtonText { get { return Resource.RemoveButtonText; } }
        public static string NoIconText { get { return Resource.NoIconText; } }
        public static string DoExcerciseText { get { return Resource.DoExcerciseText; } }
        public static string StudyText { get { return Resource.StudyText; } }
        public static string WorkText { get { return Resource.WorkText; } }
        public static string DoShoppingText { get { return Resource.DoShoppingText; } }
        public static string ReadText { get { return Resource.ReadText; } }
        public static string GoToSchoolText { get { return Resource.GoToSchoolText; } }
        public static string PlayText { get { return Resource.PlayText; } }
        public static string EatText { get { return Resource.EatText; } }
        public static string UploadIconText { get { return Resource.UploadIconText; } }
        public static string ActivityOptionsText { get { return Resource.ActivityOptionsText; } }
        public static string ActivityDoneText { get { return Resource.ActivityDoneText; } }
        public static string EstablishNotDoneActivitiesText { get { return Resource.EstablishNotDoneActivitiesText; } }
        public static string EstablishedAsNotDoneText { get { return Resource.EstablishedAsNotDoneText; } }
        public static string YouText { get { return Resource.YouText; } }
        public static string PlansText { get { return Resource.PlansText; } }
        public static string TimeText { get { return Resource.TimeText; } }
        public static string StopText { get { return Resource.StopText; } }
        public static string SaveTimeText { get { return Resource.SaveTimeText; } }
        public static string YesText { get { return Resource.YesText; } }
        public static string NoText { get { return Resource.NoText; } }
        public static string TimeRegularText { get { return Resource.TimeRegularText; } }
        public static string ActivityTimingText { get { return Resource.ActivityTimingText; } }
        public static string ModifiedActivityText { get { return Resource.ModifiedActivityText; } }
        public static string ViewActivitiesBy { get { return Resource.ViewActivitiesBy; } }
        public static string AddActivityText { get { return Resource.AddActivityText; } }
        public static string ProfileEditedContextPart1 { get { return Resource.ProfileEditedContextPart1; } }
        public static string ProfileEditedContextPart2 { get { return Resource.PorfileEditedContextPart2; } }
        public static string ProfileEditedText { get { return Resource.ProfileEditedText; } }
        public static string StartTimerText { get { return Resource.StartTimerText; } }
        public static string StopTimerText { get { return Resource.StopTimerText; } }
        public static string ViewTimeText { get { return Resource.ViewTimeText; } }
        public static string RecomendedResolText { get { return Resource.RecomendedResolText; } }
        public static string WeekMeanTimesText { get { return Resource.WeekMeanTimesText; } }
        public static string SaveTimeContext { get { return Resource.SaveTimeContext; } }
        public static string TimeFinishedText { get { return Resource.TimeFinishedText; } }
        public static string TimeWasText { get { return Resource.TimeWasText; } }
        public static string Timing { get { return Resource.TimingText; } }
        public static string HasStartedTimingText { get { return Resource.HasStartedTimingText; } }
        public static string DoTheActivityText { get { return Resource.DoTheActivityText; } }
        public static string TasksDoneText { get { return Resource.TasksDoneText; } }
        public static string TasksNotDoneText { get { return Resource.TasksNotDoneText; } }
        public static string TasksCreatedText { get { return Resource.TasksCreatedText; } }
        public static string NotTimedText { get { return Resource.NotTimedText; } }
        public static string TotalPercWeek { get { return Resource.TotalPercWeek; } }
        public static string EmptyPlanNamesText { get { return Resource.EmptyPlanNamesText; } }
        public static string PlansAddedText { get { return Resource.PlansAddedText; } }
        public static string WelcomeAppText { get { return Resource.WelcomeAppText; } }
        public static string StartText { get { return Resource.StartText; } }
        public static string AppTextIntro { get { return Resource.AppTextIntro; } }
        public static string EstablishTomorrowPlansText { get { return Resource.EstablishTomorrowPlansText; } }
        public static string YourPlansText { get { return Resource.YourPlansText; } }
        #endregion
    }
}
