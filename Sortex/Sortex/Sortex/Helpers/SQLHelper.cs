﻿using Sortex.Interfaces;
using Sortex.Models;
//using SQLite.Net;
using SQLite;
using System;
using System.IO;
using System.Linq;
using Xamarin.Forms;

namespace Sortex.Helpers
{
    public class SQLHelper
    {
        #region Properties
        //public static SQLiteConnection Connection { get; set; } SQLite.Net namespace
        public static SQLiteConnection Connection { get; set; }
        #endregion
        #region Constructors
        public SQLHelper()
        {
            //var config = DependencyService.Get<IConfig>();
            //Connection = new SQLiteConnection(
            //    config.Platform,
            //    Path.Combine(config.DirectoryDB, "Sortex.db3"));
            //Connection.CreateTable<ActivityModel>();
            Connection = DependencyService.Get<ISQLiteService>().CreateConnection();
            Connection.CreateTable<UserModel>();
            Connection.CreateTable<ActivityModel>();
            Connection.CreateTable<TaskModel>();
        }
        #endregion

        #region Methods
        public ResponseModel Insert<T>(T model) where T : class
        {
            try
            {
                Connection.Insert(model);
                Connection.Commit();
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }

            return new ResponseModel()
            {
                IsSuccess = true,
                Message = Languages.TaskAdded,
                Model = model,
            }; 
        }

        public ResponseModel Update<T>(T model) where T : class
        {
            try
            {
                Connection.Update(model);
                Connection.Commit();
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
            return new ResponseModel()
            {
                IsSuccess = true,
            };
        }

        public ResponseModel Delete<T>(T model) where T : class
        {
            try
            {
                Connection.Delete(model);
                Connection.Commit();
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
            return new ResponseModel()
            {
                IsSuccess = true,
            };
        }

        public ResponseModel DeleteAll<T>() where T : class
        {
            try
            {
                Connection.DeleteAll<T>();
                Connection.Commit();
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
            return new ResponseModel()
            {
                IsSuccess = true,
            };
        }

        public T GetLast<T>() where T : new() // public ctor
        {
            return Connection.Table<T>().ToArray()[Connection.Table<T>().Count() - 1]; ;
        }

        public void CloseConnection() =>
            Connection.Close(); 

        #endregion
    }
}
