﻿using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sortex.Infrastructure
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; }

        public InstanceLocator()
        {
            Main = new MainViewModel();
        }
    }
}
