﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sortex.Models
{
    public class DayTimeActivityModel
    {
        public string Day { get; set; }
        public string Time { get; set; }
    }
}
