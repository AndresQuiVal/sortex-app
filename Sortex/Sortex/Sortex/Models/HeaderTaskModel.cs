﻿using System.Collections.ObjectModel;

namespace Sortex.Models
{
    public class HeaderTaskModel : ObservableCollection<TaskModel>
    {
        //private string actionSheetResponse;
        public string Header { get; set; }
        public ObservableCollection<TaskModel> TaskModels => this;

        //public ICommand GridExpandCommand
        //{
        //    get { return new RelayCommand(this.GridExpand); }
        //}

        //public async void GridExpand()
        //{
        //    // IsVisible = !IsVisible;
        //    actionSheetResponse = await Application.Current.MainPage.DisplayActionSheet(
        //        String.Format($"Options of task: {this.TaskModels.Name}"),
        //        Languages.CancelText,
        //        null,
        //        Languages.TaskOptionsDetailsText,
        //        Languages.TaskOptionsDeleteText,
        //        Languages.TaskOptionsEditText);
        //    if (actionSheetResponse == Languages.TaskOptionsDetailsText)
        //    { }
        //    else if (actionSheetResponse == Languages.TaskOptionsDeleteText)
        //    {
        //        SQLHelper sqlConnection = new SQLHelper();
        //        sqlConnection.Delete(ConverterHelper.ConvertToTaskModel(this));
        //        MainViewModel.GetInstance().LoadDBContentTaskModel();
        //    }
        //    else if (actionSheetResponse == Languages.TaskOptionsEditText)
        //    {
        //        MainViewModel.GetInstance().DetailsTask = new DetailsTaskViewModel(this);
        //        await Application.Current.MainPage.Navigation.PushModalAsync
        //            (new NavigationPage(new DetailsTaskPage(this.Subtasks))
        //            { BarBackgroundColor = Xamarin.Forms.Color.FromHex("#18E1BF") });
        //    }
        //}
    }
}
