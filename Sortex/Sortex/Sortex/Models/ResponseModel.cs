﻿namespace Sortex.Models
{
    public class ResponseModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public object Model { get; set; }
    }
}
