﻿//using SQLite.Net.Attributes;
using SQLite;

namespace Sortex.Models
{
    public class ActivityModel
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        [NotNull, Unique]
        public string Name { get; set; }
        public string Description { get; set; }
        public string ActivityIcon { get; set; }
        [NotNull]
        public string ActivityDays { get; set; }
        //public bool TimingEnabled { get; set; }
        public byte[] PersonalizedIcon { get; set; } = null;
        public bool IsDone { get; set; }
        public string Times { get; set; } // ListTimes prop gets the times in a correct format
        public int DoneTimes { get; set; }
        public string NotificationIntervals { get; set; }
        public string WeekChronTimes { get; set; }
        public ActivityModel()
        {

        }
    }
}
