﻿//using SQLite.Net.Attributes;//using SQLite;
using System;
using SQLite;

namespace Sortex.Models
{
    public class UserModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string UserName { get; set; }
        public byte[] ProfileImage { get; set; }
        public string CurrentDayTask { get; set; }
        public bool ToDoListAllowed { get; set; }
        public string SleepHour { get; set; }
        public int TotalTasks { get; set; }
        public int TasksDone { get; set; }
        public bool HasEvaluatedStats { get; set; }
        public string ActivitiesNotificationText { get; set; }
        public string PlansNotificationsText { get; set; } //both title and context are separated by comma 
        [Ignore]
        public TimeSpan SleepHourTSFormat
        {
            get
            {
                if (this.sleepHourTSFormat == TimeSpan.Zero)
                {
                    if (string.IsNullOrEmpty(this.SleepHour))
                        this.sleepHourTSFormat = new TimeSpan(0, 0, 0);
                    else
                        this.sleepHourTSFormat = TimeSpan.Parse(this.SleepHour);
                }
                return this.sleepHourTSFormat;
            }
            set { this.sleepHourTSFormat = value; }
        }

        private TimeSpan sleepHourTSFormat = TimeSpan.Zero;
        //
    }
}
