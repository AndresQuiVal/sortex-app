﻿using GalaSoft.MvvmLight.Command;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.Models
{
    public partial class SubtaskModel : BaseViewModel
    {
        #region Actions
        public static Action<SubtaskModel> DeleteSubtaskAction;
        #endregion

        #region Class fields
        private string text;
        private string textId;
        private string defText = "";
        #endregion

        #region Properties
        public static int counter { get; set; }
        public string TextId
        {
            get
            {
                this.textId = "0";
                try
                {
                    //if (this.text == null)
                    //{
                    var item = /*(KeyValuePair<int, string>)*/TextDict.Where(
                        t => t.Value == this.defText).ToList()[0];
                    if (!item.Equals(default(KeyValuePair<int, string>)))
                        textId = item.Key.ToString();
                    //}
                }
                catch (Exception ex)
                {
                    textId = counter.ToString();
                }
                return textId;
            }
            set
            {
                SetValue(ref this.textId, value);
            }
        }

        public string Text
        {
            get
            {
                return this.text;
            }
            set
            {
                int id = int.Parse(this.TextId);
                this.text = value;
                defText = value;
                TextDict[id] = value;
            }
        }
        #endregion

        #region Constructors
        static SubtaskModel()
        {
            Instances = new List<SubtaskModel>();
            TextDict = new Dictionary<int, string>();
        }

        public SubtaskModel()
        {
            Instances.Add(this);
            var guid = Guid.NewGuid();
            defText =
                String.Format($"{guid.ToString().Substring(0, guid.ToString().Length - 15)}6U1D");
            TextDict.Add(++counter, defText);

            //if (MainViewModel.GetInstance().IsPlanSender)

        }
        #endregion

        #region Commands
        public ICommand DeleteSubtaskCommand
        {
            get { return new RelayCommand(this.DeleteSubtask); }
        }

        public ICommand ShowActionSheetDeleteCommand
        {
            get { return new RelayCommand(async () =>
            {
                var alertResponse =
                    await Application.Current.MainPage.DisplayAlert(
                        "Delete plan",
                        "Want to delete the plan?",
                        "Yes",
                        "No");
                if (alertResponse)
                    this.DeleteSubtask();
            }); }
        }
        #endregion

        #region Methods
        public void DeleteSubtask()
        {
            //Call the delegate action to execute
            DeleteSubtaskAction(this);

            TextDict.Remove(int.Parse(this.TextId));

            //if (MainViewModel.GetInstance().IsPlanSender)

            this.ReconstructItems();
            counter--;
        }
        public void ReconstructItems() // IMPLEMENT BETTER THIS METHOD
        {
            var keys = TextDict.Keys.ToArray();
            var values = TextDict.Values.ToArray();
            if (keys.Length > 0)
            {
                Dictionary<int, string> newItems = new Dictionary<int, string>();
                if (keys[0] != 1) keys[0] = 1;

                //TextDict = new Dictionary<int, string>(); //COMMENT
                for (int i = 0; i < keys.Length - 1; i++)
                {
                    if (keys.Length == 1) continue; //DISCOMMENT

                    if ((keys[i] + 1) != keys[i + 1])
                    {
                        for (int ii = i + 1; ii < keys.Length; ii++)
                            keys[ii] -= 1;
                        break; //DISCOMMENT
                    }
                }
                for (int i = 0; i < keys.Length; i++)
                    newItems.Add(keys[i], values[i]);
                TextDict = newItems;
            }
            //Delete current instance from Instance collection
            Instances.Remove(this);

            //Activate Setter of TextId property
            for (int i = 0; i < Instances.Count; i++)
                Instances[i].TextId = keys[i].ToString();
        }
        #endregion

        #region Static properties
        public static Dictionary<int, string> TextDict { get; set; }
        public static List<SubtaskModel> Instances { get; set; }
        #endregion
    }
}
