﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sortex.Models
{
    public class TimeModel
    {
        public string Id { get; set; }
        public string Time { get; set; }

        public override string ToString()
        {
            return $"{this.Id}. {this.Time}";
        }
    }
}
