﻿using Sortex.Helpers;
//using SQLite.Net.Attributes;
using SQLite;
using Xamarin.Forms;

namespace Sortex.Models
{
    public class TaskModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [NotNull]
        public string Name { get; set; }
        public string Description { get; set; }
        [NotNull]
        public string PriorityLevel { get; set; }
        public string Subtasks { get; set; }
        public bool IsNotRepeatedTask { get; set; }
        public bool IsDoneTask { get; set; }
        public bool IsPlan { get; set; }
        [Ignore]
        public ImageSource TaskIcon
        {
            get
            {
                if (PriorityLevel == Languages.HightPriorText)//"Hight Priority")
                    return "HightPriorityIcon";
                else if (PriorityLevel == Languages.MediumPriorText)//"Medium Priority")
                    return "NotThatHightPriority";
                else if (PriorityLevel == Languages.PlansText)
                    return "PlanIcon";
                return "NotImportantProprityIcon";
            }
        }

        [Ignore]
        public int PriorityLevelIndex
        {
            set
            {
                if (value == 0)
                    this.PriorityLevel = Languages.HightPriorText;//"Hight Priority";
                else if (value == 1)
                    this.PriorityLevel = Languages.MediumPriorText;//"Medium Priority";
                else
                    this.PriorityLevel = Languages.LessPriorityText;//"Less Priority";
            }
        }
    }
}
