﻿using Xamarin.Forms;

namespace Sortex.Models
{
    public class IDButton : Button
    {
        public int ButtonID { get; set; }
    }
}
