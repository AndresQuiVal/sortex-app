﻿using GalaSoft.MvvmLight.Command;
using Sortex.ViewModels;
using Sortex.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.Models
{
    public class DaysActivityModel : BaseViewModel
    {
        #region Constructors
        static DaysActivityModel() =>
            DaysAMCollection = new List<DaysActivityModel>();

        public DaysActivityModel()
        {
            DaysAMCollection.Add(this);

            MessagingCenter.Subscribe<ContentPage, string>(this, "TimeChanged",
                (sender, args) =>
            {
                if (ItsFirstTime)
                {
                    ItsFirstTime = false;

                    foreach (DaysActivityModel instance in DaysAMCollection)
                    {
                        if (!instance.IsChecked)
                            continue;

                        instance.Time = TimeSpan.Parse(args);

                        if (DayHourDict.ContainsKey(instance.Day))
                            DayHourDict[instance.Day] = args;
                    }
                }

            });
        }

        #endregion

        private string hourEntered = "";
        private TimeSpan time;
        private bool? isCheckedSelectedItem = null;
        public string Day { get; set; }
        public bool IsChecked { get; set; } // ?
        public static bool ItsFirstTime { get; set; }
        public bool? IsCheckedSelectedItem
        {
            get
            {
                if (this.isCheckedSelectedItem == null)
                {
                    if (!string.IsNullOrEmpty(DaysString) && !string.IsNullOrEmpty(this.Day))
                    {
                        if (DaysString.Contains(this.Day))
                        {
                            try
                            {
                                Time = 
                                    TimeSpan.Parse(DayHourDict[this.Day]);
                                this.isCheckedSelectedItem = true;
                            }
                            catch
                            {
                                this.isCheckedSelectedItem = false;
                            }

                        }
                    }
                }
                return this.isCheckedSelectedItem;
                //return false;
            }
            set
            {
                if (value == true)
                {
                    if (!string.IsNullOrEmpty(this.Day))
                    {
                        if (!DayHourDict.ContainsKey(this.Day))
                        {
                            var timeShortened = this.Time.ToString().Substring(0,
                               this.Time.ToString().LastIndexOf(":"));
                            DayHourDict.Add(this.Day, timeShortened);
                        }
                    }
                }
                else
                    DayHourDict.Remove(this.Day);
                SetValue(ref this.isCheckedSelectedItem, value);
            }
        }
        public TimeSpan Time
        {
            get
            {
                //if (this.time)
                //{
                    //if (this.IsCheckedSelectedItem)
                    //{
                    //    return TimeSpan.Parse(DayHourDict[this.Day]);
                    //}
                //}
                return this.time;
            }
            set
            {
                SetValue(ref this.time, value);

                if (DayHourDict.ContainsKey(this.Day))
                    DayHourDict[this.Day] = value.ToString().Substring(0,
                            value.ToString().LastIndexOf(":"));
            }
        }
        public string HourEntered
        {
            get
            {
                //if (hourEntered.Length == 2 && hourEntered.Contains(":") == false)
                //    hourEntered += ":";
                if (hourEntered.Length == 4 && !hourEntered.Contains(":"))
                    hourEntered.Insert(2, ":");
                return hourEntered;
            }
            set { SetValue(ref this.hourEntered, value); }
        }
        public ICommand DayEnabledCommand
        {
            get { return new RelayCommand(this.DayEnabled); }
        }

        public async void DayEnabled()
        {
            await Application.Current.MainPage.DisplayAlert(
                "Current day:",
                this.Day,
                "CANCEL");
        }

        public static List<DaysActivityModel> DaysAMCollection { get; set; }
        public static Dictionary<string, string> DayHourDict { get; set; }
        public static string DaysString { get; internal set; }
    }
}
