﻿//using SQLite.Net.Attributes;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sortex.Models
{
    public class PlansModel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [NotNull]
        public string Name { get; set; }
    }
}
