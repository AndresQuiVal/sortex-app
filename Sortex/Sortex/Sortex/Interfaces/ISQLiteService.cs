﻿using System;
using System.Collections.Generic;
using System.Text;
/*using SQLite.Net;*/using SQLite;

namespace Sortex.Interfaces
{
    public interface ISQLiteService
    {
        SQLiteConnection CreateConnection();
    }
}
