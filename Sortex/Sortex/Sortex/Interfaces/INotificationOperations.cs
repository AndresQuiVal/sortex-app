﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sortex.Interfaces
{
    public interface INotificationOperations
    {
        void RemoveNotificationTag();
    }
}
