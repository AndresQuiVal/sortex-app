﻿using Sortex.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Sortex.Interfaces
{
    public interface INotificationManager
    {
        event EventHandler NotificationReceived;
        void Initialize();
        void SetReminderNotification( // used in activities
            string title, 
            string message, 
            long startSeconds,
            int id,
            bool isRepeated = false
            /*long[] intervalSeconds = null*/);

        void SetRepeatedNotification( // used in user plans
            string title,
            string message,
            long startSeconds,
            int id);

        void SetNotification( // used in tasks
            string title, 
            string message, 
            long startSeconds,
            string[] messages,
            int id);

        void ReceiveNotification(string title, string message);

        void CancelNotification(int id, BroadCastType broadcastReceiver); 
         // SO ANDROID SPECIALIZED METHOD!!
    }
}
