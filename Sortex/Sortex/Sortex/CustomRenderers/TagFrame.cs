﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Sortex.CustomRenderers
{
    public class TagFrame : Frame
    {
        public static new readonly BindableProperty CornerRadiusProperty = 
            BindableProperty.Create(
                nameof(TagFrame), 
                typeof(CornerRadius), 
                typeof(TagFrame));

        public new CornerRadius CornerRadius
        {
            get => (CornerRadius)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        public TagFrame()
        {
            base.CornerRadius = 0;
        }
    }
}
