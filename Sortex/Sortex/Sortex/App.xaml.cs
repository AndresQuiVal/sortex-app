﻿using Plugin.LocalNotifications;
using Sortex.Helpers;
using Sortex.Models;
using Sortex.ViewModels;
using Sortex.Views;
using System;
using System.Collections.ObjectModel;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MvvmHelpers;
using System.Linq;
using System.Threading.Tasks;

namespace Sortex
{
    public partial class App : Application
    {
        private SQLHelper sqlConnection;
        public bool IsInitialized { get; set; }
        public App()
        {
            InitializeComponent();

            IsInitialized = true;
            //Create the tables
            sqlConnection = new SQLHelper();
            //SQLHelper.Connection.DropTable<UserModel>();
            //SQLHelper.Connection.DropTable<TaskModel>();
            //SQLHelper.Connection.DropTable<ActivityModel>();

            SQLHelper.Connection.CreateTable(typeof(UserModel));
            SQLHelper.Connection.CreateTable(typeof(TaskModel));
            SQLHelper.Connection.CreateTable(typeof(ActivityModel));
            sqlConnection.CloseConnection();
            //
            ExecuteUserValidation();
        }


        protected override void OnStart() //TODO: the actual error stands here!!!!
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }


        public async void ExecuteUserValidation()
        {
            SQLHelper sqlConnection = new SQLHelper();

            var table = SQLHelper.Connection.Table<UserModel>();
            var responseModel = ConverterHelper.ConvertToUserModel(table.ToList()); //edit SQLHelper for this kind of situations
            
            if (responseModel.IsSuccess)
            {
                var userModel = (UserModel)responseModel.Model;
                bool isNewDay = false;
                if (userModel.CurrentDayTask != DateTime.Now.ToShortDateString())
                {
                    isNewDay = true;

                    //Update tasks

                    var tableTasks = SQLHelper.Connection.Table<TaskModel>().ToArray();
                    int len = tableTasks.Length; // controlling the no of tasks deleted;
                    foreach (var model in tableTasks)
                    {
                        if (model.IsNotRepeatedTask && !model.IsPlan) // model.IsNotRepeatedTask && !model.IsPlan
                        {
                            sqlConnection.Delete(model);
                            len--;
                            continue;
                        }
                        if (model.IsPlan)
                        {
                            model.IsPlan = false;
                            sqlConnection.Update(model);
                        }
                    }
                    //tableTasks = SQLHelper.Connection.Table<TaskModel>().ToArray();
                    if (len >= 1)
                        NotificationsHelper.SetNotificationForTasks();

                    //Update activities
                    var tableActivities = SQLHelper.Connection.Table<ActivityModel>().
                        ToList();

                    tableActivities.ForEach(a =>
                    {
                        if (a.IsDone) a.IsDone = false;
                        a.Times = string.Empty;
                    });

                    string command = "UPDATE Activities SET isDone = false WHERE isDone = true";

                    //eval if stats have been evaluated

                    if ((int)DateTime.Now.DayOfWeek == 0)
                        userModel.HasEvaluatedStats = true;
                    else
                    {
                        if (userModel.HasEvaluatedStats)
                        {
                            userModel.HasEvaluatedStats = false;
                            userModel.TasksDone = 0;
                            userModel.TotalTasks = 0;
                            foreach (var activity in tableActivities)
                            {
                                activity.DoneTimes = 0;
                                activity.WeekChronTimes = string.Empty;
                            }
                        }
                    }


                    SQLHelper.Connection.UpdateAll(tableActivities);

                    //Update User
                    userModel.CurrentDayTask = DateTime.Now.ToShortDateString();

                    MainViewModel.GetInstance().LoadDBContentTaskModel();
                }

                if (!string.Equals(
                    userModel.ActivitiesNotificationText,
                    Languages.DoTheActivityText) || isNewDay) // we dont use and operator beacuse if one changes the others too
                {
                    if (!string.Equals(
                        userModel.ActivitiesNotificationText,
                        Languages.DoTheActivityText))
                    {
                        userModel.ActivitiesNotificationText = Languages.DoTheActivityText;
                        userModel.PlansNotificationsText =
                            $"{Languages.YourPlansText},{Languages.EstablishTomorrowPlansText}";
                    }

                    //SQLHelper sqlConnection2 = new SQLHelper();
                    sqlConnection = new SQLHelper();
                    var response = sqlConnection.Update(userModel);
                    if (!response.IsSuccess)
                        ShowMessage("Error", response.Message);
                    //sqlConnection2.CloseConnection();
                }
            }
            else
            {
                MainViewModel.GetInstance().InitialGuide = new InitialGuideViewModel();

                MainPage = new HomeTabbedPage();

                await Current.MainPage.Navigation.PushModalAsync(
                    new InitialCarouselPage());
            }
            sqlConnection.CloseConnection();

            if (MainPage == null)
                MainPage = new HomeTabbedPage();
        }

        public async void ShowMessage(string title, string message)
        {
            await Current.MainPage.DisplayAlert(
                title,
                message,
                Languages.CancelText);
        }
    }
}
