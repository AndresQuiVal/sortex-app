﻿using GalaSoft.MvvmLight.Command;
using MvvmHelpers;
using Sortex.Helpers;
using Sortex.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class TasksDoneViewModel : BaseViewModel
    {
        #region Attributes
        private ObservableCollection<Grouping<string, TaskItemViewModel>> taskDoneCollection;
        private bool presentedItemsVisible;
        #endregion

        #region Properties
        public ObservableCollection<Grouping<string, TaskItemViewModel>> TasksDoneCollection
        {
            get { return this.taskDoneCollection; }
            set { SetValue(ref this.taskDoneCollection, value); }
        }
        public bool PresentedItemsVisible
        {
            get { return this.presentedItemsVisible; }
            set { SetValue(ref this.presentedItemsVisible, value); }
        }
        #endregion

        #region Constructors
        public TasksDoneViewModel()
        {
            this.LoadTasksDone();
        }
        #endregion

        #region Methods
        public void LoadTasksDone()
        {
            bool presentedItems = false;
            PresentedItemsVisible = false;
            TasksDoneCollection = new ObservableCollection<Grouping<string, TaskItemViewModel>>();
            SQLHelper sqlConnection = new SQLHelper();
            var table = ConverterHelper.ConvertToTaskItemViewModel(SQLHelper.Connection.Table<TaskModel>());
            var listData = table.Where(l => l.IsDoneTask);
            foreach (var item in new string[]
            { Languages.HightPriorText, Languages.MediumPriorText, Languages.LessPriorityText })
            {
                var sorted = from tasks in listData
                                orderby tasks.PriorityLevel // orberby is a keyword of System.Linq namespace
                                group tasks by tasks.PriorityLevel into taskList
                                select new Grouping<string, TaskItemViewModel>(taskList.Key, taskList);
                List<Grouping<string, TaskItemViewModel>> listItems = sorted.Where(t => t.Key == item).ToList();
                foreach (var item2 in listItems)
                {
                    TasksDoneCollection.Add(item2);
                    presentedItems = true;
                }
            }
            if(!presentedItems) PresentedItemsVisible = true;
        }

        public async void PopCurrentPage() => await Application.Current.MainPage.Navigation.PopModalAsync();
        #endregion
    }
}
