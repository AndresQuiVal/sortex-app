﻿using GalaSoft.MvvmLight.Command;
using Plugin.LocalNotifications;
using Sortex.Helpers;
using Sortex.Models;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace Sortex.ViewModels
{
    public class NewTaskViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<String> pickerItems;
        private ObservableCollection<SubtaskModel> subtaskItems;
        private StackLayout subtasksLayout;
        private string listTest;
        private string taskName;
        private string taskDescription;
        private string taskNameText = Languages.DefaultTaskNameText;
        private bool checkBoxState;
        private int selectedItem;
        //private int counter = 0;
        private int listViewHeight;
        #endregion

        #region Properties
        public ObservableCollection<String> PickerItems
        {
            get { return this.pickerItems; }
            set { SetValue(ref this.pickerItems, value); }
        }

        public ObservableCollection<SubtaskModel> SubtasksItems
        {
            get { return this.subtaskItems; }
            set { SetValue(ref this.subtaskItems, value); }
        }

        public StackLayout SubtasksLayout
        {
            get { return this.subtasksLayout; }
            set { SetValue(ref this.subtasksLayout, value); }
        }

        public string FirstSubtask
        {
            get { return MainViewModel.GetInstance().SubtasksList[0]; }
            set { MainViewModel.GetInstance().SubtasksList[0] = value; }
        }

        public string ListTest // delete the property, just test / taste case / s
        {
            get { return this.listTest; }
            set { SetValue(ref this.listTest, value); }
        }

        public string TaskName
        {
            get { return this.taskName; }
            set
            {
                SetValue(ref this.taskName, value);
                TaskNameText = value;
            }
        }

        public string TaskNameText
        {
            get
            {
                if (this.taskNameText != "")
                    return this.taskNameText;
                return Languages.DefaultTaskNameText;
            }
            set { SetValue(ref this.taskNameText, value); }
        }

        public string TaskDescription
        {
            get { return this.taskDescription; }
            set { SetValue(ref this.taskDescription, value); }
        }

        public int SelectedItem
        {
            get { return this.selectedItem; }
            set { SetValue(ref this.selectedItem, value); }
        }

        public bool CheckBoxState
        {
            get { return this.checkBoxState; }
            set { SetValue(ref this.checkBoxState, value); }
        }

        public int ListViewHeight
        {
            get
            {
                return this.listViewHeight;
            }
            set { SetValue(ref this.listViewHeight, value); }
        }
        #endregion

        #region Constructors
        public NewTaskViewModel()
        {
            this.CheckBoxState = true;
            this.LoadPickerItems();

            /*Give the Action my method to execute in the Delete command 
             * and load first subtask item */
            SubtaskModel.DeleteSubtaskAction = DeleteSubtaskItems;
            ActivitySubtaskHelper.LoadSubtaskItem(
                ref subtaskItems, ref listViewHeight);
            ChangeSubtaskHelperProperties();
            //
        }
        #endregion

        #region Commands
        public ICommand DismissCommand
        {
            get { return new RelayCommand(this.Dismiss); }
        }

        public ICommand AddNewTaskCommand
        {
            get { return new RelayCommand(this.AddNewTask); }
        }

        public ICommand AddSubtaskCommand
        {
            get { return new RelayCommand(this.AddSubtask); }
        }
        #endregion

        #region Methods
        public void LoadPickerItems()
        {
            PickerItems = new ObservableCollection<string>()
            {
                Languages.HightPriorText,
                Languages.MediumPriorText,
                Languages.LessPriorityText,
            };
        }

        public void Dismiss() => Application.Current.MainPage.Navigation.PopModalAsync();

        public async void AddNewTask()
        {
            //Add the registration to Local SQLite DB - Tasks Table
            string subtasks = "";

            Dictionary<int, string>.ValueCollection valuesCollection =
                SubtaskModel.TextDict.Values;

            foreach (var value in valuesCollection) 
            {
                if(!string.IsNullOrWhiteSpace(value) && !value.EndsWith("6U1D"))
                    subtasks += String.Format($"{value}^"); //,
            } 

            SQLHelper sqliteConnection = new SQLHelper();

            var table = SQLHelper.Connection.Table<TaskModel>().Where(
                t => t.IsDoneTask == false);

            //Insert into local db
            var model = sqliteConnection.Insert(new TaskModel()
            {
                Name = TaskName,
                Description = TaskDescription,
                PriorityLevel = PickerItems[SelectedItem],
                Subtasks = subtasks,
                IsNotRepeatedTask = CheckBoxState,
            });

            //veryfing if the operation was succesfully done
            if (!model.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    model.Message,
                    Languages.CancelText);
                return;
            }

            //notifying that the operation was done
            await Application.Current.MainPage.DisplayAlert(
                    Languages.CorrectText,
                    model.Message,
                    Languages.CancelText);

            //push notifications according to the condition set as following
            int countTable = table.Count();
            if (table.Count() == 1)
                NotificationsHelper.SetNotificationForTasks();
                //NotificationsHelper.SetNotificationPlugin(4); TODO: DISCOMMENT

            var user = SQLHelper.Connection.Table<UserModel>().ToArray()[0];
            user.TotalTasks++;
            sqliteConnection.Update(user);

            sqliteConnection.CloseConnection();
            await Application.Current.MainPage.Navigation.PopModalAsync();

            var main = MainViewModel.GetInstance();

            //Clear the controls from the Subtask layout that contains all the props
            main.Profile.LoadProfileActivityInfo();
            //TaskItemViewModel.GetTableTasksCount(true);
            main.LoadDBContentTaskModel();
        }

        public void AddSubtask()
        {
            ActivitySubtaskHelper.AddSubtask(ref this.subtaskItems, ref this.listViewHeight);
            ChangeSubtaskHelperProperties();
        }

        public void ChangeSubtaskHelperProperties()
        {
            OnPropertyChanged("SubtaskItems");
            OnPropertyChanged("ListViewHeight");
        }

        public void DeleteSubtaskItems(SubtaskModel instance)
        {
            this.SubtasksItems.Remove(instance);
            this.ListViewHeight =
                this.SubtasksItems.Count *
                MainViewModel.GetInstance().ListViewSubtaskHeight;
        }
        #endregion
    }
}
