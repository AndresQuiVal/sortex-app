﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Sortex.Helpers;
using Sortex.Models;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class PlansViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<TaskItemViewModel> plansCollection;
        #endregion

        #region Properties
        public ObservableCollection<TaskItemViewModel> PlansCollection
        {
            get { return this.plansCollection; }
            set { SetValue(ref this.plansCollection, value); }
        }
        #endregion

        #region Constructors
        public PlansViewModel()
        {
            this.LoadPlans();
            this.RegisterMessages();
            MainViewModel.GetInstance().IsPlanSender = false;
        }
        public void LoadPlans()
        {
            SQLHelper sqlConnection = new SQLHelper();
            var tasksTable = SQLHelper.Connection.Table<TaskModel>().Where(
                t => t.PriorityLevel == Languages.PlansText && t.IsPlan);
            PlansCollection = new ObservableCollection<TaskItemViewModel>(
                ConverterHelper.ConvertToTaskItemViewModel(tasksTable));
            sqlConnection.CloseConnection();
        }

        public void RegisterMessages()
        {
            MessagingCenter.Subscribe<TaskItemViewModel>(
                this, Languages.PlansText, (sender) => LoadPlans());
        }
        #endregion
    }
}
