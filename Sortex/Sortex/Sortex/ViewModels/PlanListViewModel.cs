﻿using GalaSoft.MvvmLight.Command;
using Sortex.Helpers;
using Sortex.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class PlanListViewModel : BaseViewModel
    {
        #region Class fields
        private int listViewHeight;
        private ObservableCollection<SubtaskModel> planItemsCollection;
        #endregion

        #region Properties
        public int ListViewHeight
        {
            get { return this.listViewHeight; }
            set { SetValue(ref this.listViewHeight, value); }
        }

        public ObservableCollection<SubtaskModel> PlanItemsCollection
        {
            get { return this.planItemsCollection; }
            set { SetValue(ref this.planItemsCollection, value); }
        }
        public bool IsVisibleDoneItems { get; set; } = false; // Change the property implementation
        #endregion

        #region Constructors
        public PlanListViewModel()
        {
            MainViewModel.GetInstance().IsPlanSender = true;
            //Load first SubtaskModel instance
            SubtaskModel.DeleteSubtaskAction = this.DeleteSubtaskItem;
            ActivitySubtaskHelper.LoadSubtaskItem(
                ref planItemsCollection, ref listViewHeight);
            ChangeSubtaskHelperProperties();
        }
        #endregion

        #region Commands
        public ICommand AddPlanCommand
        {
            get { return new RelayCommand(() =>
            {
                ActivitySubtaskHelper.AddSubtask(
                    ref this.planItemsCollection,
                    ref this.listViewHeight);
                ChangeSubtaskHelperProperties();
            }); }
        }

        public ICommand SaveChangesCommand
        {
            get { return new RelayCommand(this.SaveChanges); }
        }
        #endregion

        #region Methods
        public void ChangeSubtaskHelperProperties()
        {
            OnPropertyChanged("PlanItemsCollection");
            OnPropertyChanged("ListViewHeight");
        }

        public void DeleteSubtaskItem(SubtaskModel instance)
        {
            PlanItemsCollection.Remove(instance);
            ListViewHeight =
                PlanItemsCollection.Count *
                MainViewModel.GetInstance().ListViewSubtaskHeight; // ?
            ChangeSubtaskHelperProperties();
        }

        public async void SaveChanges()
        {
            SQLHelper sqlConnection = new SQLHelper();
            string[] tasks = SubtaskModel.TextDict.Values.ToArray();

            bool areEqual = false;
            foreach (var item in tasks)
            {
                if (item.Length == 0)
                {
                    areEqual = true;
                    break;
                }
                else if (item.Length >= 4)
                {
                    if (item.Substring(item.Length - 4) == "6U1D")
                    {
                        areEqual = true;
                        break;
                    }
                }
            }
            if (areEqual)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    Languages.EmptyPlanNamesText,
                    Languages.CancelText);
                return;
            }
            foreach(var task in tasks)
            {
                if (string.IsNullOrWhiteSpace(task))
                    continue;
                sqlConnection.Insert(new TaskModel()
                {
                    Name = task,
                    IsPlan = true,
                    PriorityLevel = Languages.PlansText,
                    IsNotRepeatedTask = true,
                });
            }
            sqlConnection.CloseConnection();
            await Application.Current.MainPage.DisplayAlert(
                Languages.CorrectText,
                Languages.PlansAddedText,
                Languages.CancelText);

            MainViewModel.GetInstance().Task.ReadTasksFromDB();
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }
        #endregion
    }
}
