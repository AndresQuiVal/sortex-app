﻿using GalaSoft.MvvmLight.Command;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Sortex.Helpers;
using Sortex.Models;
using Sortex.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        #region Delegates
        public delegate void DelegateNTPPointer();
        public DelegateNTPPointer DelegatePointer;
        #endregion

        #region Class fields
        private List<string> subtasksList;
        private int activityCollectionLength;
        #endregion

        #region Properties
        public ProfilePage ProfilePage { get; set; }
        public StatsViewModel Stats { get; set; }
        public HomeViewModel Home { get; set; }
        public TaskViewModel Task { get; set; }
        public NewTaskViewModel NewTask { get; set; }
        public DetailsTaskViewModel DetailsTask { get; set; }
        public ProfileViewModel Profile { get; set; }
        public TasksDoneViewModel TasksDone { get; set; }
        public EditProfileViewModel EditProfile { get; set; }
        public InitialGuideViewModel InitialGuide { get; set; }
        public AddActivityViewModel AddActivity { get; set; }
        public EditActivityViewModel EditActivity { get; set; }
        public DoneActivitiesViewModel DoneActivities { get; set; }
        public ActivityTimingViewModel ActivityTiming { get; set; }
        public WeekTimesActivityViewModel WeekTimesActivity { get; set; }
        public PlanListViewModel PlanList { get; set; }
        public PlansViewModel Plans { get; set; }
        public bool IsPlanSender { get; set; } // PLansViewModel to load the plans
        public int ListViewSubtaskHeight { get; set; } = 63;
        public List<string> SubtasksList
        {
            get { return this.subtasksList; }
            set { SetValue(ref this.subtasksList, value); }
        }
        public string TasteProp { get; set; } // delete the property
        public string Day
        {
            get
            {
                var message = "";
                var currentDay = DateTime.Now.DayOfWeek.ToString();
                switch (currentDay)
                {
                    case "Monday":
                        message += Languages.MondayDay;
                        break;
                    case "Tuesday":
                        message += Languages.TuesdayDay;
                        break;
                    case "Wednesday":
                        message += Languages.WednesdayDay;
                        break;
                    case "Thursday":
                        message += Languages.ThursdayDay;
                        break;
                    case "Friday":
                        message += Languages.FridayDay;
                        break;
                    case "Saturday":
                        message += Languages.SaturdayDay;
                        break;
                    case "Sunday":
                        message += Languages.SundayDay;
                        break;
                }
                return message;
            }
        }

        public int DayNumber
        {
            get { return (int)DateTime.Now.Day; }
        }

        public ObservableCollection<string> ActivityItems = new ObservableCollection<string>()
        {
            Languages.NoIconText, Languages.DoExcerciseText, Languages.StudyText,
            Languages.WorkText, Languages.DoShoppingText, Languages.ReadText,
            Languages.GoToSchoolText, Languages.PlayText, Languages.EatText,
            Languages.UploadIconText,
        };

        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            Home = new HomeViewModel();
            Task = new TaskViewModel();
            //Profile = new ProfileViewModel();
            SubtasksList = new List<string>() { "" };
        }
        #endregion

        #region SingleTon pattron
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
                instance = new MainViewModel();
            return instance;
        }
        #endregion

        #region Commands
        public ICommand DismissCommand
        {
            get { return new RelayCommand(this.Dismiss); }
        }

        public StackLayout SubtaskLayout { get; internal set; }
        #endregion

        #region Methods
        public void Dismiss() => Application.Current.MainPage.Navigation.PopModalAsync();

        public void LoadDBContentTaskModel()
        {
            if (Task.IsFilter) Task.IsFilter = false;
            Task.ReadTasksFromDB();
        }
         

        public string GetDayEnglishToLanguages(string day)
        {
            string message = "";
            switch (day)
            {
                case "Monday":
                    message += Languages.MondayDay;
                    break;
                case "Tuesday":
                    message += Languages.TuesdayDay;
                    break;
                case "Wednesday":
                    message += Languages.WednesdayDay;
                    break;
                case "Thursday":
                    message += Languages.ThursdayDay;
                    break;
                case "Friday":
                    message += Languages.FridayDay;
                    break;
                case "Saturday":
                    message += Languages.SaturdayDay;
                    break;
                case "Sunday":
                    message += Languages.SundayDay;
                    break;
            }
            return message;
        }

        public ImageSource ActivityIconImageSource(
            int index,
            ObservableCollection<string> obs,
            string actionSheetResponse,
            ImageSource iconEvaluated)
        {
            if (string.IsNullOrEmpty(actionSheetResponse))
            {
                if (obs[index] == Languages.DoExcerciseText)
                    iconEvaluated = "excerciseIcon";
                else if (obs[index] == Languages.StudyText)
                    iconEvaluated = "studyIcon";
                else if (obs[index] == Languages.WorkText)
                    iconEvaluated = "workIcon";
                else if (obs[index] == Languages.DoShoppingText)
                    iconEvaluated = "shopIcon";
                else if (obs[index] == Languages.ReadText)
                    iconEvaluated = "readIcon";
                else if (obs[index] == Languages.GoToSchoolText)
                    iconEvaluated = "schoolIcon";
                else if (obs[index] == Languages.PlayText)
                    iconEvaluated = "playIcon";
                else if (obs[index] == Languages.EatText)
                    iconEvaluated = "eatIcon";
                else
                    iconEvaluated = "SortexAppIcon";
            }
            return iconEvaluated;
        }

        public string DayToEnglish(string day)
        {
            string message = "";
            
            if(string.Equals(day,Languages.MondayDay))
                message += "Monday";
            else if(string.Equals(day, Languages.TuesdayDay))
                message += "Tuesday";
            else if(string.Equals(day, Languages.WednesdayDay))
                message += "Wednesday";
            else if (string.Equals(day, Languages.ThursdayDay))
                message += "Thursday";
            else if (string.Equals(day, Languages.FridayDay))
                message += "Friday";
            else if (string.Equals(day, Languages.SaturdayDay))
                message += "Saturday";
            else
                message += "Sunday";
            return message;
        }

        public ImageSource PickerImageSelection(
            ObservableCollection<string> PickerImageItems,
            int PickerImageItemsIndex)
        {
            ImageSource pickerImageSelection = "";

            if (PickerImageItems[PickerImageItemsIndex] == Languages.DoExcerciseText)
                pickerImageSelection = "excerciseIcon";
            else if (PickerImageItems[PickerImageItemsIndex] == Languages.StudyText)
                pickerImageSelection = "studyIcon";
            else if (PickerImageItems[PickerImageItemsIndex] == Languages.WorkText)
                pickerImageSelection = "workIcon";
            else if (PickerImageItems[PickerImageItemsIndex] == Languages.DoShoppingText)
                pickerImageSelection = "shopIcon";
            else if (PickerImageItems[PickerImageItemsIndex] == Languages.ReadText)
                pickerImageSelection = "readIcon";
            else if (PickerImageItems[PickerImageItemsIndex] == Languages.GoToSchoolText)
                pickerImageSelection = "schoolIcon";
            else if (PickerImageItems[PickerImageItemsIndex] == Languages.PlayText)
                pickerImageSelection = "playIcon";
            else if (PickerImageItems[PickerImageItemsIndex] == Languages.EatText)
                pickerImageSelection = "eatIcon";
            else
                pickerImageSelection = "SortexAppIcon"; // SortexLogo

            return pickerImageSelection;
        }

        public void ReInstanciateSubtaskModelProps()
        {
            //Re instanciate the collections to reuse them as new as equals as the counter
            SubtaskModel.counter = 0;
            SubtaskModel.TextDict = new Dictionary<int, string>();
            SubtaskModel.Instances = new List<SubtaskModel>();
            //
        }

        //public async void ExecuteUserValidation()
        //{
        //    SQLHelper sqlConnection = new SQLHelper();
        //    //
        //    var table = SQLHelper.Connection.Table<UserModel>();
        //    var responseModel = ConverterHelper.ConvertToUserModel(table.ToList()); //edit SQLHelper for this kind of situations
        //    if (responseModel.IsSuccess)
        //    {
        //        var userModel = (UserModel)responseModel.Model;
        //        if (userModel.CurrentDayTask != DateTime.Now.ToShortDateString())
        //        {
        //            //Update tasks
        //            var tableTasks = SQLHelper.Connection.Table<TaskModel>().ToArray();
        //            foreach (var model in tableTasks)
        //            {
        //                if (model.IsNotRepeatedTask == true && model.IsPlan == false) // model.IsNotRepeatedTask && !model.IsPlan
        //                {
        //                    sqlConnection.Delete(model);
        //                    continue;
        //                }
        //                if (model.IsPlan)
        //                {
        //                    model.IsPlan = false;
        //                    sqlConnection.Update(model);
        //                }
        //            }
        //            tableTasks = SQLHelper.Connection.Table<TaskModel>().ToArray();
        //            if (tableTasks.Length >= 1)
        //                NotificationsHelper.SetNotification(4);

        //            //Update activities
        //            var tableActivities = SQLHelper.Connection.Table<ActivityModel>().
        //                ToList();

        //            tableActivities.ForEach(a =>
        //            {
        //                if (a.IsDone) a.IsDone = false;
        //                a.Times = string.Empty;
        //            });

        //            //eval if stats have been evaluated

        //            if ((int)DateTime.Now.DayOfWeek == 0)
        //                userModel.HasEvaluatedStats = true;
        //            else
        //            {
        //                if (userModel.HasEvaluatedStats)
        //                {
        //                    userModel.HasEvaluatedStats = false;
        //                    userModel.TasksDone = 0;
        //                    userModel.TotalTasks = 0;
        //                    foreach (var activity in tableActivities)
        //                        activity.DoneTimes = 0;
        //                }
        //            }

        //            SQLHelper.Connection.UpdateAll(tableActivities);

        //            //Update User
        //            userModel.CurrentDayTask = DateTime.Now.ToShortDateString();
        //            var response = sqlConnection.Update(userModel);
        //            if (!response.IsSuccess)
        //                ShowMessage("Error", response.Message);
        //            MainViewModel.GetInstance().LoadDBContentTaskModel();
        //        }
        //    }
        //    if (!responseModel.IsSuccess)
        //    {
        //        //COMMENT THE PAST LINES OF CODE WRITTEN!
        //        sqlConnection.DeleteAll<UserModel>();
        //        var response = sqlConnection.Insert(new UserModel()
        //        {
        //            CurrentDayTask = DateTime.Now.ToShortDateString(),
        //            ProfileImage = null,
        //            ToDoListAllowed = true,
        //        });
        //        if (!response.IsSuccess)
        //            ShowMessage("Error", response.Message);

        //        MainViewModel.GetInstance().InitialUser = new InitialUserViewModel();
        //        await Application.Current.MainPage.Navigation.PushModalAsync(
        //            new NavigationPage(new InitialUserPage()));
        //    }

        //    sqlConnection.CloseConnection();
        //}

        //public async void ShowMessage(string title, string message)
        //{
        //    await Application.Current.MainPage.DisplayAlert(
        //        title,
        //        message,
        //        Languages.CancelText);
        //}
        #endregion
    }
}
