﻿using GalaSoft.MvvmLight.Command;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Sortex.Helpers;
using Sortex.Interfaces;
using Sortex.Models;
using Sortex.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class ProfileViewModel : BaseViewModel
    {
        #region Class fields
        private SQLHelper sqlConnection;
        private ResponseModel tableUser;
        private MediaFile file = null;
        private ImageSource profileImageSource = "UserIcon";
        private string userName = null;
        private TimeSpan userSleepTime;
        private string totalTasksDone;
        private string totalTasks;
        private string totalActivities;
        private string totalDoneActivities;
        private INotificationManager notificationManager = null;
        #endregion

        #region Properties

        public UserModel UserModel { get; set; }
        public List<TaskModel> TasksTable { get; set; }

        public ImageSource ProfileImageSource
        {
            get { return this.profileImageSource; }
            set { SetValue(ref this.profileImageSource, value); }
        }

        public string UserName
        {
            get
            {
                if (this.userName == null)
                {
                    this.userName = Languages.YouText;
                    if (UserModel != null)
                    {
                        if (!string.IsNullOrEmpty(UserModel.UserName))
                            this.userName = UserModel.UserName; // Languages.YouText;
                        //else
                        //    this.userName = UserModel.UserName;
                    }
                }
                return this.userName;
            }
            set { SetValue(ref this.userName, value); }
        }

        public string TotalTasks
        {
            get
            {
                this.totalTasks = TasksTable.Count.ToString(); //
                return this.totalTasks;
            }
            set
            { SetValue(ref this.totalTasks, value); }
        }
        public string TotalActivities
        {
            get
            {
                this.totalActivities = 
                    ActivityItemViewModel.GetTableCount(true).ToString();
                return totalActivities;
            }
            set { SetValue(ref this.totalActivities, value); }
        }
                                                                    //when Activities table has been created
        public string TotalTasksDone
        {
            get
            {
                this.totalTasksDone = TasksTable.Where(t => t.IsDoneTask).Count().ToString();
                return this.totalTasksDone;
            }
            set { SetValue(ref this.totalTasksDone, value); }
        }

        public string TotalDoneActivities
        {
            get
            {
                this.totalDoneActivities =
                    ActivityItemViewModel.GetTotalDoneActivities().ToString();
                return this.totalDoneActivities;
            }
            set { SetValue(ref this.totalDoneActivities, value); }
        }
                                                                        //when Activities table has been created
        #endregion

        #region Commands
        public ICommand ViewTasksDoneCommand
        {
            get { return new RelayCommand(this.ViewTasksDone); }
        }

        public ICommand ImageConfigSelectorCommand
        {
            get { return new RelayCommand(this.ImageConfigSelector); }
        }

        public ICommand ChangeUserNameCommand
        {
            get { return new RelayCommand(this.ChangeUserName); }
        }

        public ICommand EditProfileCommand
        {
            get { return new RelayCommand(this.EditProfile); }
        }

        public ICommand ViewActivitiesDoneCommand
        {
            get { return new RelayCommand(async () =>
            {
                SQLHelper sqlConnection = new SQLHelper();
                MainViewModel.GetInstance().DoneActivities = new DoneActivitiesViewModel(
                    SQLHelper.Connection.Table<ActivityModel>().ToList());
                sqlConnection.CloseConnection();
                await Application.Current.MainPage.Navigation.PushModalAsync(
                    new NavigationPage(new DoneActivitiesPage())
                    { BarBackgroundColor = Color.FromHex("#18E1BF") });
            }); }
        }

        public ICommand ViewPlanListPageCommand
        {
            get { return new RelayCommand(this.ViewPlansListPage); }
        }

        public ICommand ViewPlansCommand
        {
            get { return new RelayCommand(async () =>
            {
                MainViewModel.GetInstance().Plans = new PlansViewModel();
                await Application.Current.MainPage.Navigation.PushModalAsync(
                    new NavigationPage(new PlansPage())
                    { BarBackgroundColor = Color.FromHex("#18E1BF") });
            }); }
        }
        #endregion

        #region Constructors
        public ProfileViewModel(UserModel userModel, List<TaskModel> table)
        {
            notificationManager = DependencyService.Get<INotificationManager>();
            notificationManager.NotificationReceived += (sender, args) =>
            {
                var notifArgs = (NotificationsEventArgs)args;
                if (string.Equals(notifArgs.Title, "Your plans"))
                    this.ViewPlansListPage();
            };
              

            this.UserModel = userModel;
            this.TasksTable = table;
            //TaskItemViewModel.GetTableTasksCount(true, true);
            //ActivityItemViewModel.GetTableCount(true);
            //ActivityItemViewModel.GetTotalDoneActivities(true);
            this.LoadProfileActivityInfo();
            this.LoadProfileInfo();
        }
        #endregion

        #region Methods
        
        public async void ViewPlansListPage()
        {
            MainViewModel.GetInstance().PlanList = new PlanListViewModel();
            await Application.Current.MainPage.Navigation.PushModalAsync(
                new NavigationPage(new PlanListPage())
                { BarBackgroundColor = Color.FromHex("#18E1BF") });
        }
        public async void ViewTasksDone()
        {
            MainViewModel.GetInstance().TasksDone = new TasksDoneViewModel();
            await Application.Current.MainPage.Navigation.PushModalAsync(
                new NavigationPage(new TasksDonePage())
                { BarBackgroundColor = Color.FromHex("#18E1BF") });
        }

        public async void ImageConfigSelector()
        {
            bool hasSelected = false;
            await CrossMedia.Current.Initialize();

            if (CrossMedia.Current.IsCameraAvailable &&
                    CrossMedia.Current.IsTakePhotoSupported)
            {
                var actionSheetProfileSelection = await Application.Current.MainPage.DisplayActionSheet(
                    Languages.TitleImageConfigSelection,
                    null,
                    Languages.CancelText,
                    Languages.TakeImageTitle,
                    Languages.GalleryImageTitle,
                    Languages.ResetImageTitle);

                if (actionSheetProfileSelection == Languages.TakeImageTitle)
                {
                    if (actionSheetProfileSelection == Languages.TakeImageTitle)
                    {
                        this.file = await CrossMedia.Current.TakePhotoAsync(
                            new StoreCameraMediaOptions
                            {
                                Directory = "SortexImages", //
                                Name = "myprofileimage.jpg",
                                PhotoSize = PhotoSize.Large
                            }
                        );
                    }
                }
                else if (actionSheetProfileSelection == Languages.GalleryImageTitle)
                    this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.Large
                    });

                else if (actionSheetProfileSelection == Languages.ResetImageTitle)
                {
                    this.ProfileImageSource = "UserIcon";
                    hasSelected = true;
                }

                else
                    return;
            }
            else
            {
                this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.Large,
                });

            }

            if (this.file != null || hasSelected)
            {
                if(!hasSelected)
                {
                    this.ProfileImageSource = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        return stream;
                    });
                }
                //Dump current image to db
                InitializeTableUserModel();

                if (!hasSelected)
                    UserModel.ProfileImage = FilesHelper.GetImageByteArray(file.GetStream());
                else
                    UserModel.ProfileImage = null;
                ResponseModel response = sqlConnection.Update(UserModel);
                if (!response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        response.Message,
                        Languages.CancelText);
                    return;
                }
                sqlConnection.CloseConnection();
            }
        }

        public void LoadUserInformation()
        {
            InitializeTableUserModel();
            if (UserModel.ProfileImage == null)
                this.ProfileImageSource = "UserIcon";
            else
            {
                this.ProfileImageSource = ImageSource.FromStream(() => // do this on get operator of property
                {
                    Stream profileImageStream = new MemoryStream(UserModel.ProfileImage);
                    return profileImageStream;
                });
            }
            MainViewModel.GetInstance().ProfilePage.IsSwitched = UserModel.ToDoListAllowed;
            sqlConnection.CloseConnection();
        }

        public async void EditProfile()
        {
            MainViewModel.GetInstance().EditProfile = 
                new EditProfileViewModel(UserModel);
            await Application.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(
                new EditProfilePage()) { BarBackgroundColor = Color.FromHex("#18E1BF") });
        }

        public async void ChangeUserName()
        {
            string userNamePromtResponse = 
                await Application.Current.MainPage.DisplayPromptAsync(
                    Languages.UserNamePromtTitle,
                    Languages.UserNamePromtQuestion);
            if (userNamePromtResponse != null)
            {
                InitializeTableUserModel();
                UserModel.UserName = userNamePromtResponse;
                ResponseModel response = sqlConnection.Update(UserModel);
                if (!response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        response.Message,
                        Languages.CancelText);
                    return;
                }
                sqlConnection.CloseConnection();
                this.UserName = userNamePromtResponse;
            }
        }

        public void InitializeTableUserModel()
        {
            sqlConnection = new SQLHelper();
            tableUser = ConverterHelper.ConvertToUserModel(
            SQLHelper.Connection.Table<UserModel>().ToList());
            UserModel = (UserModel)tableUser.Model;
        }

        public void LoadProfileActivityInfo()
        {
            sqlConnection = new SQLHelper();
            TasksTable = SQLHelper.Connection.Table<TaskModel>().Where(
                t => !t.IsPlan).ToList();
            this.TotalTasksDone = TasksTable.Where(t => t.IsDoneTask).Count().ToString();
            this.TotalTasks = this.TasksTable.Count.ToString();
            sqlConnection.CloseConnection();
        }

        public void LoadProfileInfo()
        {
            sqlConnection = new SQLHelper();
            var response = ConverterHelper.ConvertToUserModel(SQLHelper.Connection.Table<UserModel>().ToList());
            this.UserModel = (UserModel)response.Model;
            if (this.UserModel != null)
            {
                this.UserName = this.UserModel.UserName;
                if (this.UserModel.ProfileImage == null)
                    this.ProfileImageSource = "UserIcon";
                else
                {
                    this.ProfileImageSource = ImageSource.FromStream(() =>
                    {
                        Stream image = new MemoryStream(this.UserModel.ProfileImage);
                        return image;
                    });
                }
            }
            
            sqlConnection.CloseConnection();
        }
        #endregion
    }
}
