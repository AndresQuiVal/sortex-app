﻿using GalaSoft.MvvmLight.Command;
using MvvmHelpers;
using Plugin.LocalNotifications;
using Sortex.Enums;
using Sortex.Helpers;
using Sortex.Interfaces;
using Sortex.Models;
using Sortex.Views;
//using SQLite.Net.Attributes;//using SQLite;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class TaskItemViewModel : TaskModel, INotifyPropertyChanged
    {

        #region Class fields
        private string actionSheetResponse;
        private Color isDoneColor;
        private string isDone;
        private static int numberTaskDone = 0;
        #endregion

        #region Properties
        [Ignore]
        public string IsDone
        {
            get
            {
                if (this.IsDoneTask)
                {
                    this.isDone = Languages.DoneText;
                    this.isDoneColor = Color.LawnGreen;
                }
                else
                {
                    this.isDone = Languages.NotDoneText;
                    this.isDoneColor = Color.Red;
                }
                return this.isDone;
            }
            set
            {
                if (this.isDone != value)
                {
                    this.isDone = value;
                    PropertyChanged?.Invoke
                        (this,
                        new PropertyChangedEventArgs(nameof(this.IsDone)));
                }
            }
        }

        [Ignore]
        public Xamarin.Forms.Color IsDoneColor
        {
            get
            { return this.isDoneColor; }
            set
            {
                if (this.isDoneColor != value)
                {
                    this.isDoneColor = value;
                    PropertyChanged?.Invoke
                        (this,
                        new PropertyChangedEventArgs(nameof(this.IsDoneColor)));
                }
            }
        }
        #endregion

        #region Commands
        [Ignore]
        public ICommand GridExpandCommand
        {
            get { return new RelayCommand(this.GridExpand); }
        }

        [Ignore]
        public ICommand ItemsDoneSelectionCommand
        {
            get { return new RelayCommand(this.ItemsDoneSelection); }
        }
        #endregion

        #region Events
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Methods
        public async void GridExpand()
        {
            // IsVisible = !IsVisible;
            actionSheetResponse = await Application.Current.MainPage.DisplayActionSheet(
                String.Format($"{Languages.TaskOptionsText} {this.Name}"),
                Languages.CancelText, null,
                Languages.TaskOptionsDeleteText,
                Languages.TaskOptionsEditText,
                String.Format($"{Languages.DoneText}!"));

            if (actionSheetResponse == Languages.TaskOptionsDeleteText)
            {
                SQLHelper sqlConnection = new SQLHelper();
                var tableArray = 
                    SQLHelper.Connection.Table<TaskModel>().Where(t => t.IsDoneTask == false).ToArray();
                
                sqlConnection.Delete(ConverterHelper.ConvertToTaskModel(this));
                if (tableArray.Length == 1)
                    NotificationsHelper.CancelNotification(
                        DependencyService.Get<INotificationManager>(), -1,
                        BroadCastType.Task);

                var user = SQLHelper.Connection.Table<UserModel>().ToArray()[0];
                user.TasksDone--;
                user.TotalTasks--;
                sqlConnection.Update(user);

                MainViewModel.GetInstance().Profile.LoadProfileActivityInfo();
                //GetTableTasksCount(true);
                MainViewModel.GetInstance().LoadDBContentTaskModel();
                sqlConnection.CloseConnection();

                if (MainViewModel.GetInstance().IsPlanSender)
                    MainViewModel.GetInstance().Plans.LoadPlans();
            }
            else if (actionSheetResponse == Languages.TaskOptionsEditText)
            {
                MainViewModel.GetInstance().DetailsTask = new DetailsTaskViewModel(this);

                await Application.Current.MainPage.Navigation.PushModalAsync
                    (new NavigationPage(new DetailsTaskPage(this.Subtasks))
                    { BarBackgroundColor = Xamarin.Forms.Color.FromHex("#18E1BF") });
            }
            else if (actionSheetResponse == String.Format($"{Languages.DoneText}!"))
            {
                SQLHelper sqlConnection = new SQLHelper();
                this.IsDoneTask = !this.IsDoneTask;
                ResponseModel response = sqlConnection.Update(ConverterHelper.ConvertToTaskModel(this));
                var table = SQLHelper.Connection.Table<TaskModel>();
                if (response.IsSuccess)
                {
                    //Eval the number of tasks done
                    TaskModel[] numberModels = table.Where(t => t.IsDoneTask).ToArray();
                    if (numberModels.Length == table.Count())
                        //NotificationsHelper.CancelNotificationPlugin(5);
                        NotificationsHelper.CancelNotification(
                            DependencyService.Get<INotificationManager>(), -1, 
                            BroadCastType.Task);

                    await Application.Current.MainPage.DisplayAlert(
                       this.IsDone,
                       String.Format($"{this.Name} {Languages.Task} {this.IsDone.ToLower()}"),
                       Languages.CancelText);
                    numberTaskDone++;
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Error",
                        response.Message,
                        Languages.CancelText);
                }

                var user = SQLHelper.Connection.Table<UserModel>().ToArray()[0];
                user.TasksDone++;
                sqlConnection.Update(user);

                MainViewModel.GetInstance().Profile.LoadProfileActivityInfo();
                //GetTableTasksCount(false, true);
                MainViewModel.GetInstance().LoadDBContentTaskModel();
                sqlConnection.CloseConnection();

                if (MainViewModel.GetInstance().IsPlanSender)
                    MainViewModel.GetInstance().Plans.LoadPlans();
            }

            if (this.PriorityLevel == Languages.PlansText)
                MessagingCenter.Send(this, Languages.PlansText);
        }

        public async void TaskNotDone()
        {
            SQLHelper sqlConnection = new SQLHelper();
            var model = this;
            model.IsDoneTask = false;
            ResponseModel response = sqlConnection.Update(ConverterHelper.ConvertToTaskModel(model));
            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    Languages.CancelText);
                return;
            }

            var user = SQLHelper.Connection.Table<UserModel>().ToArray()[0];
            user.TasksDone--;
            sqlConnection.Update(user);

            var table = SQLHelper.Connection.Table<TaskModel>();
            TaskModel[] tasksTable = table.Where(t => !t.IsDoneTask).ToArray();
            if (table.Count() == tasksTable.Count()) //if all tasks are completed
                NotificationsHelper.SetNotificationForTasks();
                //NotificationsHelper.SetNotificationPlugin(4); TODO: DISCOMMENT

            sqlConnection.CloseConnection();
            MainViewModel.GetInstance().TasksDone.PopCurrentPage();
            MainViewModel.GetInstance().Profile.LoadProfileActivityInfo();
            //GetTableTasksCount(false, true);
            MainViewModel.GetInstance().LoadDBContentTaskModel();
        }

        public async void ItemsDoneSelection()
        {
            var actionSheetResponseItem = await Application.Current.MainPage.DisplayActionSheet(
                String.Format($"{Languages.TaskOptionsText} {this.Name}"),
                Languages.CancelText,
                null,
                Languages.TaskNotDoneTitle);

            if (actionSheetResponseItem == Languages.TaskNotDoneTitle)
                TaskNotDone();
        }

        public static int GetTableTasksCount(
            bool setForProfileViewModel = false,
            bool setForProfileViewModelDone = false)
        {
            SQLHelper sqlConnection = new SQLHelper();
            var tableTasks = SQLHelper.Connection.Table<TaskModel>().ToArray();
            var notPlanTasks = tableTasks.Where(t => !t.IsPlan).Count().ToString();
            var doneTasks = tableTasks.Where(t => t.IsDoneTask).Count().ToString();
            sqlConnection.CloseConnection();

            if (setForProfileViewModel)
                MainViewModel.GetInstance().Profile.TotalTasks = 
                    notPlanTasks;
            if (setForProfileViewModelDone)
                MainViewModel.GetInstance().Profile.TotalTasksDone =
                    doneTasks;

            return tableTasks.Length;
        }
        #endregion
    }
}
