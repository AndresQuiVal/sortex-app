﻿using System;
using System.Collections.Generic;
using System.Text;
using Sortex.Models;
using System.Collections.ObjectModel;
using Sortex.Helpers;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System.IO;
using System.Linq;
using Sortex.Interfaces;

namespace Sortex.ViewModels
{
    public class EditActivityViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<DaysActivityModel> daysCollection;
        private ObservableCollection<string> activityIconCollection;
        private ImageSource activityIconImageSource;
        private int selectedIndex;
        private int imageChangedCounter;
        private int selectedIndex2;
        private bool isImageLoading;
        private bool isNotImageLoading;
        private string actionSheetIconResponse;
        private string selectedItem;
        private string currentName;
        private string titleName;
        private string activityName;
        private MediaFile file;
        private ImageSource currentIcon;
        private INotificationManager notificationManager;
        #endregion

        #region Properties
        public ActivityItemViewModel ActivityModel { get; set; }
        public ImageSource CurrentIcon 
        {
            get { return this.currentIcon; }
            set { this.currentIcon = value; }
        }
        public ObservableCollection<DaysActivityModel> DaysCollection
        {
            get { return this.daysCollection; }
            set { SetValue(ref this.daysCollection, value); }
        }

        public ObservableCollection<string> ActivityIconCollection
        {
            get { return this.activityIconCollection; }
            set { SetValue(ref this.activityIconCollection, value); }
        }

        public int SelectedIndex
        {
            get { return this.selectedIndex; }
            set
            {
                if (this.selectedIndex == value)
                    return;

                if (/*(this.selectedIndex != value ||*/
                    /*this.activityIconImageSource == null && */value != -1)
                {
                    SetValue(ref this.selectedIndex, value);
                    if (this.ActivityIconCollection[this.SelectedIndex] == Languages.UploadIconText)
                    {
                        this.DisplayImageActionSheet();
                        return;
                    }

                    this.ActivityIconImageSource = this.ActivityModel.ActivityIconImage;
                    if (this.ActivityIconCollection[this.SelectedIndex] != "Current icon")
                    {
                        this.ActivityIconImageSource =
                            MainViewModel.GetInstance().PickerImageSelection(
                            ActivityIconCollection, SelectedIndex);
                    }
                    else
                    {
                        this.imageChangedCounter--;
                        return;
                    }

                    this.imageChangedCounter++;
                    return;
                }
                this.ActivityIconImageSource = ImageSource.FromStream(() =>
                   { return new MemoryStream(ActivityModel.PersonalizedIcon); });

                this.imageChangedCounter++; //DISCOMMENT FOR NO ERROR
                this.selectedIndex = MainViewModel.GetInstance().ActivityItems.Count - 1;
                SetValue(ref this.selectedIndex, value);
            }
        }

        public int SelectedIndex2 
        {
            get { return this.selectedIndex2; }
            set { SetValue(ref this.selectedIndex2, value); }
        }

        public string SelectedItem 
        {
            get { return this.selectedItem; }
            set 
            {
                if (!string.IsNullOrEmpty(this.SelectedItem))
                {
                    if (string.Equals(value, Languages.UploadIconText))
                        DisplayImageActionSheet();
                    else if (string.Equals(value, "Current icon"))
                        this.ActivityIconImageSource = this.currentIcon;
                    else if (string.Equals(value, Languages.NoIconText))
                        this.ActivityIconImageSource = "SortexAppIcon";
                    else
                        this.ActivityIconImageSource =
                            MainViewModel.GetInstance().PickerImageSelection(
                                ActivityIconCollection, SelectedIndex2);
                }
                else
                    this.ActivityIconImageSource = CurrentIcon;
                    
                //this.selectedItem = value;
                SetValue(ref this.selectedItem, value);
            }
        }

        public string TitleName
        {
            get { return this.titleName; }
            set
            {
                this.titleName = value;
                if (string.IsNullOrEmpty(value))
                    this.titleName = currentName;
                //SetValue(ref this.titleName, value);
                OnPropertyChanged();
            }
        }

        public string ActivityName 
        {
            get { return this.activityName; }
            set 
            {
                SetValue(ref this.activityName, value);
                this.TitleName = value;
            }
        }

        public bool IsImageLoading 
        {
            get { return this.isImageLoading; }
            set 
            {
                this.IsNotImageLoading = !value;
                SetValue(ref this.isImageLoading, value);
            }
        }

        public bool IsNotImageLoading 
        {
            get { return this.isNotImageLoading; }
            set { SetValue(ref this.isNotImageLoading, value); }
        }

        public ImageSource ActivityIconImageSource
        {
            get { return this.activityIconImageSource; }
            set { SetValue(ref this.activityIconImageSource, value); }
        }
        #endregion

        #region Commands
        public ICommand EditActivityCommand
        {
            get { return new RelayCommand(this.EditActivity2); } // CHANGE TO DEF EditActiivty() METHOD!!!!!!
        }
        #endregion

        #region Constructors
        public EditActivityViewModel(ActivityModel activityModel)
        {
            if (activityModel.PersonalizedIcon == null) // COMMENT IF NECCESARY
                currentIcon = activityModel.ActivityIcon;
            else
                currentIcon = ImageSource.FromStream(() =>
                { return new MemoryStream(activityModel.PersonalizedIcon); });


            this.ActivityIconCollection = MainViewModel.GetInstance().ActivityItems;
            this.ActivityIconCollection.Add("Current icon");
            this.notificationManager = DependencyService.Get<INotificationManager>();

            this.ActivityModel = ConverterHelper.ConvertToActivityItemViewModel(activityModel);

            this.SelectedIndex = ActivityIconCollection.IndexOf(ActivityModel.ActivityIcon);

            if (string.IsNullOrEmpty(this.ActivityModel.ActivityIcon))
                this.ActivityIconImageSource = ImageSource.FromStream(() =>
                { return new MemoryStream(this.ActivityModel.PersonalizedIcon); });
            else
                this.ActivityIconImageSource = this.ActivityModel.ActivityIconImage;

            if (SelectedIndex == 0 || selectedIndex == -1) this.imageChangedCounter++;

            this.IsImageLoading = false;

            this.LoadDaysCollection();
        }

        public EditActivityViewModel(ActivityModel activityModel, bool isConstructor)
        {
            //    if (activityModel.PersonalizedIcon == null)
            //        currentIcon = activityModel.ActivityIcon;
            //    else
            //        currentIcon = ImageSource.FromStream(() =>
            //        { return new MemoryStream(activityModel.PersonalizedIcon); });

            this.ActivityName = activityModel.Name;
            this.currentName = activityModel.Name;
            this.ActivityIconCollection = MainViewModel.GetInstance().ActivityItems;
            this.ActivityIconCollection.Add("Current icon");
            this.notificationManager = DependencyService.Get<INotificationManager>();

            this.ActivityModel = ConverterHelper.ConvertToActivityItemViewModel(activityModel);

            if (string.IsNullOrEmpty(this.ActivityModel.ActivityIcon))
            {
                this.ActivityIconImageSource = ImageSource.FromStream(() =>
                { return new MemoryStream(this.ActivityModel.PersonalizedIcon); });

                this.SelectedIndex2 =
                    this.ActivityIconCollection.IndexOf(Languages.UploadIconText);
            }
                
            else
            {
                this.ActivityIconImageSource = this.ActivityModel.ActivityIconImage;

                this.SelectedIndex2 =
                    this.ActivityIconCollection.IndexOf(this.ActivityModel.ActivityIcon);
            }
                

            this.CurrentIcon = this.ActivityIconImageSource;


            this.IsImageLoading = false;

            this.LoadDaysCollection();
        }
        #endregion

        #region Methods
        public void LoadDaysCollection()
        {
            DaysCollection = new ObservableCollection<DaysActivityModel>();
            DaysActivityModel.DayHourDict = ActivityModel.DayHourDict;
            DaysActivityModel.DaysString = ActivityModel.ActivityDays;

            foreach (var item in new[] { Languages.MondayDay, Languages.TuesdayDay,
                Languages.WednesdayDay,Languages.ThursdayDay, Languages.FridayDay,
                Languages.SaturdayDay, Languages.SundayDay })
            {

                DaysCollection.Add(new DaysActivityModel()
                {
                    Day = item,
                    IsChecked = true,
                });
            }
        }

        public async void EditActivity()
        {
            SQLHelper sqlConnection = new SQLHelper();
            string dayHourString = "", notificationIntervals = "";

            foreach(var item in DaysActivityModel.DayHourDict) // to LINQ?
            {
                dayHourString += string.Format($"{item.Key}:{item.Value},");
                notificationIntervals += $"{item.Value},";
            }

            this.ActivityModel.Name = this.ActivityName;
            this.ActivityModel.ActivityDays = dayHourString;
            this.ActivityModel.NotificationIntervals = notificationIntervals;

            if (this.imageChangedCounter > 1)
            {
                this.ActivityModel.PersonalizedIcon = null; // do with an if - else block
                if (this.ActivityIconCollection[this.SelectedIndex] != "Current icon")
                    this.ActivityModel.ActivityIcon = 
                        this.ActivityIconCollection[this.SelectedIndex];
                if (file != null)
                {
                    this.ActivityModel.PersonalizedIcon = FilesHelper.GetImageByteArray(
                        this.file.GetStream());
                    this.ActivityModel.ActivityIcon = null;
                }
                //else
                //{
                //    this.ActivityModel.ActivityIcon =
                //    this.ActivityIconCollection[this.SelectedIndex];

                //    this.ActivityModel.PersonalizedIcon = null;
                //}
            }

            var response = sqlConnection.Update(
                ConverterHelper.ConvertToActivityModel(this.ActivityModel));

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    Languages.CancelText);
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.CorrectText,
                    Languages.ModifiedActivityText,
                    Languages.CancelText);
            }

            //send notification

            NotificationsHelper.SetRepeatedNotificationDependency(
                notificationManager,
                this.ActivityModel.Name,
                $"Do the activity {this.ActivityModel.Name}",
                DaysActivityModel.DayHourDict[MainViewModel.GetInstance().Day],
                DaysActivityModel.DayHourDict.Values.ToArray(),
                Convert.ToInt32(this.ActivityModel.Id));
            //

            this.imageChangedCounter = 0;
            sqlConnection.CloseConnection();
            MainViewModel.GetInstance().Home.LoadDBActivitiesContent();
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }

        public async void EditActivity2()
        {
            SQLHelper sqlConnection = new SQLHelper();
            string dayHourString = "", notificationIntervals = "";

            foreach (var item in DaysActivityModel.DayHourDict)
            {
                dayHourString += string.Format($"{item.Key}:{item.Value},");
                notificationIntervals += $"{item.Value},";
            }

            this.ActivityModel.Name = this.ActivityName;
            this.ActivityModel.ActivityDays = dayHourString;
            this.ActivityModel.NotificationIntervals = notificationIntervals;

            if (!string.Equals(this.SelectedItem, "Current icon"))
            {
                if (string.Equals(this.SelectedItem, Languages.UploadIconText))
                {
                    this.ActivityModel.PersonalizedIcon = 
                        FilesHelper.GetImageByteArray(
                            this.file.GetStream());
                    this.ActivityModel.ActivityIcon = null;
                }
                else
                {
                    this.ActivityModel.PersonalizedIcon = null;
                    this.ActivityModel.ActivityIcon = this.SelectedItem;
                }
            }

            var response = sqlConnection.Update(
                ConverterHelper.ConvertToActivityModel(this.ActivityModel));

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    Languages.CancelText);
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.CorrectText,
                    Languages.ModifiedActivityText,
                    Languages.CancelText);
            }

            //send notification

            NotificationsHelper.SetRepeatedNotificationDependency(
                notificationManager,
                this.ActivityModel.Name,
                $"Do the activity {this.ActivityModel.Name}",
                DaysActivityModel.DayHourDict[MainViewModel.GetInstance().Day],
                DaysActivityModel.DayHourDict.Values.ToArray(),
                Convert.ToInt32(this.ActivityModel.Id));
            //

            //this.imageChangedCounter = 0;
            sqlConnection.CloseConnection();
            MainViewModel.GetInstance().Home.LoadDBActivitiesContent();
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }

        public async void DisplayImageActionSheet()
        {
            await CrossMedia.Current.Initialize();

            this.imageChangedCounter--; 

            if (CrossMedia.Current.IsCameraAvailable &&
                    CrossMedia.Current.IsTakePhotoSupported)
            {
                actionSheetIconResponse = await Application.Current.MainPage.DisplayActionSheet(
                    Languages.RecomendedResolText,//Languages.TitleImageConfigSelection
                    null,
                    Languages.CancelText,
                    Languages.TakeImageTitle,
                    Languages.GalleryImageTitle);

                if (actionSheetIconResponse == Languages.TakeImageTitle)
                {
                    this.IsImageLoading = true;
                    this.file = await CrossMedia.Current.TakePhotoAsync(
                        new StoreCameraMediaOptions
                        {
                            Directory = "SortexImages", //
                            Name = "iconImage.jpg",
                            PhotoSize = PhotoSize.Large,
                        }
                    );
                }
                else if (actionSheetIconResponse == Languages.GalleryImageTitle)
                {
                    this.IsImageLoading = true;
                    this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.Large,
                    });
                }

                else
                {
                    this.actionSheetIconResponse = null;
                    this.IsImageLoading = false;
                    //SetDefaultUserImage();
                    return;
                }
            }
            else
            {
                this.IsImageLoading = true;
                this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.Large,
                });

            }

            if (this.file != null)
            {
                this.ActivityIconImageSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
                this.imageChangedCounter++;
            }
            //else SetDefaultUserImage();

            this.actionSheetIconResponse = null;
            this.IsImageLoading = false;
        }

        public void SetDefaultUserImage()
        {
            if (ActivityModel.PersonalizedIcon == null)
            {
                this.ActivityIconImageSource = ActivityModel.ActivityIcon;
                return;
            }

            this.ActivityIconImageSource = ImageSource.FromStream(() =>
            { return new MemoryStream(ActivityModel.PersonalizedIcon); });
        }
        #endregion
    }
}
