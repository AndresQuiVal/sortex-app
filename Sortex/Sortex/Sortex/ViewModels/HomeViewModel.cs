﻿using GalaSoft.MvvmLight.Command;
using MvvmHelpers;
using Sortex.Helpers;
using Sortex.Interfaces;
using Sortex.Models;
using Sortex.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<Grouping<string, ActivityItemViewModel>> activitiesCollection;
        private IEnumerable<Grouping<string, ActivityItemViewModel>> sorted;
        private List<string> listHours;
        private SQLHelper sqlConnection;
        private string textFilter;
        private bool isVisibleNoItems;
        private bool isFilter;
        #endregion

        #region Properties
        public List<ActivityItemViewModel> table;
        public ObservableCollection<Grouping<string, ActivityItemViewModel>> ActivitiesCollection
        {
            get { return this.activitiesCollection; }
            set { SetValue(ref this.activitiesCollection, value); }
        }

        public string Day
        {
            get { return MainViewModel.GetInstance().Day; }
        }

        public string TextFilter 
        {
            get { return this.textFilter; }
            set { SetValue(ref this.textFilter, value); }
        } // 

        public string DayNumber
        {
            get { return MainViewModel.GetInstance().DayNumber.ToString(); }
        }

        public bool IsFilter 
        {
            get { return this.isFilter; }
            set { SetValue(ref this.isFilter, value); }
        }

        public bool IsVisibleNoItems
        {
            get
            {
                if (this.ActivitiesCollection.Count == 0) this.isVisibleNoItems = true;
                else this.isVisibleNoItems = false;

                return this.isVisibleNoItems;
            }
            set { SetValue(ref this.isVisibleNoItems, value); }
        }
        #endregion

        #region Commands
        public ICommand AddActCommand
        {
            get { return new RelayCommand(async () => 
            {
                MainViewModel.GetInstance().AddActivity = new AddActivityViewModel();
                await Application.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(
                    new AddActivityPage())
                {
                    BarBackgroundColor = Color.FromHex("#18E1BF"),
                });
                //DELETE THIS CODE
                //var instance = DependencyService.Get<INotificationManager>();
                //instance.Initialize();
                //instance.SetNotification("Test notification", "this notification is a test one!", 10, true, 10); //not repeated
            }); }
        }

        public ICommand FilterActivitiesCommand
        {
            get { return new RelayCommand(async () =>
            {
                string[] days = new string[]
                {
                    Languages.MondayDay, 
                    Languages.TuesdayDay,
                    Languages.WednesdayDay,
                    Languages.ThursdayDay, 
                    Languages.FridayDay,
                    Languages.SaturdayDay,
                    Languages.SundayDay,
                };
                string ASResponse = await Application.Current.MainPage.DisplayActionSheet(
                    Languages.ViewActivitiesBy,
                    Languages.CancelText,
                    null,
                    days);

                //if (ASResponse != Languages.FilterViewAllText && ASResponse != Languages.CancelText)
                //{
                //    List<Grouping<string, ActivityModel>> listItems = 
                //    sorted.Where(t => t.Key == ASResponse).ToList();
                //    ActivitiesCollection = new ObservableCollection<Grouping<string, ActivityModel>>(
                //        listItems);
                //}
                //else if (ASResponse == Languages.FilterViewAllText)
                //    this.LoadDBActivitiesContent(); SHOW THE HOURS OF THE CURRENT DAY
                if (ASResponse == Languages.CancelText)
                    return;

                this.LoadCurrentHours(ASResponse);
                listHours.Add(Languages.FilterViewAllText);
                //LoadCurrentDays();
                string hourResponse = await Application.Current.MainPage.DisplayActionSheet(
                    ASResponse/*Languages.FilterTasksTitle*/,
                    Languages.CancelText,
                    null,
                    listHours.ToArray());

                if (hourResponse == Languages.CancelText)
                    return;

                if (hourResponse != Languages.FilterViewAllText)// && 
                //    hourResponse != Languages.CancelText)
                {
                    this.LoadCurrentDays();
                    ActivitiesCollection =
                        new ObservableCollection<Grouping<string, ActivityItemViewModel>>(
                    sorted.Where(t => t.Key == hourResponse).ToArray());
                }
                else/* if (hourResponse == Languages.FilterViewAllText)*/
                {
                    ActivitiesCollection = 
                        new ObservableCollection<Grouping<string, ActivityItemViewModel>>();
                    this.LoadCurrentDays();
                }

                //if (hourResponse != Languages.CancelText)
                //{
                this.SetVisibilityImage(false);

                if (ASResponse == MainViewModel.GetInstance().Day && 
                    hourResponse == Languages.FilterViewAllText)
                {
                    if (this.IsFilter) this.IsFilter = false;
                    return;
                }

                this.TextFilter = $"{ASResponse}, {hourResponse}";
                this.IsFilter = true;
                //}
            });
            }
        }

        public ICommand OpenStatsCommand
        {
            get { return new RelayCommand(async () =>
            {
                //MainViewModel.GetInstance().Home = new HomeViewModel();
                MainViewModel.GetInstance().Stats = new StatsViewModel();
                await Application.Current.MainPage.Navigation.PushModalAsync(
                    new NavigationPage(new StatsPage())
                    { BarBackgroundColor = Color.FromHex("#18E1BF")});
            }); }
        }

        public ICommand QuitFilterCommand 
        {
            get { return new RelayCommand(() =>
            {
                this.LoadCurrentHours(MainViewModel.GetInstance().Day);
                ActivitiesCollection =
                    new ObservableCollection<Grouping<string, ActivityItemViewModel>>();
                this.LoadCurrentDays();
                this.IsFilter = false;
                //this.TextFilter = string.Empty;
                SetVisibilityImage(false);
            }); }
        }
        #endregion

        #region Constructors
        public HomeViewModel()
        {
            //this.LoadDBActivitiesContent(); CORRECT
            ActivitiesCollection = 
                new ObservableCollection<Grouping<string, ActivityItemViewModel>>();
            this.LoadCurrentHours(MainViewModel.GetInstance().Day); // CHANGE
            this.LoadCurrentDays();
        }
        #endregion

        #region Methods
        public void LoadDBActivitiesContent()
        {
            ActivitiesCollection = 
                new ObservableCollection<Grouping<string, ActivityItemViewModel>>();
            SQLHelper sqlConnection = new SQLHelper();

            string dayWeek = MainViewModel.GetInstance().Day;
            listHours = new List<string>();
            table = ConverterHelper.ConverToListActivityItemViewModel(
                SQLHelper.Connection.Table<ActivityModel>().Where(a =>
                a.ActivityDays.Contains(dayWeek)).ToList());

            List<double> hoursTransformed = new List<double>();

            foreach (var item in table)
            {
                bool isPresented = false;
                string[] arr = item.ActivityDays.Split(char.Parse(","));
                var dayHour = arr.Where(h => h.Contains(dayWeek)).ToArray()[0];
                dayHour = dayHour.Substring(dayHour.IndexOf(":") + 1);
                item.SpecificHour = dayHour;
                for (int i = 0; i<listHours.Count; i++)
                {
                    if (dayHour == listHours[i])
                    {
                        isPresented = true;
                        break;
                    }
                }
                if (!isPresented)
                {
                    int index = 0;
                    var str = dayHour.Replace(char.Parse(":"), char.Parse("."));
                    if (!string.IsNullOrEmpty(str))
                    {
                        for (int i = 0; i < listHours.Count; i++)
                        {
                            var replaced = double.Parse(listHours[i].Replace(Char.Parse(":"),
                                Char.Parse(".")));

                            if (double.Parse(str) > replaced)
                                index = i + 1;
                        }
                    }
                    if(dayHour.Length > 0)
                        listHours.Insert(index, dayHour);
                }
            }

            foreach (var item in listHours)
            {

                sorted = from activities in table
                             orderby activities.SpecificHour// orberby is a keyword of System.Linq namespace
                             group activities by activities.SpecificHour into activitiesList
                             select new Grouping<string, ActivityItemViewModel>(activitiesList.Key, activitiesList);
                List<Grouping<string, ActivityItemViewModel>> listItems = 
                    sorted.Where(t => t.Key == item).ToList();
                foreach (var item2 in listItems)
                {
                    ActivitiesCollection.Add(item2);
                    //PresentedItemsVisible = false;
                }
            }
            sqlConnection.CloseConnection();

            listHours.Add(Languages.FilterViewAllText);

            if(this.ActivitiesCollection.Count == 0) this.IsVisibleNoItems = true;

            if (this.IsFilter) this.IsFilter = false;

            else this.IsVisibleNoItems = false;
        }

        public void LoadCurrentHours(string day)
        {
            //ActivitiesCollection = new ObservableCollection<Grouping<string, ActivityModel>>();
            sqlConnection = new SQLHelper();

            string dayWeek = day;//MainViewModel.GetInstance().Day;
            listHours = new List<string>();
            table = ConverterHelper.ConverToListActivityItemViewModel(SQLHelper.Connection.Table<ActivityModel>().Where(a =>
                a.ActivityDays.Contains(dayWeek)).ToList());

            List<double> hoursTransformed = new List<double>();

            foreach (var item in table)
            {
                bool isPresented = false;
                string[] arr = item.ActivityDays.Split(char.Parse(","));
                var dayHour = arr.Where(h => h.Contains(dayWeek)).ToArray()[0];
                dayHour = dayHour.Substring(dayHour.IndexOf(":") + 1);
                item.SpecificHour = dayHour;
                for (int i = 0; i < listHours.Count; i++)
                {
                    if (dayHour == listHours[i])
                    {
                        isPresented = true;
                        break;
                    }
                }
                if (!isPresented)
                {
                    int index = 0;
                    var str = dayHour.Replace(char.Parse(":"), char.Parse("."));
                    if (!string.IsNullOrEmpty(str))
                    {
                        for (int i = 0; i < listHours.Count; i++)
                        {
                            var replaced = double.Parse(listHours[i].Replace(Char.Parse(":"),
                                Char.Parse(".")));

                            if (double.Parse(str) > replaced)
                                index = i + 1;
                        }
                    }

                    if(dayHour.Length > 0)
                        listHours.Insert(index, dayHour);
                }
            }
        }

        public void LoadCurrentDays()
        {
            foreach (var item in listHours)
            {

                sorted = from activities in table
                         orderby activities.SpecificHour// orberby is a keyword of System.Linq namespace
                         group activities by activities.SpecificHour into activitiesList
                         select new Grouping<string, ActivityItemViewModel>(activitiesList.Key, activitiesList);
                List<Grouping<string, ActivityItemViewModel>> listItems = sorted.Where(t => t.Key == item).ToList();
                
                foreach (var item2 in listItems)
                    ActivitiesCollection.Add(item2);
            }
            sqlConnection.CloseConnection();

            listHours.Add(Languages.FilterViewAllText);
        }

        public void SetVisibilityImage(bool isVisible)
        {
            this.IsVisibleNoItems = isVisible;
            if (ActivitiesCollection.Count == 0) this.IsVisibleNoItems = !isVisible;
        }
        #endregion
    }
}
