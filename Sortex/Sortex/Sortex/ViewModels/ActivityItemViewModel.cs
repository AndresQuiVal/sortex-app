﻿using GalaSoft.MvvmLight.Command;
using Plugin.LocalNotifications;
using Sortex.Enums;
using Sortex.Helpers;
using Sortex.Interfaces;
using Sortex.Models;
using Sortex.Views;
//using SQLite.Net.Attributes;//using SQLite;
using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class ActivityItemViewModel : ActivityModel, INotifyPropertyChanged
    {
        private bool isTiming;
        private bool isTimingButtonEnabled;
        private string timerButtonText;
        private Color activityBackColor;

        public event PropertyChangedEventHandler PropertyChanged;

        [Ignore]
        public ImageSource ActivityIconImage
        {
            get
            {
                if (this.PersonalizedIcon == null)
                {
                    if (ActivityIcon == Languages.DoExcerciseText)
                        return "excerciseIcon";
                    else if (ActivityIcon == Languages.StudyText)
                        return "studyIcon";
                    else if (ActivityIcon == Languages.WorkText)
                        return "workIcon";
                    else if (ActivityIcon == Languages.DoShoppingText)
                        return "shopIcon";
                    else if (ActivityIcon == Languages.ReadText)
                        return "readIcon";
                    else if (ActivityIcon == Languages.GoToSchoolText)
                        return "schoolIcon";
                    else if (ActivityIcon == Languages.PlayText)
                        return "playIcon";
                    else if (ActivityIcon == Languages.EatText)
                        return "eatIcon";
                    else
                        return "SortexAppIcon";
                }
                return ImageSource.FromStream(() =>
                {
                    Stream streamImage = new MemoryStream(this.PersonalizedIcon);
                    return streamImage;
                });
            }
        }

        [Ignore]
        public string SpecificHour { get; set; }

        [Ignore]
        public string MeanChronTime { get; set; }

        [Ignore]
        public string StartChronTime { get; set; } // TEMPORARY VAR VALUES

        [Ignore]
        public string StartStopActionSheetText { get; set; } = Languages.StartTimerText; // TEMPORARY VAR VALUES

        [Ignore]
        public bool IsTimingButtonEnabled
        {
            get { return this.isTimingButtonEnabled; }
            set
            {
                if (this.isTimingButtonEnabled == value)
                    return;
                this.isTimingButtonEnabled = value;
                OnPropertyChanged();
            }
        }

        [Ignore]
        public string TimerButtonText
        {
            get { return this.timerButtonText; }
            set
            {
                if (this.timerButtonText == value)
                    return;
                this.timerButtonText = value;
                OnPropertyChanged();
            }
        }

        [Ignore]
        public TimeModel[] TimesArray
        {
            get
            {
                if (string.IsNullOrEmpty(this.Times))
                    return new TimeModel[0];

                var timesSplitted = this.Times.Split(char.Parse("^"));
                TimeModel[] timeInstances = new TimeModel[timesSplitted.Length - 1];
                for (int i = 0; i < timesSplitted.Length - 1; i++)
                {
                    timeInstances[i] = new TimeModel()
                    {
                        Id = (i + 1).ToString(),
                        Time = timesSplitted[i],
                    };
                }
                return timeInstances;
            }
        }

        [Ignore]
        public ICommand ActivityOptionsCommand
        {
            get { return new RelayCommand(this.ActivityOptions); }
        }

        [Ignore]
        public ICommand DoneActivityOptionsCommand
        {
            get { return new RelayCommand(async () =>
            {
                string doneASR = await Application.Current.MainPage.DisplayActionSheet(
                    Languages.ActivityOptionsText,
                    Languages.CancelText,
                    null,
                    Languages.EstablishNotDoneActivitiesText);
                if (doneASR == Languages.EstablishNotDoneActivitiesText)
                {
                    SQLHelper sqlConnection = new SQLHelper();

                    this.IsDone = false;
                    this.DoneTimes--;

                    if (!string.IsNullOrEmpty(this.WeekChronTimes))
                    {
                        string newWCT =
                            this.WeekChronTimes.Remove(this.WeekChronTimes.LastIndexOf(","));

                        this.WeekChronTimes = this.WeekChronTimes.Remove(
                            newWCT.LastIndexOf(char.Parse(",")) + 1);
                    }

                    var response = sqlConnection.Update(
                        ConverterHelper.ConvertToActivityModel(this));

                    sqlConnection.CloseConnection();
                    if (!response.IsSuccess)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                            "Error",
                            response.Message,
                            Languages.CancelText);
                        return;
                    }
                    await Application.Current.MainPage.DisplayAlert(
                            Languages.CorrectText,
                            $"{this.Name} {Languages.EstablishedAsNotDoneText}",
                            Languages.CancelText);

                    MainViewModel.GetInstance().Home.LoadDBActivitiesContent();
                    GetTotalDoneActivities(true);
                    await Application.Current.MainPage.Navigation.PopModalAsync();
                }
            }); }
        }

        [Ignore]
        public ICommand StartTimerCommand
        {
            get { return new RelayCommand(this.StartTimer); }
        }

        [Ignore]
        public ICommand WeekChronTimesCommand
        {
            get
            {
                return new RelayCommand(async () =>
                {
                    MainViewModel.GetInstance().WeekTimesActivity =
                        new WeekTimesActivityViewModel(this);
                    await Application.Current.MainPage.Navigation.PushModalAsync(
                        new NavigationPage(new WeekTimesActivityPage())
                        { BarBackgroundColor = Xamarin.Forms.Color.FromHex("#18E1BF") });
                });
            }
        }

        [Ignore]
        public Color ActivityBackColor
        {
            get
            {
                if (this.IsDone)
                    activityBackColor = Color.FromHex("#18E1BF");
                else
                    activityBackColor = Color.FromHex("#FF2300");

                return activityBackColor;
            }
            //set
            //{
            //    this.activityBackColor = value;
            //    OnPropertyChanged();
            //}
        }

        [Ignore]
        public Dictionary<string, string> DayHourDict
        {
            get
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                var separated = this.ActivityDays.Split(char.Parse(","));
                foreach (var item in separated)
                {
                    if (item != "")
                    {
                        var key = item.Substring(0, item.IndexOf(":"));
                        var value = item.Substring(item.IndexOf(":") + 1);
                        dict.Add(key, value);
                    }
                }
                return dict;
            }
        }

        [Ignore]
        public Dictionary<string, long> DayHourDictLong
        {
            get
            {
                Dictionary<string, long> dict = new Dictionary<string, long>();
                var separated = this.ActivityDays.Split(char.Parse(","));
                foreach (var item in separated)
                {
                    if (item != "")
                    {
                        var key = item.Substring(0, item.IndexOf(":"));
                        var value = item.Substring(item.IndexOf(":") + 1);
                        dict.Add(key, (long)(TimeManagerHelper.GetHourInDouble(value) * 3600));
                    }
                }
                return dict;
            }
        }

        [Ignore]
        public Dictionary<string, string> DictWeekChronTimes 
        {
            get
            {
                if (string.IsNullOrEmpty(WeekChronTimes))
                    return null;

                string[] weekChronTimesSplitted = WeekChronTimes.Split(char.Parse(","));
                Dictionary<string, string> dictWeekCT = new Dictionary<string, string>();
                //var dictWeekChronTimes = from time in weekChronTimesSplitted
                //                         select new KeyValuePair<string, string>(
                //                             time.Substring(0, time.IndexOf(char.Parse(":"))),
                //                             time.Substring(time.IndexOf(char.Parse(":")) + 1));

                //return (Dictionary<string, string>)dictWeekChronTimes;

                foreach (var keyValue in weekChronTimesSplitted)
                {

                    if(string.Equals(keyValue, string.Empty)) continue;

                    //string newKeyValue = keyValue.Remove(keyValue.Length - 1);
                    dictWeekCT.Add(keyValue.Substring(0, keyValue.IndexOf(char.Parse(":"))),
                                    keyValue.Substring(keyValue.IndexOf(char.Parse(":")) + 1));
                }

                return dictWeekCT;
            }
        }

        [Ignore]
        public long[] NotificationIntervalsArr 
        {
            get // how to parse all elems without a for or a foreach or while??
            {
                string[] splitted = this.NotificationIntervals.Split(char.Parse(","));
                long[] newItems = TimeManagerHelper.TimeToLongParser(
                    splitted.Skip(0).Take(splitted.Length).ToArray());

                //for (int i = 0; i < splitted.Length; i++)
                //    newItems[i] = long.Parse(splitted[i]);
                return newItems;
            }
        }

        public ActivityItemViewModel()
        {
            TimerButtonText = Languages.TimeText;
            IsTimingButtonEnabled = true;
        }
            

        public async void ActivityOptions()
        {
            List<string> asOptions = new List<string>
            {
                Languages.TaskOptionsDeleteText,
                Languages.TaskOptionsEditText,
            };


            if (string.Equals(StartStopActionSheetText, Languages.StopTimerText))
                asOptions.Add(Languages.ViewTimeText);

            if (!MainViewModel.GetInstance().Home.IsFilter)
            {
                asOptions.Add(StartStopActionSheetText);

                if (!this.IsDone)
                    asOptions.Add(Languages.ActivityDoneText);

            }


            //var response = await Application.Current.MainPage.DisplayActionSheet(
            //        String.Format($"{Languages.ActivityOptionsText}{this.Name}"),
            //        Languages.CancelText,
            //        null,
            //        Languages.TaskOptionsDeleteText,
            //        Languages.TaskOptionsEditText,
            //        this.EvalStartStopTimerText( Languages.StartTimerText, Languages.StopTimerText),
            //        Languages.ActivityDoneText);

            var response = await Application.Current.MainPage.DisplayActionSheet(
                    String.Format($"{Languages.ActivityOptionsText}{this.Name}"),
                    Languages.CancelText,
                    null,
                    asOptions.ToArray());

            if (response == Languages.TaskOptionsDeleteText)
            {
                SQLHelper sqlConnection = new SQLHelper();
                sqlConnection.Delete(ConverterHelper.ConvertToActivityModel(this));

                MainViewModel.GetInstance().Home.LoadDBActivitiesContent();
                sqlConnection.CloseConnection();
                GetTableCount(true);

                //cancel notification
                NotificationsHelper.CancelNotification(
                    DependencyService.Get<INotificationManager>(),
                    Convert.ToInt32(this.Id),
                    BroadCastType.Activity);
            }
            else if (response == Languages.TaskOptionsEditText)
            {
                MainViewModel.GetInstance().EditActivity = new EditActivityViewModel(
                    ConverterHelper.ConvertToActivityModel(this), true); // CHANGE TO DEF CTOR!!!!!
                await Application.Current.MainPage.Navigation.PushModalAsync(
                    new NavigationPage(new EditActivityPage())
                    { BarBackgroundColor = Xamarin.Forms.Color.FromHex("#18E1BF") });
            }
            else if (response == Languages.ActivityDoneText)
            {
                if (this.IsDone)
                    return;

                SQLHelper sqlConnection = new SQLHelper();
                this.IsDone = true;
                this.DoneTimes++;

                sqlConnection.Update(ConverterHelper.ConvertToActivityModel(this));

                this.SetNextNotification();

                //var homeViewModel = MainViewModel.GetInstance().Home;
                MainViewModel.GetInstance().Home.LoadDBActivitiesContent();

                //if (homeViewModel.IsFilter) homeViewModel.IsFilter = false;

                sqlConnection.CloseConnection();
                GetTotalDoneActivities(true);
            }
            else if (response == Languages.StartTimerText)
            {
                StartStopActionSheetText = Languages.StopTimerText;
                StartChronTime = DateTime.Now.ToString("HH:mm:ss");
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Timing,
                    $"{this.Name} {Languages.HasStartedTimingText}",
                    "OK");
            }
            else if (response == Languages.StopTimerText)
                this.StopTimer();

            else if (response == Languages.ViewTimeText)
            {
                MainViewModel.GetInstance().ActivityTiming =
                    new ActivityTimingViewModel(this);

                await Application.Current.MainPage.Navigation.PushModalAsync(
                    new NavigationPage(new ActivityTimingPage())
                    { BarBackgroundColor = Xamarin.Forms.Color.FromHex("#18E1BF") });
            }
        }

        public async void StopTimer()
        {
            var time = 
                TimeManagerHelper.GetSubstractionHourLongString(
                    DateTime.Now.ToString("HH:mm:ss"), 
                    this.StartChronTime);

            string[] saveTimeStringContext = Languages.SaveTimeContext.Split(char.Parse(","));

            bool confirmationResponse = await Application.Current.MainPage.DisplayAlert(
                Languages.SaveTimeText,
                $"{saveTimeStringContext[0]}{time}{saveTimeStringContext[1]}{this.Name}{saveTimeStringContext[2]}?",
                Languages.NoText, // INVERSE ORDER!
                Languages.YesText);

            StartStopActionSheetText = Languages.StartTimerText;

            if (confirmationResponse)
                return;

            //if (!confirmationResponse)
            //{

            if (!string.IsNullOrEmpty(this.WeekChronTimes) &&
                //this.WeekChronTimes.StartsWith(MainViewModel.GetInstance().Day)
                this.WeekChronTimes.Contains(MainViewModel.GetInstance().Day))
            {
                string substringWeekChronTimes = this.WeekChronTimes.Substring(
                    this.WeekChronTimes.IndexOf(MainViewModel.GetInstance().Day));

                substringWeekChronTimes = substringWeekChronTimes.Substring(0, 
                    substringWeekChronTimes.IndexOf(char.Parse(",")) + 1); // + 1

                string newWeekChronTime =
                    String.Format($"{MainViewModel.GetInstance().Day}:{time},");

                this.WeekChronTimes = this.WeekChronTimes.Replace(
                    substringWeekChronTimes,/*this.WeekChronTimes*/
                    newWeekChronTime);
            }
                
            else
                this.WeekChronTimes +=
                    $"{MainViewModel.GetInstance().Day}:{time},";

            SQLHelper sqlConnection = new SQLHelper();

            var response = sqlConnection.Update(
                ConverterHelper.ConvertToActivityModel(this));

            if (response.IsSuccess)
                await Application.Current.MainPage.DisplayAlert(
                    $"{this.Name} {Languages.TimeFinishedText}",
                    $"{Languages.TimeWasText} {time}",
                    "OK");

            sqlConnection.CloseConnection();
            StartChronTime = null;
        }

        public string EvalStartStopTimerText(string t1, string t2)
        {
            if (string.Equals(t1, StartStopActionSheetText))
                return t2;
            return t1;

        }

        public string CalculateChronActivity()
        {
            //return TimeManagerHelper.GetSubstractionHours(
            //    $"{this.DayHourDict[MainViewModel.GetInstance().Day]}:00",
            //    DateTime.Now.ToString("HH:mm:ss"));
            return TimeManagerHelper.GetSubstractionHourLongString(
                DateTime.Now.ToString("HH:mm:ss"),
                this.StartChronTime);
        }

        public void SetNextNotification()
        {
            var hourNotification = float.Parse(this.DayHourDict[MainViewModel.GetInstance().Day].Replace(char.Parse(":"),
                char.Parse(".")));
            var currentHour = float.Parse(DateTime.Now.ToString("HH:mm").Substring(0, 5).
                Replace(char.Parse(":"), char.Parse(".")));
            //if (float.Parse(this.DayHourDict[MainViewModel.GetInstance().Day].Replace(char.Parse(":"),
            //    char.Parse("."))) < float.Parse(DateTime.Now.ToShortTimeString().Substring(0, 5).
            //    Replace(char.Parse(":"), char.Parse("."))))
            //    return;

            if (hourNotification < currentHour)
                return;

            INotificationManager notificationManager =
                DependencyService.Get<INotificationManager>();

            notificationManager.CancelNotification(Convert.ToInt32(this.Id), 
                BroadCastType.Activity); // better Convert.ToInt64()

            string[] keyCollection = DayHourDict.Keys.ToArray();
            int index = Array.IndexOf(keyCollection, MainViewModel.GetInstance().Day) + 1;
            //int rest = (index + 1) - index;
            
            if (index == keyCollection.Length)
                index = 0;

            string nextDay = keyCollection[index];

            //Get how many days where long since today and mult them to 24

            ////Get days long
            int diffCurrentDay = (int)DateTime.Now.DayOfWeek;
            int diffNextDay = TimeManagerHelper.GetConstValueDay(
                MainViewModel.GetInstance().DayToEnglish(nextDay));

            if (diffNextDay == 0)
                diffNextDay = 7;

            int absDif = Math.Abs(diffNextDay - diffCurrentDay);
            ////

            ////Add hours
             string newHourNotification = 
                TimeManagerHelper.AddHours(DayHourDict[nextDay], 24 * absDif);
            ////
            
            //

            NotificationsHelper.SetRepeatedNotificationDependency(
                notificationManager,
                this.Name,
                $"{Languages.DoTheActivityText} {this.Name}",
                newHourNotification,
                DayHourDict.Values.ToArray(),
                Convert.ToInt32(this.Id));
        }

        public async void StartTimer()
        {
            //Call notification DependencyService
            var notificationManager = DependencyService.Get<INotificationManager>();

            isTiming = !isTiming;
            int seconds = -1, minutes = 0, hours = 0;

            //Set all the buttons timing button to their opposed value
            ActivityItemViewModel[] instanceCollection = 
                MainViewModel.GetInstance().Home.table.Where(
                a => a != this).ToArray();
            foreach (var instance in instanceCollection)
                instance.IsTimingButtonEnabled = !isTiming;

            //Send to the ActivityTimingPage.xaml
            if (isTiming)
            {
                TimerButtonText = Languages.StopText;
                MainViewModel.GetInstance().ActivityTiming =
                new ActivityTimingViewModel(this);

                await Application.Current.MainPage.Navigation.PushModalAsync(
                    new NavigationPage(new ActivityTimingPage())
                    { BarBackgroundColor = Xamarin.Forms.Color.FromHex("#18E1BF") });
            }
            else
            {
                TimerButtonText = Languages.TimeText;
                CrossLocalNotifications.Current.Cancel(1);
            }
            //Run timer
            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                seconds++;
                if (seconds >= 60)
                {
                    seconds = 0;
                    minutes++;
                    if (minutes >= 60)
                    {
                        minutes = 0;
                        hours++;
                    }
                }

                //CrossLocalNotifications.Current.Show(
                //    $"{this.Name} {Languages.ActivityTimingText}",
                //     $"{Languages.TimeRegularText}: {hours.ToString("00")}:" +
                //     $"{minutes.ToString("00")}:" +
                //     $"{seconds.ToString("00")}",
                //     1);
                notificationManager.SetReminderNotification(
                    $"{this.Name} {Languages.ActivityTimingText}",
                    $"{Languages.TimeRegularText}: {hours.ToString("00")}:",
                    1, Convert.ToInt32(this.Id) ,true/*, new long[] { 1,1,1,1,1,1,1 }*/);


                MainViewModel.GetInstance().ActivityTiming.ExecuteTime(
                    hours, minutes, seconds);

                return isTiming;
            });
        }


        #region BaseViewModel methods
        public void OnPropertyChanged([CallerMemberName] string property = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion


        public static int GetTableCount(bool setForProfileViewModel = false)
        {
            SQLHelper sqlConnection = new SQLHelper();
            int tableCount = SQLHelper.Connection.Table<ActivityModel>().Count();
            sqlConnection.CloseConnection();

            //Verification for setting the table count into the ProfileViewModel property
            if (setForProfileViewModel)
                MainViewModel.GetInstance().Profile.TotalActivities = 
                    tableCount.ToString();

            return tableCount;
        }

        public static int GetTotalDoneActivities(bool setForProfileViewModel = false)
        {
            SQLHelper sqlConnection = new SQLHelper();
            int tableCount = SQLHelper.Connection.Table<ActivityModel>().Where(
                l => l.IsDone).Count();
            sqlConnection.CloseConnection();

            if (setForProfileViewModel)
                MainViewModel.GetInstance().Profile.TotalDoneActivities = tableCount.ToString();

            return tableCount;
        }
    }
}
