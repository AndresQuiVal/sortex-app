﻿using GalaSoft.MvvmLight.Command;
using MvvmHelpers;
using Sortex.Helpers;
using Sortex.Models;
using Sortex.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class TaskViewModel : BaseViewModel
    {
        #region Attributes
        //private ObservableCollection<Grouping<string, TaskItemViewModel>> veryImportantList;
        private ObservableCollection<Grouping<string, TaskItemViewModel>> veryImportantList;
        private SQLHelper sqliteConnection;
        //private string isDone;
        //private string day;
        private string textFilter;
        private bool isFilter;
        private bool isRefreshing;
        private bool presentedItemsVisible = false;
        #endregion

        #region Properties
        public ObservableCollection<Grouping<string, TaskItemViewModel>> VeryImportantList
        {
            get { return this.veryImportantList; }
            set { SetValue(ref this.veryImportantList, value); }
        }
        public string Day
        {
            get { return String.Format(
                $"{Languages.ToDoTitleText}\n{MainViewModel.GetInstance().Day}"); }
        }

        public string TextFilter 
        {
            get { return this.textFilter; }
            set { SetValue(ref this.textFilter, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        public bool PresentedItemsVisible
        {
            get { return this.presentedItemsVisible; }
            set { SetValue(ref this.presentedItemsVisible, value); }
        }

        public bool IsFilter 
        {
            get { return this.isFilter; }
            set { SetValue(ref this.isFilter, value); }
        }
        #endregion

        #region Commands
        public ICommand AddNewTaskCommand
        {
            get { return new RelayCommand(this.AddNewTask); }
        }

        public ICommand FilterTasksCommand
        {
            get { return new RelayCommand(this.FilterTasks); }
        }

        public ICommand QuitFilterCommand 
        {
            get { return new RelayCommand(() =>
            {
                IsFilter = false;
                this.ReadTasksFromDB();
            }); }
        }
        #endregion

        #region Constructors
        public TaskViewModel()
        {
            //NotThatImportantList = new ObservableCollection<TaskModel>();
            //NotImportantList = new ObservableCollection<TaskModel>();
            ReadTasksFromDB();
        }
        #endregion

        #region Methods
        public async void AddNewTask()
        {
            MainViewModel.GetInstance().NewTask = new NewTaskViewModel();
            await Application.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(
                new NewTaskPage())
            { BarBackgroundColor = Xamarin.Forms.Color.FromHex("#18E1BF") });
        }

        public void ReadTasksFromDB()
        {
            PresentedItemsVisible = true;
            IsRefreshing = true;
            List<TaskItemViewModel> tableList = GetTableData().Where(
                t => t.IsDoneTask == false && t.IsPlan == false).ToList(); //!t.IsDoneTask && !t.IsPlan

            //Organize the priorities in our code from the most important to the less important
            foreach (var item in new string[]
            {
                Languages.PlansText,
                Languages.HightPriorText,
                Languages.MediumPriorText,
                Languages.LessPriorityText
            })
            {
                var sorted = from tasks in tableList
                             orderby tasks.PriorityLevel // orberby is a keyword of System.Linq namespace
                             group tasks by tasks.PriorityLevel into taskList
                             select new Grouping<string, TaskItemViewModel>(taskList.Key, taskList);
                List<Grouping<string, TaskItemViewModel>> listItems = sorted.Where(t => t.Key == item).ToList();
                foreach (var item2 in listItems)
                {
                    VeryImportantList.Add(item2);
                    PresentedItemsVisible = false;
                }
            }

            sqliteConnection.CloseConnection();
            IsRefreshing = false;
        }

        public async void FilterTasks()
        {
            var filterResponse = await Application.Current.MainPage.DisplayActionSheet(
                Languages.FilterTasksTitle,
                Languages.CancelText,
                null,
                Languages.PlansText,
                Languages.HightPriorText,
                Languages.MediumPriorText,
                Languages.LessPriorityText,
                Languages.FilterViewAllText);

            if (filterResponse == Languages.CancelText)
                return;

            else if (filterResponse == Languages.FilterViewAllText)
            {
                if (IsFilter) IsFilter = false;

                ReadTasksFromDB();
                return;
            }
            SortTableOnListView(filterResponse); // filters data by the condition set
            IsFilter = true;
            TextFilter = filterResponse;
    }

        public void SortTableOnListView(string condition)
        {
            var sorted = from tasks in GetTableData()
                         orderby tasks.PriorityLevel
                         where tasks.PriorityLevel == condition && !tasks.IsDoneTask
                         group tasks by tasks.PriorityLevel into taskList
                         select new Grouping<string, TaskItemViewModel>(taskList.Key, taskList);
            
            foreach (var item in sorted)
                VeryImportantList.Add(item);

            sqliteConnection.CloseConnection();
        }

        public List<TaskItemViewModel> GetTableData()
        {
            VeryImportantList = new ObservableCollection<Grouping<string, TaskItemViewModel>>();
            sqliteConnection = new SQLHelper();
            var table = SQLHelper.Connection.Table<TaskModel>().Where(t => !t.IsPlan);
            return ConverterHelper.ConvertToTaskItemViewModel(table).ToList();
        }
        #endregion
    }
}
