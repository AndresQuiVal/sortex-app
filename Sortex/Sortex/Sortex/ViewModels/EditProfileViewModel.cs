﻿using GalaSoft.MvvmLight.Command;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Sortex.Enums;
using Sortex.Helpers;
using Sortex.Interfaces;
using Sortex.Models;
using Sortex.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class EditProfileViewModel : BaseViewModel
    {
        #region Class fields
        private ImageSource profileImage;
        private string userName = null;
        private bool isRunning;
        private bool hasSelected;
        private INotificationManager notificationManager;
        public MediaFile file;
        private TimeSpan previousSleepTime;
        #endregion

        #region Properties
        public string UserName
        {
            get
            {
                if (this.userName == null)
                {
                    if (this.userModel == null || string.IsNullOrEmpty(userModel.UserName))
                        this.userName = Languages.YouText;
                    else
                        this.userName = userModel.UserName;
                }
                return this.userName;
            }
            set
            { SetValue(ref this.userName, value); }
        }

        public UserModel userModel { get; set; }
        public ImageSource ProfileImage
        {
            get
            {
                if (this.file == null && !hasSelected)
                {
                    if (this.userModel != null && this.userModel.ProfileImage != null)
                    {
                        this.profileImage = ImageSource.FromStream(() =>
                        {
                            Stream profileImageStream = new MemoryStream(userModel.ProfileImage);
                            return profileImageStream;
                        });
                    }
                    else this.profileImage = "UserIcon";
                }
                return this.profileImage;
            }
            set { SetValue(ref this.profileImage, value); }
        }

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }
        #endregion

        #region Commands
        public ICommand SaveProfileChangesCommand
        {
            get { return new RelayCommand(this.SaveProfileChanges); }
        }

        public ICommand ImageConfigSelectorCommand
        {
            get { return new RelayCommand(this.ImageConfigSelector); }
        }
        #endregion

        #region Constructors
        public EditProfileViewModel(UserModel userModel)
        {
            this.userModel = userModel;
            this.notificationManager = DependencyService.Get<INotificationManager>();
            if(this.userModel != null)
                this.previousSleepTime = userModel.SleepHourTSFormat;
        }
        #endregion

        #region Methods
        //public void LoadNotificationManager()
        //{
        //    this.notificationManager.NotificationReceived += async (sender, args) =>
        //    {
        //        var notifArgs = (NotificationsEventArgs)args;
        //        if (string.Equals(notifArgs.Title, "Your plans"))
        //        {
        //            MainViewModel.GetInstance().PlanList = new PlanListViewModel();
        //            await Application.Current.MainPage.Navigation.PushModalAsync(
        //                new NavigationPage(new PlanListPage()));
        //        }
        //    };
        //}
        public async void SaveProfileChanges()
        {
            SQLHelper sqlConnection = new SQLHelper();

            this.userModel.SleepHour = this.userModel.SleepHourTSFormat.ToString();
            if (!this.userModel.ToDoListAllowed)
                this.userModel.SleepHour = this.previousSleepTime.ToString();

            this.userModel.UserName = this.UserName;
            if (file != null)
                this.userModel.ProfileImage = FilesHelper.GetImageByteArray(this.file.GetStream());
            else if (hasSelected)
                this.userModel.ProfileImage = null;
            sqlConnection.Update(this.userModel);

            //creating notification
            //if (this.IsTimingNotificationEnabled())
            //    await Application.Current.MainPage.DisplayAlert( // simplify with a method
            //        "Notification success",
            //        $"The app will notify you at " +
            //        $"{this.userModel.SleepHour.Substring(0, this.userModel.SleepHour.LastIndexOf(":"))}" +
            //        $" about when to make your plans list",
            //        "OK");

            this.IsTimingNotificationEnabled();

            MainViewModel.GetInstance().Profile.UserModel = userModel;
            MainViewModel.GetInstance().Profile.LoadProfileInfo();

            await Application.Current.MainPage.Navigation.PopModalAsync();
            //violating Single responsibilty principle

            sqlConnection.CloseConnection();
        }

        public async void IsTimingNotificationEnabled()
        {
            if (this.userModel.ToDoListAllowed)
            {
                double min30less = TimeManagerHelper.GetRest(
                    this.userModel.SleepHour);

                if (min30less < 1800)
                    min30less = TimeManagerHelper.GetRest(
                        this.userModel.SleepHour, 1);

                string newHour;

                double userModelSleepHour =
                    TimeManagerHelper.GetHourInDouble(this.userModel.SleepHour);

                if (userModelSleepHour >= .30)
                {
                    min30less -= //TimeManagerHelper.GetRest(
                        /*this.userModel.SleepHour) -*/ 1800;

                    //var hourInDouble = (userModelSleepHour * 3600) - 1800;
                    //newHour = TimeManagerHelper.GetDoubleInHour(hourInDouble);
                }

                 //min30less = TimeManagerHelper.GetRest(this.userModel.SleepHour);
                    
                notificationManager.SetRepeatedNotification(
                    Languages.YourPlansText,
                    Languages.EstablishTomorrowPlansText,
                    /*TimeManagerHelper.GetRest(this.userModel.SleepHour)*/
                    (long)min30less, 
                    0);

                newHour = 
                    TimeManagerHelper.GetSubstractionHourLongString(
                        this.userModel.SleepHour, "00:30");
                newHour = newHour.Substring(0, newHour.LastIndexOf(char.Parse(":")));

                await Application.Current.MainPage.DisplayAlert( // simplify with a method
                    Languages.ProfileEditedText,
                    $"{Languages.ProfileEditedContextPart1} {newHour}" +
                    $" {Languages.ProfileEditedContextPart2}",
                    "OK");

                return;
            }

            await Application.Current.MainPage.DisplayAlert( // simplify with a method
                   Languages.ProfileEditedText,
                   Languages.ProfileEditedContextPart1.Substring(0,
                       Languages.ProfileEditedContextPart1.IndexOf("!") + 1),
                   "OK");

            NotificationsHelper.CancelNotification(
                DependencyService.Get<INotificationManager>(), 0, BroadCastType.Activity);
        }

        public async void ImageConfigSelector()
        {
            this.IsRunning = true;
            this.hasSelected = false;
            await CrossMedia.Current.Initialize();

            if (CrossMedia.Current.IsCameraAvailable &&
                    CrossMedia.Current.IsTakePhotoSupported)
            {
                var actionSheetProfileSelection = await Application.Current.MainPage.DisplayActionSheet(
                    Languages.RecomendedResolText,//Languages.TitleImageConfigSelection,
                    null,
                    Languages.CancelText,
                    Languages.TakeImageTitle,
                    Languages.GalleryImageTitle,
                    Languages.ResetImageTitle);

                if (actionSheetProfileSelection == Languages.TakeImageTitle)
                {
                    this.file = await CrossMedia.Current.TakePhotoAsync(
                        new StoreCameraMediaOptions
                        {
                            Directory = "SortexImages", //
                            Name = "myprofileimage.jpg",
                            PhotoSize = PhotoSize.Large
                        }
                    );
                }
                else if (actionSheetProfileSelection == Languages.GalleryImageTitle)
                    this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.Large
                    });

                else if (actionSheetProfileSelection == Languages.ResetImageTitle)
                {
                    hasSelected = true;
                    this.ProfileImage = "UserIcon";
                }

                else
                {
                    IsRunning = false;
                    return;
                }
            }
            else
            {
                this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.Large
                });

            }

            if (this.file != null)// || hasSelected)
            {
                if (!hasSelected)
                {
                    this.ProfileImage = ImageSource.FromStream(() =>
                    {
                        var stream = file.GetStream();
                        return stream;
                    });
                }
            }
            IsRunning = false;
        }
        #endregion
    }
}
