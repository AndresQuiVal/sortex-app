﻿using GalaSoft.MvvmLight.Command;
using Sortex.Helpers;
using Sortex.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class InitialGuideViewModel : BaseViewModel
    {
        #region Commands
        public ICommand FinishInitialGuideCommand 
        {
            get { return new RelayCommand(async () =>
            {
                await Application.Current.MainPage.Navigation.PopModalAsync();

                this.ShowMessage(Languages.WelcomeAppText, 
                    Languages.AppTextIntro,
                    Languages.StartText);

                this.CreateUser();
            }); }
        }
        #endregion

        #region Methods
        private void CreateUser()
        {
            SQLHelper sqlConnection = new SQLHelper();
            sqlConnection.DeleteAll<UserModel>(); // if its an existing user on the device
            
            UserModel userModel = new UserModel()
            {
                CurrentDayTask = DateTime.Now.ToShortDateString(),
                ProfileImage = null,
                ActivitiesNotificationText = Languages.DoTheActivityText,
                PlansNotificationsText =
                    $"{Languages.YourPlansText},{Languages.EstablishTomorrowPlansText}"
            };

            MainViewModel.GetInstance().Profile.UserModel = userModel;

            var response = sqlConnection.Insert(userModel);

            if (!response.IsSuccess)
                this.ShowMessage("Error", response.Message, Languages.CancelText);

            sqlConnection.CloseConnection();
        }

        private async void ShowMessage(
            string title,
            string content,
            string textButton) =>
            await Application.Current.MainPage.DisplayAlert(title, content, textButton);
        #endregion
    }
}
