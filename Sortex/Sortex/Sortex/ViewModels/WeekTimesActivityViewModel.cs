﻿using Sortex.Helpers;
using Sortex.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Sortex.ViewModels
{
    public class WeekTimesActivityViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<DayTimeActivityModel> dayTimeCollection;
        private int meanChronTableHeight;
        #endregion

        #region Properties
        public ActivityItemViewModel Activity { get; set; }
        public ObservableCollection<DayTimeActivityModel> DayTimeCollection
        {
            get { return this.dayTimeCollection; }
            set { SetValue(ref this.dayTimeCollection, value); }
        }

        public int MeanChronTableHeight 
        {
            get { return this.meanChronTableHeight; }
            set { SetValue(ref this.meanChronTableHeight, value); }
        }
        #endregion

        #region Constructors
        public WeekTimesActivityViewModel(ActivityItemViewModel activityItemViewModel)
        {
            this.Activity = activityItemViewModel;
            this.LoadDayTimeCollection();
        }
        #endregion

        #region Methods
        public void LoadDayTimeCollection()
        {
            DayTimeCollection = new ObservableCollection<DayTimeActivityModel>();

            foreach (var day in new string[] {
                Languages.MondayDay, Languages.TuesdayDay, Languages.WednesdayDay,
                Languages.ThursdayDay, Languages.FridayDay, Languages.SaturdayDay,
                Languages.SundayDay })
            {
                DayTimeActivityModel dtam = new DayTimeActivityModel() { Day = day };
                dtam.Time = Languages.NotTimedText;

                if (this.Activity.DictWeekChronTimes != null && 
                    this.Activity.DictWeekChronTimes.ContainsKey(day))
                    dtam.Time = 
                        TimeSpan.Parse(this.Activity.DictWeekChronTimes[day]).ToString();

                DayTimeCollection.Add(dtam);
            }

            MeanChronTableHeight = DayTimeCollection.Count * 46;
        }
        #endregion
    }
}
