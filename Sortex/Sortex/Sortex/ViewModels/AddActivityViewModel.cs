﻿using GalaSoft.MvvmLight.Command;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Sortex.Helpers;
using Sortex.Interfaces;
using Sortex.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class AddActivityViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<DaysActivityModel> daysCollection;
        private ObservableCollection<string> pickerImageItems;
        private ImageSource pickerImageSelection;
        private INotificationManager notificationManager;
        private MediaFile file;
        private string activityName;
        private string activityDescription;
        private string actionSheetIconResponse = null;
        private bool isChecked;
        private bool isImageLoading;
        private bool isNotImageLoading;
        //private bool isInitalTimeSet = true;
        private int pickerImageItemsIndex;
        private SQLHelper sqlConnection;
        #endregion

        #region Properties
        public ObservableCollection<DaysActivityModel> DaysCollection
        {
            get { return this.daysCollection; }
            set { SetValue(ref this.daysCollection, value); }
        }

        public ObservableCollection<string> PickerImageItems
        {
            get { return this.pickerImageItems; }
            set { SetValue(ref this.pickerImageItems, value); }
        }

        public bool IsChecked
        {
            get { return this.isChecked; }
            set { SetValue(ref this.isChecked, value); }
        }

        public string ActivityName
        {
            get { return this.activityName; }
            set 
            {
                SetValue(ref this.activityName, value);
                OnPropertyChanged("TitleActivity");
            }
        }

        public string ActivityDescription
        {
            get { return this.activityDescription; }
            set { SetValue(ref this.activityDescription, value); }
        }

        public string TitleActivity 
        {
            get 
            {
                if (string.IsNullOrEmpty(this.ActivityName))
                    return Languages.AddActivityText;
                return this.ActivityName;
            }
        }
        public ImageSource PickerImageSelection
        {
            get
            {
                if (string.IsNullOrEmpty(actionSheetIconResponse))
                {
                    this.pickerImageSelection = MainViewModel.GetInstance().PickerImageSelection(
                        PickerImageItems, PickerImageItemsIndex);
                }
                return this.pickerImageSelection;
            }
            set { SetValue(ref this.pickerImageSelection, value); }
        }

        public int PickerImageItemsIndex
        {
            get { return this.pickerImageItemsIndex; }
            set
            {
                SetValue(ref this.pickerImageItemsIndex, value);

                if (this.file != null) file = null;

                if (PickerImageItems[this.pickerImageItemsIndex] == Languages.UploadIconText)
                    DisplayImageActionSheet();

                OnPropertyChanged("PickerImageSelection");
            }
        }

        public bool IsImageLoading
        {
            get { return this.isImageLoading; }
            set
            {
                this.IsNotImageLoading = !value;
                SetValue(ref this.isImageLoading, value);
            }
        }

        public bool IsNotImageLoading
        {
            get { return this.isNotImageLoading; }
            set { SetValue(ref this.isNotImageLoading, value); }
        }
        #endregion

        #region Commands
        public ICommand AddActivityCommand
        {
            get { return new RelayCommand(this.AddActivity); }
        }
        #endregion

        #region Constructors
        public AddActivityViewModel()
        {
            this.PickerImageItems = MainViewModel.GetInstance().ActivityItems;
            this.notificationManager = DependencyService.Get<INotificationManager>();
            this.LoadDaysCollectionInfo();
            IsImageLoading = false;
        }
        #endregion

        #region Methods
        public void LoadDaysCollectionInfo()
        {
            DaysCollection = new ObservableCollection<DaysActivityModel>();
            DaysActivityModel.DayHourDict = new Dictionary<string, string>();
            DaysActivityModel.ItsFirstTime = true;

            foreach (var item in new[] { Languages.MondayDay, Languages.TuesdayDay, Languages.WednesdayDay,
                Languages.ThursdayDay, Languages.FridayDay, Languages.SaturdayDay, Languages.SundayDay
                //"Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
                /*"Saturday", "Sunday"*/ })
            {
                DaysCollection.Add(new DaysActivityModel()
                {
                    Day = item,
                    IsChecked = true,
                });
                DaysActivityModel.DayHourDict.Add(item, "00:00");
            }   
        }

        public async void AddActivity()
        {
            string dictDays = "", actIcon = null, notificationTimes = "";
            byte[] byteArr = null;

            //Instance of SQLHelper
            sqlConnection = new SQLHelper();
            var tableInfo = SQLHelper.Connection.GetTableInfo("ActivityModel");

            //Iterate over the dayhour dictionary

            //foreach (KeyValuePair<string, string> item in DaysActivityModel.DayHourDict)
            //    dictDays += String.Format($"{item.Key}:{item.Value},");
            string todayHour = DaysActivityModel.DayHourDict[MainViewModel.GetInstance().Day];

            if (DaysActivityModel.DayHourDict.ContainsKey(Languages.SundayDay))
            {
                dictDays = $"{Languages.SundayDay}:" +
                        $"{DaysActivityModel.DayHourDict[Languages.SundayDay]},";
                DaysActivityModel.DayHourDict.Remove(Languages.SundayDay);
                //if(DaysActivityModel.DayHourDict.Keys.ToList().IndexOf(Languages.SundayDay)
                //    != DaysActivityModel.DayHourDict.Keys.ToList().LastIndexOf(Languages.SundayDay))
            }
                
    
            for (int i = 0; i < DaysActivityModel.DayHourDict.Keys.Count; i++)
            //i < DaysActivityModel.DayHourDict.Keys.Count - 1
            {
                if (i == DaysActivityModel.DayHourDict.Keys.Count) break;

                if (string.Equals(
                    DaysActivityModel.DayHourDict[
                        DaysActivityModel.DayHourDict.Keys.ToArray()[i]],
                    string.Empty))
                    continue;

                var key = DaysActivityModel.DayHourDict.Keys.ToArray()[i];
                dictDays += $"{key}:{DaysActivityModel.DayHourDict[key]},";
                notificationTimes += $"{DaysActivityModel.DayHourDict[key]},";
            }

            //eval and set image
            if (file != null)
                byteArr = FilesHelper.GetImageByteArray(this.file.GetStream());
            else
                actIcon = PickerImageItems[PickerImageItemsIndex];

            //Insert notification times arr into property

            //string[] notificationIntervalsArr = 
            //    DaysActivityModel.DayHourDict.Values.ToArray();
            //string notificationTimes = "";
            //for(int i = 0; i<notificationIntervalsArr.Length - 1; i++) // take advantage of the upper foreach loop
            //    notificationTimes += $"{notificationIntervalsArr[i]},";

            //add activity to local db
            ResponseModel response = sqlConnection.Insert(new ActivityModel()
            {
                Name = this.ActivityName,
                Description = this.ActivityDescription,
                ActivityDays = dictDays,
                PersonalizedIcon = byteArr,
                ActivityIcon = actIcon,
                NotificationIntervals = notificationTimes,
                DoneTimes = 0,
            });

            //send an Alert to establish that the process was not ok
            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    Languages.CancelText);
                return;
            }

            //set notification
            //var currentHourSTR = DateTime.Now.ToString("mm/dd/yyyy HH:mm:ss");
            //currentHourSTR = currentHourSTR.Substring(
            //    currentHourSTR.IndexOf(" ") + 1, 5);

            //var currentHour = GetHourInDouble(currentHourSTR);
            //string hour = 
            //    DaysActivityModel.DayHourDict[MainViewModel.GetInstance().Day];
            //var mil = GetHourInDouble(hour) * 3600;

            //var milHourRest = (long)mil;
            //var longCurrentHourRest = (long)(currentHour * 3600);
            //var rest = milHourRest - longCurrentHourRest;

            //var rest = TimeManagerHelper.GetRest();

            //if (rest >= 0)
            //{
            //    long[] timesLongArr = 
            //        TimeManagerHelper.TimeToLongParser(
            //            DaysActivityModel.DayHourDict.Values.ToArray());

            //    NotificationManager = DependencyService.Get<INotificationManager>();

            //    NotificationManager.SetNotification(
            //        $"{this.ActivityName}",
            //        $"Do the activity {this.ActivityName}",
            //        rest,
            //        true,
            //        /*(24 * 3600)*/
            //        timesLongArr);
            //}

            this.EstablishNotification(todayHour);

            //close connection and send to pages
            sqlConnection.CloseConnection();
            await Application.Current.MainPage.Navigation.PopModalAsync();

            MainViewModel.GetInstance().Home.LoadDBActivitiesContent();
            ActivityItemViewModel.GetTableCount(true);
        }

        public void EstablishNotification(string startHour)
        {
            var lastElement = sqlConnection.GetLast<ActivityModel>();

            NotificationsHelper.SetRepeatedNotificationDependency(
                notificationManager,
                this.ActivityName,
                $"{Languages.DoTheActivityText} {this.ActivityName}",
                startHour,
                DaysActivityModel.DayHourDict.Values.ToArray(),
                Convert.ToInt32(lastElement.Id));
        }

        public double GetHourInDouble(string _hour) // can be commented
        {
            int hour =  int.Parse(_hour.Substring(0, _hour.IndexOf(":")));
            int minHour = 
                (100 * int.Parse(_hour.Substring(_hour.IndexOf(":") + 1))) / 60;
            //double min = (100 * minHour) / 60;
            var result = double.Parse($"{hour}.{minHour}");
            if (result <= 0)
                return 1;
            return double.Parse($"{hour}.{minHour}");
        }

        public async void DisplayImageActionSheet()
        {
            //bool hasSelected = false;
            await CrossMedia.Current.Initialize();

            if (CrossMedia.Current.IsCameraAvailable &&
                    CrossMedia.Current.IsTakePhotoSupported)
            {
                actionSheetIconResponse = await Application.Current.MainPage.DisplayActionSheet(
                    Languages.RecomendedResolText, //{Languages.TitleImageConfigSelection}, 
                    null,
                    Languages.CancelText,
                    Languages.TakeImageTitle,
                    Languages.GalleryImageTitle);

                if (actionSheetIconResponse == Languages.TakeImageTitle)
                {
                    this.IsImageLoading = true;
                    this.file = await CrossMedia.Current.TakePhotoAsync(
                        new StoreCameraMediaOptions
                        {
                            Directory = "SortexImages", //
                            Name = "iconImage.jpg",
                            PhotoSize = PhotoSize.Large,
                        }
                    );
                }
                else if (actionSheetIconResponse == Languages.GalleryImageTitle)
                {
                    this.IsImageLoading = true;
                    this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.Large,
                    });
                }

                else
                {
                    this.actionSheetIconResponse = null;
                    this.IsImageLoading = false;
                    return;
                }
            }
            else
            {
                this.IsImageLoading = true;
                this.file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                {
                    PhotoSize = PhotoSize.Large,
                });
            }

            if (this.file != null)
            {
                this.PickerImageSelection = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });
            }

            this.actionSheetIconResponse = null;
            this.IsImageLoading = false;

            //IsRunning = false;
        }
        #endregion
    }
}
