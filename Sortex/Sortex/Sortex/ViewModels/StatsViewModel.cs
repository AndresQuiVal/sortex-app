﻿using System;
using System.Collections.Generic;
using System.Text;
using Microcharts.Forms;
using Microcharts;
using MCEntry = Microcharts.Entry;
using SkiaSharp;
using Sortex.Helpers;
using Sortex.Models;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;
using Sortex.Views;

namespace Sortex.ViewModels
{
    public class StatsViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<ActivityItemViewModel> activities;
        private double meanChronTableHeight;
        #endregion

        #region Properties
        public ChartView ChartViewType { get; set; }
        public List<MCEntry> CVEntry { get; set; }
        public bool IsVisibleAll { get; set; }
        public string TotalActivitiesMean { get; set; }
        public string TotalTasksMean { get; set; }
        public string TotalMean { get; set; }
        public string TasksCreated { get; set; }
        public string TasksDone { get; set; }
        public string TasksNotDone { get; set; }
        public ObservableCollection<ActivityItemViewModel> Activities
        {
            get { return this.activities; }
            set { SetValue(ref this.activities, value); }
        }
        public double MeanChronTableHeight 
        {
            get { return this.meanChronTableHeight; }
            set { SetValue(ref this.meanChronTableHeight, value); }
        }
        #endregion

        #region Commands
        
        #endregion

        #region Constructors
        public StatsViewModel()
        {
            this.LoadStatsData();
        }
        #endregion

        #region Methods
        public void LoadStatsData() // TODO: Initialize the activities collection as well implement the property that gets the mean of activities in ActivityItemViewModel
        {
            IsVisibleAll = true;
            if (((int)DateTime.Now.DayOfWeek) != 0)
            {
                IsVisibleAll = false;
                return;
            }

            CVEntry = new List<MCEntry>();
            Activities = new ObservableCollection<ActivityItemViewModel>();

            //Activities

            SQLHelper sqlConnection = new SQLHelper();
            var table = ConverterHelper.ConverToListActivityItemViewModel(
                SQLHelper.Connection.Table<ActivityModel>().ToList());
            int meanActivities = 0, counter = 0;

            foreach (var activity in table)
            {
                int percentageOfActivity = (activity.DoneTimes * 100) / 7;
                meanActivities += percentageOfActivity;

                var random = new Random();
                var color = String.Format("#{0:X6}", random.Next(0x1000000));
                CVEntry.Add(new MCEntry(percentageOfActivity)
                {
                    Color = SKColor.Parse(color),
                    ValueLabel = $"{percentageOfActivity}%",
                    Label = activity.Name,
                });

                counter++;

                string meanTimeChron = "";

                if (activity.DictWeekChronTimes != null && 
                    activity.DictWeekChronTimes.Values.Count() > 0)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        int sum = 0;
                        foreach (var time in activity.DictWeekChronTimes.Values)
                            sum += int.Parse(time.Split(char.Parse(":"))[i]);

                        sum /= activity.DictWeekChronTimes.Values.Count();
                        meanTimeChron += $"{Math.Round(d:sum).ToString("00")}:";
                    }
                    meanTimeChron = meanTimeChron.Remove(meanTimeChron.Length - 1);
                } 
                else
                    meanTimeChron += Languages.NotTimedText;

                activity.MeanChronTime = meanTimeChron;

                Activities.Add(activity);
            }

            MeanChronTableHeight = Activities.Count * 40; // listview height

            int totalActivitiesM = 0, totalTasksM = 0;
            if (counter > 0)
                totalActivitiesM = (meanActivities / counter);

            TotalActivitiesMean = $"{totalActivitiesM}%";

            //Tasks

            var tableUser = SQLHelper.Connection.Table<UserModel>().ToArray()[0];

            this.TasksCreated = $"{Languages.TasksCreatedText} {tableUser.TotalTasks}";
            this.TasksDone = $"{Languages.TasksDoneText} {tableUser.TasksDone}";
            this.TasksNotDone =
                $"{Languages.TasksNotDoneText} {tableUser.TotalTasks - tableUser.TasksDone}";

            if(tableUser.TotalTasks > 0)
                totalTasksM = ((tableUser.TasksDone * 100) / tableUser.TotalTasks);

            this.TotalTasksMean =  $"{totalTasksM}%";

            sqlConnection.CloseConnection();

            //General

            this.TotalMean = $"{(totalActivitiesM + totalTasksM) / 2}%";

            //Initialize the ChartView

            ChartViewType = new ChartView();
            ChartViewType.Chart = new DonutChart 
            {
                Entries = CVEntry,
                BackgroundColor = SKColors.Transparent,
            };
        }
        #endregion
    }
}
