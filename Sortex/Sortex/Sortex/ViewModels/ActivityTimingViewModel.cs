﻿using GalaSoft.MvvmLight.Command;
using Sortex.Helpers;
using Sortex.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class ActivityTimingViewModel : BaseViewModel
    {
        #region Class fields
        private string time;
        private ObservableCollection<TimeModel> timesCollection;
        #endregion

        #region Properties
        public ActivityItemViewModel Activity { get; set; }
        public string Time
        {
            get { return this.time; }
            set { SetValue(ref this.time, value); }
        }
        #endregion

        #region Commands
        public ICommand StopTimeCommand
        {
            get { return new RelayCommand(async () =>
            {
                Activity.StopTimer();
                await Application.Current.MainPage.Navigation.PopModalAsync();
            }); } 
        }
        #endregion

        #region Constructors
        public ActivityTimingViewModel(
            ActivityItemViewModel activityItemViewModel)
        {
            this.Activity = activityItemViewModel;

            string[] substractionHoursSplitted =
                (TimeManagerHelper.GetSubstractionHourLongString(
                    DateTime.Now.ToString("HH:mm:ss"), activityItemViewModel.StartChronTime))
                    .Split(char.Parse(":"));
            
            int hour = int.Parse(substractionHoursSplitted[0]),
                    min = int.Parse(substractionHoursSplitted[1]),
                    sec = int.Parse(substractionHoursSplitted[2]);

            Time = $"{hour.ToString("00")}:{min.ToString("00")}:{sec.ToString("00")}";

            Device.StartTimer(TimeSpan.FromSeconds(1), () =>
            {
                sec++;
                if (sec >= 60)
                {
                    sec = 0;
                    min++;
                    if (min >= 60)
                    {
                        min = 0;
                        hour++;
                    } 
                }

                Time = 
                    $"{hour.ToString("00")}:{min.ToString("00")}:{sec.ToString("00")}";

                return true;
            });
            //Subscirbing to the message 
            //MessagingCenter.Subscribe<ContentPage>(this, "OnDisappearingATP", StopTimer);
        }
        #endregion

        #region Methods
        public void ExecuteTime(int hours, int minutes, int seconds)
        {
            Time = $"{hours.ToString("00")}:" +
                $"{minutes.ToString("00")}:" +
                $"{seconds.ToString("00")}";
        }
        #endregion
    }
}
