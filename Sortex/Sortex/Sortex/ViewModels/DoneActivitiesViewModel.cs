﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using MvvmHelpers;
using Sortex.Helpers;
using Sortex.Models;

namespace Sortex.ViewModels
{
    public class DoneActivitiesViewModel : BaseViewModel
    {
        #region Class fields
        private List<ActivityModel> activityList;
        private ObservableCollection<Grouping<string, ActivityItemViewModel>> doneActivitiesCollection;
        private bool isVisibleDoneItems;
        #endregion

        #region Properties
        public ObservableCollection<Grouping<string, ActivityItemViewModel>> DoneActivitiesCollection
        {
            get { return this.doneActivitiesCollection; }
            set { SetValue(ref this.doneActivitiesCollection, value); }
        }

        public bool IsVisibleDoneItems
        {
            get
            {
                if (this.DoneActivitiesCollection.Count == 0)
                    this.isVisibleDoneItems = true;
                else this.isVisibleDoneItems = false;

                return this.isVisibleDoneItems;
            }
            set { SetValue(ref this.isVisibleDoneItems, value); }
        }
        #endregion

        #region Constructors
        public DoneActivitiesViewModel(List<ActivityModel> list)
        {
            this.activityList = list;
            this.LoadDoneActivities();
        }
        #endregion

        #region Methods
        public void LoadDoneActivities()
        {
            var filtered = ConverterHelper.ConverToListActivityItemViewModel(
                this.activityList.Where(a => a.IsDone).ToList());

            //
            DoneActivitiesCollection =
                new ObservableCollection<Grouping<string, ActivityItemViewModel>>();
            SQLHelper sqlConnection = new SQLHelper();

            string dayWeek = MainViewModel.GetInstance().Day;
            var listHours = new List<string>();
            var table = filtered.Where(a =>
                a.ActivityDays.Contains(dayWeek)).ToList();

            List<double> hoursTransformed = new List<double>();

            foreach (var item in table)
            {
                bool isPresented = false;
                string[] arr = item.ActivityDays.Split(char.Parse(","));
                var dayHour = arr.Where(h => h.Contains(dayWeek)).ToArray()[0];
                dayHour = dayHour.Substring(dayHour.IndexOf(":") + 1);
                item.SpecificHour = dayHour;
                for (int i = 0; i < listHours.Count; i++)
                {
                    if (dayHour == listHours[i])
                    {
                        isPresented = true;
                        break;
                    }
                }
                if (!isPresented)
                {
                    int index = 0;
                    var str = dayHour.Replace(char.Parse(":"), char.Parse("."));
                    if (!string.IsNullOrEmpty(str))
                    {
                        for (int i = 0; i < listHours.Count; i++)
                        {
                            var replaced = double.Parse(listHours[i].Replace(Char.Parse(":"),
                                Char.Parse(".")));

                            if (double.Parse(str) > replaced)
                                index = i + 1;
                        }
                    }
                    if (dayHour.Length > 0)
                        listHours.Insert(index, dayHour);
                }
            }

            foreach (var item in listHours)
            {

                var sorted = from activities in table
                            orderby activities.SpecificHour// orberby is a keyword of System.Linq namespace
                            group activities by activities.SpecificHour into activitiesList
                            select new Grouping<string, ActivityItemViewModel>(activitiesList.Key, activitiesList);
                List<Grouping<string, ActivityItemViewModel>> listItems =
                    sorted.Where(t => t.Key == item).ToList();
                foreach (var item2 in listItems)
                {
                    DoneActivitiesCollection.Add(item2);
                    //PresentedItemsVisible = false;
                }
            }
            sqlConnection.CloseConnection();

            ////listHours.Add(Languages.FilterViewAllText);
            //

        }
        #endregion

    }
}
