﻿using GalaSoft.MvvmLight.Command;
using Sortex.Helpers;
using Sortex.Models;
using Sortex.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Sortex.ViewModels
{
    public class DetailsTaskViewModel : BaseViewModel
    {
        #region Class fields
        private ObservableCollection<String> pickerItems;
        private ObservableCollection<SubtaskModel> subtaskItems;
        private bool checkBoxState;
        private int listViewHeight;
        private string titleName;
        private string currentName;
        private string taskName;
        #endregion

        #region Commands
        public ICommand SaveChangesCommand
        {
            get { return new RelayCommand(this.SaveChanges); }
        }
        public ICommand DismissCommand
        {
            get { return new RelayCommand(this.Dismiss); }
        }
        public ICommand AddSubtaskCommand
        {
            get { return new RelayCommand(this.AddSubtask); }
        }
        #endregion

        #region Properties
        public TaskModel Task { get; set; }
        public ObservableCollection<String> PickerItems
        {
            get { return this.pickerItems; }
            set { SetValue(ref this.pickerItems, value); }
        }
        public bool CheckBoxState
        {
            get { return this.checkBoxState; }
            set { SetValue(ref this.checkBoxState, value); }
        }
        public int ListViewHeight
        {
            get { return this.listViewHeight; }
            set { SetValue(ref this.listViewHeight, value); }
        }
        public ObservableCollection<SubtaskModel> SubtasksItems
        {
            get { return this.subtaskItems; }
            set { SetValue(ref this.subtaskItems, value); }
        }
        public string TitleName
        {
            get { return this.titleName; }
            set
            {
                this.titleName = value;
                if (string.IsNullOrEmpty(value))
                    this.titleName = currentName;

                OnPropertyChanged();
            }
        }

        public string TaskName
        {
            get { return this.taskName; }
            set
            {
                SetValue(ref this.taskName, value);
                this.TitleName = value;
            }
        }
        #endregion

        #region Constructors
        public DetailsTaskViewModel(TaskModel model)
        {
            this.TaskName = model.Name;
            this.currentName = model.Name;
            this.CheckBoxState = true;
            this.Task = model;
            PickerItems = new ObservableCollection<string>()
            {
                Languages.HightPriorText,
                Languages.MediumPriorText,
                Languages.LessPriorityText,
            };

            //Give the Action my method to execute in the Delete command
            SubtaskModel.DeleteSubtaskAction = DeleteSubtaskItems;
            //

            this.LoadSubtasks();
        }
        #endregion

        #region Methods
        public async void SaveChanges()
        {
            SQLHelper sqlConnection = new SQLHelper();
            string subtaskString = "";
            foreach (var value in SubtaskModel.TextDict)
            {
                if (!string.IsNullOrWhiteSpace(value.Value))
                    subtaskString += String.Format($"{value.Value}^"); //,
            }

            this.Task.Name = this.TaskName;
            this.Task.Subtasks = subtaskString;
            ResponseModel response = sqlConnection.Update(ConverterHelper.ConvertToTaskModel(this.Task));
            if (response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.CorrectText,
                    String.Format($"{this.Task.Name} {Languages.AddedCorrectrlySubstring}"),
                    Languages.CancelText);
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.CorrectText,
                    response.Message,
                    Languages.CancelText);
            }
            //MainViewModel.GetInstance().Profile.LoadProfileActivityInfo();
            MainViewModel.GetInstance().LoadDBContentTaskModel();
            sqlConnection.CloseConnection();
            await Application.Current.MainPage.Navigation.PopModalAsync();

            if (MainViewModel.GetInstance().IsPlanSender)
                MainViewModel.GetInstance().Plans.LoadPlans();
        }

        public void ChangeSubtaskHelperProperties()
        {
            OnPropertyChanged("SubtaskItems");
            OnPropertyChanged("ListViewHeight");
        }

        public void AddSubtask()
        {
            ActivitySubtaskHelper.AddSubtask(ref this.subtaskItems, ref this.listViewHeight);
            //ChangeSubtaskHelperProperties();
            ChangeSubtaskHelperProperties();
        }

        public void LoadSubtasks()
        {
            SubtasksItems = new ObservableCollection<SubtaskModel>();

            if (this.Task.Subtasks == null || this.Task.Subtasks.Length == 0)
                return;

            var splitted = Task.Subtasks.Split(char.Parse("^"));
            for (int i = 0; i < splitted.Length - 1; i++)
            {
                if (splitted[i].EndsWith("6U1D"))
                    continue;
                SubtasksItems.Add(new SubtaskModel() { Text = splitted[i] });
            }
            ActivitySubtaskHelper.SetListViewLength(
                ref this.subtaskItems, 
                ref this.listViewHeight);
        }

        public void DeleteSubtaskItems(SubtaskModel instance) // TODO: CODE CAN BE SIMPLIFIED
        {
            this.SubtasksItems.Remove(instance);
            this.ListViewHeight =
                this.SubtasksItems.Count *
                MainViewModel.GetInstance().ListViewSubtaskHeight; // ?
        }

        public async void Dismiss() => await Application.Current.MainPage.Navigation.PopModalAsync();

        #endregion
    }
}
