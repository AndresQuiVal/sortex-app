﻿using Sortex.Models;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlanListPage : ContentPage
    {
        public PlanListPage()
        {
            InitializeComponent();
            NavigationPage.SetTitleIconImageSource(this, "SortexLogo");
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MainViewModel.GetInstance().ReInstanciateSubtaskModelProps();
            MainViewModel.GetInstance().IsPlanSender = false;
            //SubtaskModel.StackDefValues = new Stack<string>();
        }
    }
}