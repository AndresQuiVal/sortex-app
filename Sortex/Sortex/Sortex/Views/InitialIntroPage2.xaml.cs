﻿using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InitialIntroPage2 : ContentPage
    {
        public InitialIntroPage2()
        {
            InitializeComponent();
        }
    }
}