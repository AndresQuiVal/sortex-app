﻿using Sortex.Helpers;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sortex.Models;
using Xamarin.Forms.Xaml;
using Xamarin.Forms;
using SQLite;
//using SQLite.Net;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        private bool isSwitched;
        private SQLHelper sqlConnection;
        private TableQuery<TaskModel> tasksTable;
        private UserModel userModel;
        public bool IsSwitched
        {
            get { return this.IsSwitched; }
            set { this.isSwitched = value; }
        }
        public ProfilePage()
        {
            InitializeComponent();
            MainViewModel.GetInstance().ProfilePage = this;
            MainViewModel.GetInstance().Profile =
                new ProfileViewModel(InitializeSQL(), tasksTable.ToList()); // possible error
        }

        public UserModel InitializeSQL()
        {
            sqlConnection = new SQLHelper();
            tasksTable = SQLHelper.Connection.Table<TaskModel>().Where(t => !t.IsPlan);
            var tableUser = ConverterHelper.ConvertToUserModel(
                SQLHelper.Connection.Table<UserModel>().ToList());
            return (UserModel)tableUser.Model;
        }
    }
}