﻿using Sortex.Models;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Sortex.Helpers;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddActivityPage : ContentPage
    {
        public bool IsInitialized { get; set; }
        public AddActivityPage()
        {
            InitializeComponent();
            NavigationPage.SetTitleIconImageSource(this, "SortexLogo");
        }

        private void CheckBox_CheckedChanged(object sender, CheckedChangedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (this.IsInitialized)
            {
                DaysActivityModel CurrentDay = 
                    MainViewModel.GetInstance().AddActivity.DaysCollection.Where(
                    a => a.Day == cb.ClassId).ToArray()[0];

                if (cb.IsChecked && !DaysActivityModel.DayHourDict.ContainsKey(cb.ClassId))
                    DaysActivityModel.DayHourDict.Add(
                        cb.ClassId, CurrentDay.Time.ToString().Substring(0, CurrentDay.Time.ToString()
                        .LastIndexOf(char.Parse(":")))/*CurrentDay.HourEntered*/); // TODO: POSSIBLE TIMES IN ACTIVITIES ERROR
                else
                    DaysActivityModel.DayHourDict.Remove(cb.ClassId);

            }

            if(cb.ClassId == Languages.SundayDay)//"Sunday")
                this.IsInitialized = true;
        }

        //private void EntryHour_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    Entry entr = (Entry)sender;
        //    var id = entr.ClassId;
        //    DaysActivityModel.DayHourDict[entr.ClassId] = entr.Text;
        //}

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            //TimePicker entr = (TimePicker)sender;
            //var id = entr.ClassId;
            //DaysActivityModel.DayHourDict[entr.ClassId] = entr.Time.ToString().Substring(0,
            //    entr.Time.ToString().LastIndexOf(":"));
        }

        private void TimePicker_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TimePicker.TimeProperty.PropertyName)
            {
                TimePicker entr = (TimePicker)sender;
                var id = entr.ClassId;

                string correctTimeFormat = entr.Time.ToString().Substring(0,
                    entr.Time.ToString().LastIndexOf(":"));

                MessagingCenter.Send((ContentPage)this, "TimeChanged", correctTimeFormat);

                DaysActivityModel.DayHourDict[entr.ClassId] = correctTimeFormat;
            }
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            foreach (var instance in DaysActivityModel.DaysAMCollection)
            {
                MessagingCenter.Unsubscribe<ContentPage, string>(
                    instance, "TimeChanged");
            }
        }
    }
}