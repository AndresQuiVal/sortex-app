﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivityTimingPage : ContentPage
    {
        public ActivityTimingPage()
        {
            InitializeComponent();
            NavigationPage.SetTitleIconImageSource(this, "SortexLogo");
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Send((ContentPage)this, "OnDisappearingATP");
            base.OnDisappearing();
        }
    }
}