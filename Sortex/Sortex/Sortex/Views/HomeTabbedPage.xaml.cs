﻿using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeTabbedPage : TabbedPage
    {
        public HomeTabbedPage()
        {
            InitializeComponent();
            try
            {
                //MainViewModel.GetInstance().ExecuteUserValidation();
                this.Children.Add(new HomePage());
                this.Children.Add(new TasksPage());
                this.Children.Add(new ProfilePage());
            }
            catch (Exception ex)
            {

            }
        }
    }
}