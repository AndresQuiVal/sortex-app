﻿using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditActivityPage : ContentPage
    {
        public EditActivityPage()
        {
            InitializeComponent();
            NavigationPage.SetTitleIconImageSource(this, "SortexLogo");
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            MainViewModel.GetInstance().ActivityItems.Remove("Current icon");
            //TODO: CHANGE LITERAL OF "Current icon"
        }
    }
}