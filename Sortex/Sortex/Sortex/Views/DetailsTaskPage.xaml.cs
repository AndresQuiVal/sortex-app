﻿using Sortex.Helpers;
using Sortex.Models;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailsTaskPage : ContentPage
    {
        public int TaskNumber { get; private set; } = 0;
        public string EntryText { get; set; } = "";
        public static List<string> EditSubtasks { get; set; }
        public List<int> ListIDButton { get; set; } = new List<int>();
        private Entry textBox;

        //private string subtasks;
        public DetailsTaskPage(string subtasks)
        {
            InitializeComponent();
            NavigationPage.SetTitleIconImageSource(this, "SortexLogo");
            //EditSubtasks = new List<string>();
            //this.LoadSubtasks(subtasks);
        }

        //public void LoadSubtasks(string subtasks)
        //{
        //    var elements = subtasks.Split(char.Parse("^")); //,
        //    int counter = 0;
        //    for(int i = 0; i<elements.Length - 1; i++)
        //    {
        //        EditSubtasks.Add(elements[i]); //DELETE COMMENT FOR THIS LINE OF CODE!!
        //        AddSubtasks(elements[i]);
        //        counter++;
        //    }
                
        //}

        //public void AddSubtasksPlanB(string textItem)
        //{
        //    EditSubtasks.Add(textItem);
        //    var lbl = new Label()
        //    {
        //        Text = (++TaskNumber).ToString(),
        //        FontSize = 20,
        //        TextColor = Color.White,
        //        HorizontalOptions = LayoutOptions.Start,
        //        VerticalOptions = LayoutOptions.Center
        //    };

        //    textBox = new Entry()
        //    {
        //        ClassId = (TaskNumber - 1).ToString(),
        //        PlaceholderColor = Color.FromHex("#32A390"),
        //        Placeholder = "ex Math homework",
        //        TextColor = Color.FromHex("#18E1BF"),
        //        HorizontalOptions = LayoutOptions.CenterAndExpand,
        //        VerticalOptions = LayoutOptions.Center,
        //        Text = textItem,
        //    };

        //    var bindingProp = new Binding("EditSubtasks[TaskNumber - 1]");
        //    var text = EditSubtasks[TaskNumber - 1];
        //    //textBox.TextChanged += new EventHandler<Xamarin.Forms.TextChangedEventArgs>(TextChanged);
        //    textBox.BindingContext = this;
        //    textBox.SetBinding(Entry.TextProperty, bindingProp);
        //    textBox.Text = text;

        //    var btnLess = new IDButton()
        //    {
        //        ButtonID = TaskNumber - 1,
        //        CornerRadius = 50,
        //        HorizontalOptions = LayoutOptions.End,
        //        VerticalOptions = LayoutOptions.Center,
        //        HeightRequest = 35,
        //        WidthRequest = 60,
        //        Text = "Remove",
        //        FontSize = 10,
        //        BackgroundColor = Color.Red,
        //        TextColor = Color.White,
        //    };

        //    btnLess.Clicked += new EventHandler(Delete_Command);
        //    //ListIDButton.Add(btnLess.ButtonID);

        //    var stackContainer = new StackLayout()
        //    {
        //        ClassId = TaskNumber.ToString(),
        //        Orientation = StackOrientation.Horizontal,
        //        Padding = new Thickness(10)
        //    };

        //    stackContainer.Children.Add(lbl);
        //    stackContainer.Children.Add(textBox);
        //    stackContainer.Children.Add(btnLess);

        //    EditSubtasksLayout.Children.Add(stackContainer);
        //}

        //public void AddSubtasks(string textItem) // DO NOT DELETE THIS METHOD, ACTUAL METHOD ON ACTION
        //{
        //    //EditSubtasks.Add("");
        //    EntryText = textItem;
        //    var lbl = new Label()
        //    {
        //    //    WidthRequest = 100,
        //        Text = (++TaskNumber).ToString(),
        //        FontSize = 20,
        //        TextColor = Color.White,
        //        HorizontalOptions = LayoutOptions.Start,
        //        VerticalOptions = LayoutOptions.Center
        //    };

        //    textBox = new Entry()
        //    {
        //        //WidthRequest = 220,
        //        ClassId = (TaskNumber - 1).ToString(),
        //        PlaceholderColor = Color.FromHex("#32A390"),
        //        Placeholder = "ex Math homework",
        //        TextColor = Color.FromHex("#18E1BF"),
        //        HorizontalOptions = LayoutOptions.CenterAndExpand,
        //        VerticalOptions = LayoutOptions.Center,
        //        Text = textItem,
        //    };

        //    //var bindingProp = new Binding("EditSubtasks[TaskNumber - 1]");
        //    var text = EditSubtasks[TaskNumber - 1];
        //    textBox.TextChanged += new EventHandler<Xamarin.Forms.TextChangedEventArgs>(TextChanged);
        //    textBox.BindingContext = this;
        //    textBox.SetBinding(Entry.TextProperty,
        //        new Binding("EntryText"));
        //    textBox.Text = text;

        //    var btnLess = new IDButton()
        //    {
        //        ButtonID = TaskNumber - 1,
        //        CornerRadius = 50,
        //        HorizontalOptions = LayoutOptions.End,
        //        VerticalOptions = LayoutOptions.Center,
        //        HeightRequest = 35,
        //        //WidthRequest = 60,
        //        Text = Languages.RemoveButtonText,
        //        FontSize = 10,
        //        BackgroundColor = Color.Red,
        //        TextColor = Color.White,
        //    };

        //    btnLess.Clicked += new EventHandler(Delete_Command);
        //    ListIDButton.Add(btnLess.ButtonID);
        //    //
        //    var gridContainer = new Grid()
        //    {
        //        Margin = new Thickness(5),
        //        ClassId = TaskNumber.ToString(),
        //        ColumnDefinitions = new ColumnDefinitionCollection()
        //        {
        //            new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
        //            new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) },
        //            new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
        //        },
        //        Padding = new Thickness(5),
        //    };

        //    gridContainer.Children.Add(lbl, 0, 0);
        //    gridContainer.Children.Add(textBox, 1, 0);
        //    gridContainer.Children.Add(btnLess, 2, 0);

        //    EditSubtasksLayout.Children.Add(gridContainer);
        //    //

        //    //var stackContainer = new StackLayout()
        //    //{
        //    //    Margin = 5,
        //    //    ClassId = TaskNumber.ToString(),
        //    //    Orientation = StackOrientation.Horizontal,
        //    //    Padding = new Thickness(10)
        //    //};

        //    //stackContainer.Children.Add(lbl);
        //    //stackContainer.Children.Add(textBox);
        //    //stackContainer.Children.Add(btnLess);

        //    //EditSubtasksLayout.Children.Add(stackContainer);
        //}

        //private void Delete_Command(object sender, EventArgs e)
        //{
        //    IDButton btnSender = sender as IDButton;
        //    var index = ListIDButton.IndexOf(btnSender.ButtonID);
        //    EditSubtasksLayout.Children.RemoveAt(index);
        //    EditSubtasks.RemoveAt
        //        (index);
        //    //TaskNumber--;
        //    ListIDButton.Remove(btnSender.ButtonID);

        //    // Remove all controls from layout and then add them to sort the number of it
        //    var count = EditSubtasksLayout.Children.Count;
        //    EditSubtasksLayout.Children.Clear();
        //    TaskNumber = 0;
        //    EntryText = "";
        //    ListIDButton.Clear();
        //    for (int i = 0; i < count; i++)
        //        AddSubtasks(EditSubtasks[i]);
        //}

        //private void TextChanged(object sender, TextChangedEventArgs e)
        //{
        //    Entry entSender = sender as Entry;
        //    EditSubtasks[int.Parse(entSender.ClassId)] = EntryText;
        //}

        //private void Button_Clicked(object sender, EventArgs e)
        //{
        //    subtasksLbl.Text = "";
        //    foreach (var item in EditSubtasks)
        //        subtasksLbl.Text += String.Format($"{item}, ");
        //}

        //public void Button_ClickedAddItems(object sender, EventArgs e)
        //{
        //    EditSubtasks.Add("");
        //    AddSubtasks("");
        //}

        protected override void OnDisappearing() =>
            MainViewModel.GetInstance().ReInstanciateSubtaskModelProps();
    }
}