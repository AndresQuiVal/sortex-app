﻿using Sortex.Models;
using Sortex.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sortex.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewTaskPage : ContentPage
    {
        public int TaskNumber { get; set; } = 1;
        public Entry textBox { get; set; }
        public string EntryText { get; set; } = "";
        //public List<string> ListLabelText { get; set; } = new List<string>();
        public List<int> ListIDButton { get; set; } = new List<int>();
        public NewTaskPage()
        {
            InitializeComponent();
            NavigationPage.SetTitleIconImageSource(this, "SortexLogo");
            //MainViewModel.GetInstance().DelegatePointer = ClearControlsFromSubtasksLayout;
            //MainViewModel.GetInstance().SubtaskLayout = SubtasksLayout;
        }

        //public void AddSubtasks()
        //{
        //    //ListLabelText.Add("");
        //    //ListLabelText[TaskNumber - 1] = (++TaskNumber).ToString();
        //    var lbl = new Label()
        //    {
        //        Text = (++TaskNumber).ToString(),
        //        FontSize = 20,
        //        TextColor = Color.White,
        //        HorizontalOptions = LayoutOptions.Start,
        //        VerticalOptions = LayoutOptions.Center
        //    };
        //    //var numberText = (++TaskNumber).ToString();
        //    //lbl.BindingContext = this;
        //    //lbl.SetBinding(Label.TextProperty,
        //    //    new Binding("numberText"));
        //    //ListLabelText[TaskNumber - 1] = numberText;

        //    textBox = new Entry()
        //    {
        //        ClassId = (TaskNumber - 1).ToString(),
        //        Placeholder = "ex Math homework",
        //        PlaceholderColor = Color.FromHex("#32A390"),
        //        TextColor = Color.FromHex("#18E1BF"),
        //        HorizontalOptions = LayoutOptions.CenterAndExpand,
        //        VerticalOptions = LayoutOptions.Center,
        //        Text = MainViewModel.GetInstance().SubtasksList[TaskNumber - 1],
        //    };

        //    var text = MainViewModel.GetInstance().SubtasksList[TaskNumber - 1];
        //    textBox.TextChanged += new EventHandler<Xamarin.Forms.TextChangedEventArgs>(TextChanged);
        //    textBox.BindingContext = this;
        //    textBox.SetBinding(Entry.TextProperty,
        //        new Binding("EntryText"));//"MainViewModel.GetInstance().SubtasksList[TaskNumber - 1]"));
        //    textBox.Text = text;

        //    var btnLess = new IDButton()
        //    {
        //        ButtonID = TaskNumber - 1,
        //        CornerRadius = 50,
        //        HorizontalOptions = LayoutOptions.End,
        //        VerticalOptions = LayoutOptions.Center,
        //        HeightRequest = 35,
        //        WidthRequest = 60,
        //        Text = "Remove",
        //        FontSize = 10,
        //        BackgroundColor = Color.Red,
        //        TextColor = Color.White,
        //    };

        //    btnLess.Clicked += new EventHandler(Delete_Command);
        //    ListIDButton.Add(btnLess.ButtonID);
        //    //
        //    var gridContainer = new Grid()
        //    {
        //        Margin = new Thickness(5),
        //        ClassId = TaskNumber.ToString(),
        //        ColumnDefinitions = new ColumnDefinitionCollection()
        //        {
        //            new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Auto) },
        //            new ColumnDefinition() { Width = new GridLength(2, GridUnitType.Star) },
        //            new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) },
        //        },
        //        Padding = new Thickness(10),
        //    };

        //    gridContainer.Children.Add(lbl, 0, 0);
        //    gridContainer.Children.Add(textBox, 1, 0);
        //    gridContainer.Children.Add(btnLess, 2, 0);

        //    SubtasksLayout.Children.Add(gridContainer);
        //    //

        //    //var stackContainer = new StackLayout()
        //    //{
        //    //    ClassId = TaskNumber.ToString(),
        //    //    Orientation = StackOrientation.Horizontal,
        //    //    Padding = new Thickness(10)
        //    //};

        //    //stackContainer.Children.Add(lbl);
        //    //stackContainer.Children.Add(textBox);
        //    //stackContainer.Children.Add(btnLess);

        //    //SubtasksLayout.Children.Add(stackContainer);
        //}

        //private void Button_Clicked(object sender, EventArgs e)
        //{
        //    MainViewModel.GetInstance().SubtasksList.Add("");
        //    AddSubtasks();
        //}

        //public void Delete_Command(object sender, EventArgs e)
        //{
        //    IDButton btnSender =  sender as IDButton;
        //    var index = ListIDButton.IndexOf(btnSender.ButtonID);
        //    SubtasksLayout.Children.RemoveAt(index);
        //    MainViewModel.GetInstance().SubtasksList.RemoveAt
        //        (index + 1);
        //    //TaskNumber--;
        //    ListIDButton.Remove(btnSender.ButtonID);

        //    // Remove all controls from layout and then add them to sort the number of it
        //    var count = SubtasksLayout.Children.Count;
        //    SubtasksLayout.Children.Clear();
        //    TaskNumber = 1;
        //    EntryText = "";
        //    ListIDButton.Clear();
        //    for (int i = 0; i < count; i++)
        //        AddSubtasks();
        //    //
        //}

        //public void TextChanged(object sender, EventArgs e)
        //{
        //    Entry entSender = sender as Entry;
        //    MainViewModel.GetInstance().SubtasksList[int.Parse(entSender.ClassId)] = EntryText;            
        //}

        //public void ClearControlsFromSubtasksLayout() => SubtasksLayout.Children.Clear();

        protected override void OnDisappearing() =>
            MainViewModel.GetInstance().ReInstanciateSubtaskModelProps();
    }
}