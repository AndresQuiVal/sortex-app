﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

using Foundation;
using Sortex.CustomRenderers;
using Sortex.iOS.CRImplementations;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TagFrame), typeof(TagFrameCustomRendereriOS))]
namespace Sortex.iOS.CRImplementations
{
    public class TagFrameCustomRendereriOS : FrameRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                this.Layer.CornerRadius = 15;

                this.Layer.ShadowOpacity = 0.5f;
                this.Layer.ShadowRadius = 1.0f;
                this.Layer.ShadowOffset = new SizeF(2, 2);
            }
        }
    }
}