﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using Sortex.Interfaces;
using Sortex.iOS.Implementations;
using SQLite;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(iOSSQLiteService))]
namespace Sortex.iOS.Implementations
{
    public class iOSSQLiteService : ISQLiteService
    {
        public SQLiteConnection CreateConnection()
        {
            var path = Path.Combine(
                System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal),
                "sortexdb.sqlite");
            return new SQLiteConnection(path);
        }
    }
}